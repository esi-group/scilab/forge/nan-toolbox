function [h,stats] = nan_cdfplot(X,varargin)
    //  plots empirical commulative distribution function
    //  Calling Sequence
    //   nan_cdfplot(X)
    //   nan_cdfplot(X, FMT)
    //   nan_cdfplot(X, PROPERTY, VALUE,...)
    //   h = nan_cdfplot(...)
    //   [h,stats] = nan_cdfplot(...)
    // Parameters
    //  X: contains the data vector (matrix data is currently changed to a vector, this might change in future)
    //  FMT,PROPERTY,VALUE : are used for formating; see HELP PLOT for more details
    //  h: is the handle to the cdf curve
    //  stats: is a struct containing various summary statistics    like mean, std, median, min, max, etc.
    //  Examples
    //  y=grand(100,1,'gam',1,1);
    //  nan_cdfplot(y);
    //  x = 0:0.1:7;
    //  f = cdfgam("PQ",x',ones(71,1),ones(71,1));
    //  plot(x,f,'m');
    //  legend('Empirical','Theoretical','Location','NW');
    // See also
    //nan_ecdf
    //nan_median
    //nan_statistic
    // nan_hist2res
    //Authors
    //       Copyright (C) 2009 by Alois Schloegl a.schloegl@ieee.org
    //  H.Nahrstaedt - 2010

    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    // This program is free software; you can redistribute it and/or
    // modify it under the terms of the GNU General Public License
    // as published by the Free Software Foundation; either version 3
    // of the  License, or (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program; if not, write to the Free Software
    // Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    try
        his = histo_mex(X(:));
    catch
        his = nan_histo3(X(:));
    end

    //hh  = plot(his.X,his.H/sum(his.H));


    cdf = cumsum(his.H,1) ./ sum(his.H,1);
    ix1 = ceil ([1:2*size(his.X,1)]'/2);
    ix2 = floor([2:2*size(his.X,1)]'/2);
    plot (his.X(ix1), [0; cdf(ix2)], varargin(:));
    if nargout>0,
        h = gcf();
    end;
    if nargout>1,
        stats = nan_hist2res(his);
        stats.median = nan_quantile(his,.5);
    end;


endfunction
