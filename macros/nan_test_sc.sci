function [R]=nan_test_sc(CC,D,classlabel,TYPE)
    //  apply statistical and SVM classifier to test data
    // Calling Sequence
    // R = nan_test_sc(CC,D)
    // R = nan_test_sc(CC,D,target_Classlabel)
    //  R = nan_test_sc(CC,D, target_Classlabel,TYPE)
    //  Parameters
    //  CC: classifier, which is typically obtained by train_sc.
    //    target_Classlabel: target class label
    //  TYPE: If a statistical  classifier is used, TYPE can be used to modify the classifier.
    //    TYPE = 'MDA' :   mahalanobis distance based classifier
    //    TYPE = 'MD2' :   mahalanobis distance based classifier
    //    TYPE = 'MD3' :   mahalanobis distance based classifier
    //    TYPE = 'GRB' :   Gaussian radial basis function
    //    TYPE = 'QDA' :   quadratic discriminant analysis
    //    TYPE = 'LD2' :   linear discriminant analysis
    //    TYPE = 'LD3', 'LDA', 'FDA, 'FLDA' :  (Fisher's) linear discriminant analysis
    //    TYPE = 'LD4'  :  linear discriminant analysis
    //    TYPE = 'GDBC' :  general distance based classifier
    //    R: struct with the following fields
    //       R.output  :   	output: "signed" distance for each class.
    //		This represents the distances between sample D and the separating hyperplane
    //		The "signed distance" is possitive if it matches the target class, and
    //		and negative if it lays on the opposite side of the separating hyperplane.
    //       R.classlabel: 	class for output data
    //  target :  The target class is optional. If it is provided, the following values are returned.
    //       R.kappa :	Cohen's kappa coefficient
    //       R.ACC   :	Classification accuracy
    //       R.H     :	Confusion matrix
    //
    // Examples
    // samples = [rand(1,20); -1*rand(1,20)];
    // CC = nan_train_sc(samples,[1;2]);
    // train_samples = [rand(1,20); -1*rand(1,20)];
    // tt = nan_test_sc(CC,train_samples,[1;2]);
    // disp(tt.classlabel==CC.Labels);
    //
    // See also
    // nan_train_sc
    //
    // Bibliography
    // [1] R. Duda, P. Hart, and D. Stork, Pattern Classification, second ed.
    //       John Wiley & Sons, 2001.
    //  Authors
    //  Copyright (C) 2005,2006,2008,2009,2010 by Alois Schloegl a.schloegl@ieee.org
    //  H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2005,2006,2008,2009,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the %nan-toolbox
    //       http://biosig-consulting.com/matlab/%nan/

    // This program is free software; you can redistribute it and/or
    // modify it under the terms of the GNU General Public License
    // as published by the Free Software Foundation; either version 3
    // of the  License, or (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program; if not, write to the Free Software
    // Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
    if nargin==3,
        TYPE = [];
        if type(classlabel)==10 & max(size(classlabel))==1
            TYPE=classlabel;
            classlabel=[];
        end;
    elseif nargin ==2,
        classlabel=[];
        TYPE = [];
    elseif nargin < 2
        error("To less arguments!");
    end;
    mod=ieee();
    ieee(2);
    [t1] = strtok(CC.datatype,":");
    [t2] = strtok(":");
    [t3] = strtok(":");
    if ~(t1=="classifier"), return; end;

    //if isfield(CC,'prewhite')
    allfields=getfield(1,CC);
    if or(allfields(3:$)=="prewhite")
        D = D*CC.prewhite(2:$,:) + CC.prewhite(ones(size(D,1),1),:);
        //CC = rmfield(CC,'prewhite');
    end;

    POS1 = [strindex(CC.datatype,"/gsvd"),strindex(CC.datatype,"/sparse"),strindex(CC.datatype,"/delet")];
    CCfields=getfield(1,CC);
    if 0,


    elseif (CC.datatype=="classifier:nbpw")
        ieee(mod);
        error("NBPW not implemented yet")

        //////// Naive Bayesian Parzen Window Classifier ////////
        d = repmat(%nan,size(D,1),size(CC.MEAN,1));
        for k = 1:size(CC.MEAN,1)
            z = (D - CC.MEAN(repmat(k,size(D,1),1),:)).^2 ./ (CC.VAR(repmat(k,size(D,1),1),:));
            z = z + log(CC.VAR(repmat(k,size(D,1),1),:)); // + log(2*%pi);
            d(:,k) = sum(-z/2, 2) + log(nan_mean(CC.N(k,:)));
        end;
        d = exp(d-log(nan_mean(sum(CC.N,1)))-log(2*%pi)/2);


    elseif (CC.datatype=="classifier:nbc")
        //////// Naive Bayesian Classifier ////////
        d = repmat(%nan,size(D,1),size(CC.MEAN,1));
        for k = 1:size(CC.MEAN,1)
            z = (D - CC.MEAN(repmat(k,size(D,1),1),:)).^2 ./ (CC.VAR(repmat(k,size(D,1),1),:));
            z = z + log(CC.VAR(repmat(k,size(D,1),1),:)); // + log(2*%pi);
            d(:,k) = sum(-z/2, 2) + log(nan_mean(CC.N(k,:)));
        end;
        d = exp(d-log(nan_mean(sum(CC.N,1)))-log(2*%pi)/2);


    elseif (CC.datatype=="classifier:anbc")
        //////// Augmented Naive Bayesian Classifier ////////
        d = repmat(%nan,size(D,1),size(CC.MEAN,1));
        for k = 1:size(CC.MEAN,1)
            z = (D*CC.V - CC.MEAN(repmat(k,size(D,1),1),:)).^2 ./ (CC.VAR(repmat(k,size(D,1),1),:));
            z = z + log(CC.VAR(repmat(k,size(D,1),1),:)); // + log(2*%pi);
            d(:,k) = sum(-z/2, 2) + log(nan_mean(CC.N(k,:)));
        end;
        d = exp(d-log(nan_mean(sum(CC.N,1)))-log(2*%pi)/2);


    elseif (CC.datatype=="classifier:statistical:rda")
        // Friedman (1989) Regularized Discriminant analysis
        //if isfield(CC,'hyperparameter') & isfield(CC.hyperparameter,'lambda')  & isfield(CC.hyperparameter,'gamma')
        if or(CCfields(3:$)=="hyperparameter")
            allfields=getfield(1,CC.hyperparameter);
            if or(allfields(3:$)=="lambda") & or(allfields(3:$)=="gamma")
                D = [ones(size(D,1),1),D];  // add 1-column
                lambda = CC.hyperparameter.lambda;
                CCgamma  = CC.hyperparameter.gamma;
                d = repmat(%nan,size(D,1),size(CC.MD,1));
                ECM = CC.MD./CC.NN;
                NC = size(ECM);
                ECM0 = moc_squeeze(sum(ECM,3));  //decompose ECM
                [M0,sd,COV0] = nan_decovm(ECM0);
                for k = 1:NC(3);
                    [M,sd,s,xc,N] = nan_decovm(moc_squeeze(ECM(:,:,k)));
                    s = ((1-lambda)*N*s+lambda*COV0)/((1-lambda)*N+lambda);
                    s = (1-CCgamma)*s+CCgamma*(trace(s))/(NC(2)-1)*eye(NC(2)-1,NC(2)-1);
                    ir  = [-M;eye(NC(2)-1,NC(2)-1)]*inv(s)*[-M',eye(NC(2)-1,NC(2)-1)];  // inverse correlation matrix extended by mean
                    d(:,k) = -sum((D*ir).*D,2); // calculate distance of each data point to each class
                end;
            else
                ieee(mod);
                error("QDA: hyperparamters lambda and/or gamma not defined")

            end;
        else
            ieee(mod);
            error("QDA: hyperparamters lambda and/or gamma not defined")
        end;


    elseif (CC.datatype=="classifier:csp")
        d = moc_filtfilt(CC.FiltB,CC.FiltA,(D*CC.csp_w).^2);
        R = nan_test_sc(CC.CSP,log(d));	// LDA classifier of
        d = R.output;


    elseif (CC.datatype=="classifier:svm:lib:1vs1") | (CC.datatype=="classifier:svm:lib:rbf") ;
        try
            if type(svmpredict)~=130
                ieee(mod);
                error("No SVM training algorithm available. Install libsvm for scilab.\n");
            end;
        catch
            ieee(mod);
            error("No SVM training algorithm available. Install libsvm for scilab.\n");
        end

        cl = svmpredict(ones(size(D,1),1), D, CC.model);   //Use the classifier

        //Create a pseudo tsd matrix for bci4eval
        d = zeros(size(D,1), CC.model.nr_class);
        for i = 1:size(cl,1)
            d(i,cl(i)) = 1;
        end

    elseif (CC.datatype=="classifier:svm:lin4") ;

        try
            if type(predict)~=130
                ieee(mod);
                error("No SVM training algorithm available. Install libsvm for scilab.\n");
            end;
        catch
            ieee(mod);
            error("No SVM training algorithm available. Install libsvm for scilab.\n");
        end
        cl = predict(ones(size(D,1),1), sparse(D), CC.model);   //Use the classifier

        //Create a pseudo tsd matrix for bci4eval
        d = zeros(size(D,1), CC.model.nr_class);
        for i = 1:size(cl,1)
            d(i,cl(i)) = 1;
        end

        //elseif isfield(CC,'weights'); //strcm%pi(t2,'svm') | (strcm%pi(t2,'statistical') & strncm%pi(t3,'ld',2)) ;
    elseif  or(CCfields(3:$)=="weights")
        // linear classifiers like: LDA, SVM, LPM
        //d = [ones(size(D,1),1), D] * CC.weights;
        d = repmat(%nan,size(D,1),size(CC.weights,2));
        for k = 1:size(CC.weights,2),
            d(:,k) = D * CC.weights(2:$,k) + CC.weights(1,k);
        end;


    elseif ~isempty(POS1)	// GSVD, sparse & DELETION
        CC.datatype = part(CC.datatype,(1:POS1(1)-1));
        r = nan_test_sc(CC, D*sparse(CC.G));
        d = r.output;


    elseif (t2=="statistical");
        if isempty(TYPE)
            TYPE.TYPE = convstr(t3,"u");
        end;
        D = [ones(size(D,1),1),D];  // add 1-column
        ccmd_size=size(CC.MD);
        if size(ccmd_size,"*")>2
            W = repmat(%nan, size(D,2), ccmd_size(3));
        else
            W = repmat(%nan, size(D,2), 1);
        end;

        if 0,
        elseif (TYPE.TYPE=="LD2"),
            //d = ldbc2(CC,D);
            ECM = CC.MD./CC.NN;
            NC = size(ECM);
            ECM0 = moc_squeeze(sum(ECM,3));  //decompose ECM
            [M0] = nan_decovm(ECM0);
            for k = 1:NC(3);
                ecm = moc_squeeze(ECM(:,:,k));
                [M1,sd,COV1] = nan_decovm(ECM0-ecm);
                [M2,sd,COV2] = nan_decovm(ecm);
                w     = (COV1+COV2)\(M2'-M1')*2;
                w0    = -M0*w;
                W(:,k) = [w0; w];
            end;
            d = D*W;
        elseif (TYPE.TYPE=="LD3") | (TYPE.TYPE=="FLDA");
            //d = ldbc3(CC,D);
            ECM = CC.MD./CC.NN;
            NC = size(ECM);
            ECM0 = moc_squeeze(sum(ECM,3));  //decompose ECM
            [M0,sd,COV0] = nan_decovm(ECM0);
            for k = 1:NC(3);
                ecm = moc_squeeze(ECM(:,:,k));
                [M1] = nan_decovm(ECM0-ecm);
                [M2] = nan_decovm(ecm);
                w     = COV0\(M2'-M1')*2;
                w0    = -M0*w;
                W(:,k) = [w0; w];
            end;
            d = D*W;
        elseif (TYPE.TYPE=="LD4");
            //d = ldbc4(CC,D);
            ECM = CC.MD./CC.NN;
            NC = size(ECM);
            ECM0 = moc_squeeze(sum(ECM,3));  //decompose ECM
            M0 = nan_decovm(ECM0);
            for k = 1:NC(3);
                ecm = moc_squeeze(ECM(:,:,k));
                [M1,sd,COV1,xc,N1] = nan_decovm(ECM0-ecm);
                [M2,sd,COV2,xc,N2] = nan_decovm(ecm);
                w     = (COV1*N1+COV2*N2)\((M2'-M1')*(N1+N2));
                w0    = -M0*w;
                W(:,k) = [w0; w];
            end;
            d = D*W;
        elseif (TYPE.TYPE=="MDA");
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = -sum((D*CC.IR(k)).*D,2); // calculate distance of each data point to each class
            end;
        elseif (TYPE.TYPE=="MD2");
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = sum((D*CC.IR(k)).*D,2); // calculate distance of each data point to each class
            end;
            d = -sqrt(d);
        elseif (TYPE.TYPE=="GDBC");
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = sum((D*CC.IR(k)).*D,2) + CC.logSF7(k); // calculate distance of each data point to each class
            end;
            d = exp(-d/2);
        elseif (TYPE.TYPE=="MD3");
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = sum((D*CC.IR(k)).*D,2) + CC.logSF7(k); // calculate distance of each data point to each class
            end;
            d = exp(-d/2);
            d = d./repmat(sum(d,2),1,size(d,2));  // Zuordungswahrscheinlichkeit [1], p.601, equ (18.39)
        elseif (TYPE.TYPE=="QDA");
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                // [1] (18.33) QCF - quadratic classification function
                d(:,k) = -(sum((D*CC.IR(k)).*D,2) - CC.logSF5(k));
            end;
        elseif (TYPE.TYPE=="QDA2");
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                // [1] (18.33) QCF - quadratic classification function
                d(:,k) = -(sum((D*(CC.IR(k))).*D,2) + CC.logSF4(k));
            end;
        elseif (TYPE.TYPE=="GRB");     // Gaussian RBF
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = sum((D*CC.IR(k)).*D,2); // calculate distance of each data point to each class
            end;
            d = exp(-sqrt(d)/2);
        elseif (TYPE.TYPE=="GRB2");     // Gaussian RBF
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = sum((D*CC.IR(k)).*D,2); // calculate distance of each data point to each class
            end;
            d = exp(-d);
        elseif (TYPE.TYPE=="MQU");     // Multiquadratic
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = sum((D*CC.IR(k)).*D,2); // calculate distance of each data point to each class
            end;
            d = -sqrt(1+d);
        elseif (TYPE.TYPE=="IMQ");     // Inverse Multiquadratic
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = sum((D*CC.IR(k)).*D,2); // calculate distance of each data point to each class
            end;
            d = (1+d).^(-1/2);
        elseif (TYPE.TYPE=="Cauchy");     // Cauchy RBF
            d = repmat(%nan,size(D,1),length(CC.IR));
            for k = 1:length(CC.IR);
                d(:,k) = sum((D*CC.IR(k)).*D,2); // calculate distance of each data point to each class
            end;
            d = 1 ./(1+d);
        else
            ieee(mod);
            error(sprintf("Classifier %s not supported. see HELP TRAIN_SC for supported classifiers.",TYPE.TYPE));
        end;
    else
        ieee(mod);
        error("Error TEST_SC: unknown classifier\n");
    end;

    if size(d,2)>1,
        [tmp,cl] = mtlb_max(d,[],2);
        cl = CC.Labels(cl);
        cl(isnan(tmp)) = %nan;
    elseif size(d,2)==1,
        cl = (d<0) + 2*(d>0);
        cl(isnan(d)) = %nan;
    end;

    R.output = d;
    R.classlabel = cl;

    if nargin>2 & ~isempty(classlabel),
        [R.kappa,R.sd,R.H,z,R.ACC] = nan_kappa(classlabel(:),cl(:));
    end;
    ieee(mod);
endfunction





