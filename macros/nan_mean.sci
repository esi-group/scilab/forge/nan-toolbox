function [y]=nan_mean(x,DIM,opt,W)
    //  calculates the mean of data elements.
    //  Calling Sequence
    //	nan_mean(x)
    //	nan_mean(x,DIM)
    //	nan_mean(x,opt)
    //	nan_mean(x,opt,DIM)
    //	nan_mean(x,DIM,opt)
    //	nan_mean(x,DIM,W)
    //	nan_mean(x,DIM,opt,W);
    // Parameters
    // DIM:	dimension
    //	1 MEAN of columns
    //	2 MEAN of rows
    // 	N MEAN of  N-th dimension
    //	default or []: first DIMENSION, with more than 1 element
    //
    // opt:	options
    //	'A' arithmetic mean
    //	'G' geometric mean
    //	'H' harmonic mean
    //
    // W:	weights to compute weighted mean (default: [])
    //	if W=[], all weights are 1.
    //	number of elements in W must match size(x,DIM)
    //
    // Description
    // features:
    //
    // - can deal with NaN's (missing values)
    //
    // - weighting of data
    //
    // - dimension argument also in Octave
    //
    // Examples
    // X=testmatrix('magi',3)
    // X([1 6:9]) = %nan*ones(1,5)
    //
    // y = nan_mean(X)
    //
    // See also
    // sumskipnan
    // nan_geomean
    // nan_harmmean
    // Authors
    //	Copyright (C) 2000-2004,2008,2009 by Alois Schloegl a.schloegl@ieee.org
    //	H. Nahrstaedt - 2010


    //	$Id$
    //	Copyright (C) 2000-2004,2008,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //    	This is part of the NaN-toolbox. For more details see
    //       http://biosig-consulting.com/matlab/NaN/
    //
    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    mod=ieee();
    ieee(2);
    if nargin==1,
        //------ case:  mean(x)
        W = [];
        DIM=[];
        opt="a";
    elseif (nargin==2)
        W = [];
        //if ~isnumeric(DIM), //>=65;//abs('A'),
        if type(DIM)==10 //abs('A'),
            //------ case:  mean(x,opt)
            opt=DIM;
            DIM=[];
        else
            //------ case:  mean(x,DIM)
            opt="a";
        end;
    elseif (nargin == 3),
        if or(type(DIM)==[1 5 8]) & or(type(opt)==[1 5 8])
            //------ case:  mean(x,DIM,W)
            W = opt;
            opt="a";
        elseif type(DIM)==10 //abs('A'),
            //------ case:  mean(x,opt,DIM)
            //if ~isnumeric(DIM), //>=65;//abs('A'),
            tmp=opt;
            opt=DIM;
            DIM=tmp;
            W = [];
        else
            //------ case:  mean(x,DIM,opt)
            W = [];
        end;
    elseif nargin==4,
        //------ case: mean(x,DIM,opt,W)
        ;
    else
        ieee(mod);
        //error("At least one argument is needed!");
        error("usage: nan_mean(x) or nan_mean(x,DIM) or nan_mean(x,opt,DIM) or nan_mean(x,DIM,opt) or nan_mean(x,DIM,W) or nan_mean(x,DIM,opt,W); ");
    end;
    if isempty(opt)
        opt = "A";
    end;
    //if or(opt=='aAgGhH')
    opt = convstr(opt,"u"); // eliminate old version
    //else
    //	error('Error MEAN: invalid opt argument');
    //end;

    if  (opt == "A")
        [y, n] = sumskipnan(x,DIM,W);
        y = y./n;
    elseif (opt == "G")
        [y, n] = sumskipnan(log(x),DIM,W);
        y = exp (y./n);
    elseif (opt == "H")
        [y, n] = sumskipnan(1 ./x,DIM,W);
        y = n./y;
    else
        printf ("nan_mean:  option `%s` not recognized", opt);
    end
    ieee(mod);
endfunction
