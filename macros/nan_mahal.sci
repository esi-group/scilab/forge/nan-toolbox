function [d] = nan_mahal(X,Y)
    // return the Mahalanobis' D-square distance
    // Calling Sequences
    //  d = mahal(X,Y)
    // Description
    //   d(k) = (X(k,:)-MU)*inv(SIGMA)*(X(k,:)-MU)'
    //
    //   where MU and SIGMA are the mean and the covariance matrix of Y
    //
    // return the Mahalanobis' D-square distance between the
    // multivariate samples x and y, which must have the same number
    // of components (columns), but may have a different number of observations (rows).
    // See also
    // nan_train_sc
    // nan_test_sc
    // nan_covm
    // Authors
    // Copyright (C) 2009 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010


    //	$Id: train_sc.m 2140 2009-07-02 12:03:55Z schloegl $
    //	Copyright (C) 2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    // This program is free software; you can redistribute it and/or
    // modify it under the terms of the GNU General Public License
    // as published by the Free Software Foundation; either version 2
    // of the  License, or (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program; if not, write to the Free Software
    // Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
    sx = size(X);
    sy = size(Y);

    if sx(2)~=sy(2),
        error("number of columns of X and Y do not fit");
    end;
    mod=ieee();
    ieee(2);
    IR = inv(nan_covm(Y,"E"));
    IR(1,1) = IR(1,1)-1;

    D = [ones(size(X,1),1),X];
    d = sum((D*IR).*D,2);
    ieee(mod);
endfunction
