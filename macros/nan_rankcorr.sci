function r = nan_rankcorr(X,Y)
    // calculated the rank correlation coefficient.
    // Calling Sequence
    // r = nan_rankcorr(X,Y)
    // Description
    // This function is replaced by corrcoef.
    // Significance test and confidence intervals can be obtained from corrcoef, too.
    //
    // R = nan_corrcoef(X, [Y, ] 'Rank');
    //
    // The rank correlation   r = corrcoef(ranks(x)).
    // is often confused with Spearman's rank correlation.
    // Spearman's correlation is defined as
    //
    //   r(x,y) = 1-6*sum((ranks(x)-ranks(y)).^2)/(N*(N*N-1))
    //
    // The results are different. Here, the former version is implemented.
    //
    // See also
    // nan_corrcoef
    // nan_spearman
    // nan_ranks
    // Bibliography
    // [1] http://mathworld.wolfram.com/SpearmanRankCorrelationCoefficient.html
    // [2] http://mathworld.wolfram.com/CorrelationCoefficient.html
    // Authors
    //    Copyright (C) 2000-2003 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt

    //    $Id$
    //    Copyright (C) 2000-2003 by Alois Schloegl <a.schloegl@ieee.org>
    //    This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    // warning('RANKCORR might become obsolete; use CORRCOEF(ranks(x)) or CORRCOEF(...,''Rank'') instead');

    if nargin < 2
        r = nan_corrcoef(nan_ranks(X));
    else
        r = nan_corrcoef(nan_ranks(X),nan_ranks(Y));
    end
endfunction
