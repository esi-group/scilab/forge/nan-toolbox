function y=nan_var(x,opt,DIM,W)
    // calculates the variance.
    //  Calling Sequence
    //	y=nan_var(x)
    //	y=nan_var(x, opt, DIM)
    //	y=nan_var(x, [], DIM)
    //	y=nan_var(x, W, DIM)
    //	y=nan_var(x, opt, DIM, W)
    // Parameters
    // opt :  0: normalizes with N-1 [default]
    //	1: normalizes with N
    // DIM :	dimension
    //	1: VAR of columns
    //	2: VAR of rows
    // 	N: VAR of  N-th dimension
    //	default or []: first DIMENSION, with more than 1 element
    // W :	weights to compute weighted variance (default: [])
    //	if W=[], all weights are 1.
    //	number of elements in W must match size(x,DIM)
    // y: variance in dimension DIM, the default DIM is the first non-single dimension
    // Description
    // features:
    // - can deal with NaN's (missing values)
    // - weighting of data
    // - dimension argument
    // - compatible to Matlab and Octave
    //
    // Examples
    // X=testmatrix('magi',3)
    // X([1 6:9]) = %nan*ones(1,5);
    // X  =
    //    Nan   1.    Nan
    //    3.    5.    Nan
    //    4.    Nan   Nan
    //  y = nan_var(X)
    //  y =
    //  0.5    8.    Nan
    // See also
    // nan_meansq
    // nan_sumsq
    // sumskipnan
    // nan_mean
    // nan_rms
    // nan_std
    // Authors
    //	Copyright (C) 2000-2003,2006,2009,2010 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.

    //	$Id$
    //	Copyright (C) 2000-2003,2006,2009,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This is part of the NaN-toolbox for Octave and Matlab
    //       http://biosig-consulting.com/matlab/NaN/
    mod=ieee();
    ieee(2);
    if nargin<3,
        DIM = [];
    end;

    if nargin==1,
        W = [];
        opt = [];

    elseif or(nargin==[2,3])
        if (length(opt)<2),
            W = [];
        else
            W = opt;
            opt = [];
        end;
    elseif (nargin==4) & (length(opt)<2) & (length(DIM)<2),
        ;
    else
        ieee(mod);
        error("Error nan_var: incorrect usage\n");

    end;

    if isempty(opt),
        opt = 0;
    end;

    if isempty(DIM),
        DIM = find(size(x)>1,1);
        if isempty(DIM), DIM=1; end;
    end;

    [y,n,ssq] = sumskipnan(x,DIM,W);
    if and(ssq(:).*n(:) > 2*(y(:).^2)),
        //// rounding error is neglectable
        y = ssq - y.*y./n;
    else
        //// rounding error is not neglectable
        szx = size(x);
        szy = size(y);
        if max(size(szy))<max(size(szx));
            szy(max(size(szy))+1:max(size(szx))) = 1;
        end;
        [y,n] = sumskipnan((x-repmat(y./n,szx./szy)).^2,DIM,W);
    end;

    if (opt~=1)
        n = max(n-1,0);			// in case of n=0 and n=1, the (biased) variance, STD and STE are INF
    end;
    y = y./n;	// normalize
    ieee(mod);
endfunction
