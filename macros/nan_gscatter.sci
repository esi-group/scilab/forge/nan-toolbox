function [h] = nan_gscatter(x,y,groups,clr,sym,siz,doleg,x_name,y_name)
    // scatter plot of groups
    // Calling Sequence
    //  nan_gscatter(x,y,group)
    //  nan_gscatter(x,y,group,clr,sym,siz)
    //  nan_gscatter(x,y,group,clr,sym,siz,doleg)
    //  nan_gscatter(x,y,group,clr,sym,siz,doleg,xname,yname)
    //  h = nan_gscatter(...)
    // Parameters
    //  x,y, group: 	vectors with equal length
    //  clr: 	color vector, default 'bgrcmyk'
    //  sym:		symbol, default '.'
    //  siz: 	size of Marker
    //  doleg:  'on' (default) shows legend, 'off' turns of legend
    //  xname, yname: name of axis
    //  Examples
    //      loadmatfile(nan_getpath()+"/demos/data/iris.mat");
    //      nan_gscatter(meas(:,1),meas(:,2),species)
    //
    // See also
    // nan_ecdf
    // nan_cdfplot
    // Authors
    // 	Copyright (C) 2009 by Alois Schloegl a.schloegl@ieee.org
    //  H. Nahrstaedt - 2010


    //	$Id$
    //	Copyright (C) 2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    // This program is free software; you can redistribute it and/or
    // modify it under the terms of the GNU General Public License
    // as published by the Free Software Foundation; either version 3
    // of the  License, or (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program; if not, write to the Free Software
    // Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
    if  ~isempty(groups)
        [groups] = nan_mgrp2idx(groups,size(x,1),",");
    end
    [b,j] = unique(groups);

    if nargin<3
        error("At least 3 arguments are needed!")
    end;
    if nargin<4, clr = [];end
    if nargin<5, sym = [];end
    if nargin<6, siz = [];end
    if nargin<7, doleg = [];end
    if nargin<8, x_name = [];end
    if nargin<9, y_name = [];end;

    if isempty(clr), clr="bgrcmyk"; end;
    if isempty(sym), sym="."; end;
    if isempty(doleg), doleg="on"; end;



    for k=1:length(b);
        //ix = find(k==j);
        c = part(clr,(pmodulo(k-1,length(clr))+1));
        s = part(sym,(pmodulo(k-1,length(sym))+1));
        plot(x(groups==b(k)),y(groups==b(k)),[c+s]);
        hh(k)=gce();
        if ~isempty(siz)
            z = siz(pmodulo(k-1,length(siz))+1);
            e=gce();
            e.children(1).mark_size=z;
            //set(hh(k),'MarkerSize',z);
        end
        //hold on;
    end;
    //hold off;

    if ~(doleg=="off")
        // 	if or(type(b)==[1 5 8])
        // 		b=string(b(:));
        // 	end;
        legend(string(b(:)));
    end;
    if ~isempty(x_name)
        xlabel(x_name);
    end;
    if ~isempty(y_name)
        ylabel(y_name);
    end;

    if nargout>0,
        h = hh;
    end;
endfunction
