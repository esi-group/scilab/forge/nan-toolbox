function R=nan_histo2(Y,W)
    //  calculates histogram for multiple columns with separate bin values  for each data column.
    // Calling Sequence
    // R=nan_histo2(Y)
    // R = nan_histo2(Y, W)
    // Parameters
    //      Y :	data
    //      W :	weight vector containing weights of each sample, number of rows of Y and W must match. default W=[] indicates that each sample is weighted with 1.
    // 	R : is a struct with th fields
    //       R.X : the bin-values, bin-values are computed separately for each data column, thus R.X is a matrix, each column contains the 	the bin values of for each data column, unused elements are indicated with NaN.	In order to have common bin values, use nan_histo3.
    //       R.H : is the frequency of occurence of value X
    //  	R.N  :  are the number of valid (not NaN) samples (i.e. sum of weights)
    // Description
    // more histogram-based results can be obtained by hist2res2
    // Examples
    // x= [ 9 9 9 9 2 2 3 3 4 5 9 ]';
    // R=nan_histo2(x);
    // H=nan_hist2res(R);
    // Bibliography
    //  C.E. Shannon and W. Weaver "The mathematical theory of communication" University of Illinois Press, Urbana 1949 (reprint 1963).
    // See also
    // nan_hist2res
    // nan_histo
    // nan_histo3
    // nan_histo4
    // Authors
    //	Copyright (C) 1996-2002,2008 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //	$Id: histo2.m 5090 2008-06-05 08:12:04Z schloegl $
    //	Copyright (C) 1996-2002,2008 by Alois Schloegl <a.schloegl@ieee.org>
    //    	This is part of the TSA-toolbox
    //	http://hci.tugraz.at/~schloegl/matlab/tsa/
    //
    //    This program is free software: you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation, either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    //////// check input arguments /////
    if (nargin == 0),
        error("At least 1 parameter required");
    end;

    [yr,yc] = size(Y);

    if nargin < 2,
        W = [];
    end;
    if ~isempty(W) & (yr ~= max(size(W))),
        error("number of rows of Y does not match number of elements in W");
    end;
    mod=ieee();
    ieee(2);
    ////// identify all possible X's and generate overall Histogram ////
    N   = sum(~isnan(Y),1);
    NN = N;
    if isempty(W)
        sY  = mtlb_sort(Y,1);
    else
        [sY, idx] = mtlb_sort(Y,1);
        W = cumsum(W(idx));     // W becomes cumulative sum
    end;
    [ix,iy] = find( diff(sY, 1, 1) > 0);
    nn0 = 0;


    for k=1:yc,
        tmp    = [ix(iy==k), N(k)]';
        nn1    = max(size(tmp));

        if isempty(W)
            H(1:nn1,k) = [tmp(1); diff(tmp)];
        else
            //// Note that W is the cumulative sum
            H(1:nn1,k) = [W(tmp(1),k); diff(W(tmp,k))];
            NN(k) = W(N(k), k);
        end;
        X(1:nn1,k) = sY(tmp,k);

        if k==1;
            nn0 = nn1;
        elseif nn1 < nn0,
            H(1+nn1:nn0,k) = %nan;
            X(1+nn1:nn0,k) = %nan;
        elseif nn1>nn0,
            H(1+nn0:nn1,1:k-1) = %nan;
            X(1+nn0:nn1,1:k-1) = %nan;
            nn0 = nn1;
        end;
    end;

    R.datatype = "HISTOGRAM";
    R.H = H;
    R.X = X;
    R.N = NN;

    ieee(mod);
endfunction
