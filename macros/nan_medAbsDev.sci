function [D, M] = nan_medAbsDev(X, DIM)
    //  calculates the median absolute deviation
    // Calling Sequence
    //   D = nan_medAbsDev(X, DIM)
    //   [D, M] = nan_medAbsDev(X, DIM)
    // Parameters
    // Input:
    //         X  : data
    //         DIM: dimension along which mad should be calculated (1=columns, 2=rows)
    //               (optional, default=first dimension with more than 1 element
    // Output:
    //  D  : median absolute deviations
    //  M  : medians (optional)
    //  Examples
    // salery=[950; 1200; 1370; 1580; 1650; 1800; 6000; %nan];
    // nan_medAbsDev(salery);
    // See also
    // nan_meanAbsDev
    // nan_median
    //Authors
    //	Copyright (C) 2003 Patrick Houweling
    //	Copyright (C) 2009 Alois Schloegl
    // H. Nahrstaedt - 2010

    //	$Id$
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software: you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation, either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    // input checks
    if or(size(X)==0),
        error("or(size(X)==0)");
    end;
    mod=ieee();
    ieee(2);
    if nargin<2,
        M = nan_median(X);
        // median absolute deviation: median of absolute deviations to median
        D = nan_median(abs(X - repmat(M, size(X)./size(M))));
    else
        M = nan_median(X, DIM);
        // median absolute deviation: median of absolute deviations to median
        D = nan_median(abs(X - repmat(M, size(X)./size(M))), DIM);
    end;
    ieee(mod);
endfunction
