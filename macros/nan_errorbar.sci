function nan_errorbar(x,y,em,ep,prop,direction)
    // This function put an errobar range onto plot
    // Calling Sequence
    //   errobar_plus(x,y,e)
    //   errobar_plus(x,y,em,ep)
    //   errobar_plus(...,prop)
    //   errobar_plus(...,prop,dir)
    //   Parameters
    //   prop :  property string like for plot()
    //   dir :   vertical or horizontal direction of error
    // Examples
    //  t=[0:0.1:2*%pi]';
    //  y=[sin(t) cos(t)]; x=[t t];
    //  plot2d(x,y)
    //  nan_errorbar(x,y,0.05*ones(x),0.03*ones(x))
    //
    //        x = 1:10;
    //        y = sin(x);
    //        e = nan_std(y)*ones(x);
    //        nan_errorbar(x,y,e,'r^')
    //     draws symmetric error bars of unit standard deviation.'
    //     Authors
    //     By Nguyen Chuong 2004/06/30

    if exists("ep")
        if type(ep)==10 then
            if exists("prop") then
                direction=prop;
            end;
            prop=ep;
            ep=em;
        end;

    else
        ep=em;
    end;

    if exists("prop")
        color_list="bgrcmyk";
        mark_list=".ox+*sdv^<>ph";
        col="b";
        for j=1:length(prop)
            for i=1:length(color_list)
                if part(prop,j)==part(color_list,i)
                    col=part(color_list,i);
                    break
                end
            end
            if part(prop,j)==part(color_list,i)
                break
            end
        end
        mark="i";
        for j=1:length(prop)
            for i=1:length(mark_list)
                if part(prop,j)==part(mark_list,i)
                    mark=part(mark_list,i);
                    break
                end
            end
            if part(prop,j)==part(mark_list,i)
                break
            end
        end
    else
        col="b"; //default values
        mark="i";
    end
    if (exists("direction")) & (direction ~= "v") & (direction ~= "h")
        direction = "v"; //default value
    elseif ~exists("direction") // not exist
        direction = "v"; //default value
    end
    dx=(x(1)-x($))/length(x)/12; // for the case mark='i' only
    dy=(y(1)-y($))/length(y)/12; // for the case mark='i' only
    // hold on
    // drawlater();
    for i=1:length(x)
        if direction == "v" // vertical errobar
            plot([x(i) x(i)], [y(i)-em(i) y(i)+ep(i)],col)
            select convstr(mark)
            case "i" then
                plot([x(i)-dx x(i)+dx], [y(i)-em(i) y(i)-em(i)],col)
                plot([x(i)-dx x(i)+dx], [y(i)+ep(i) y(i)+ep(i)],col)
            case "^" then
                plot(x(i), y(i)-em(i),[col+"v"])
                plot(x(i), y(i)+ep(i),[col+"^"])
            case "v" then
                plot(x(i), y(i)-em(i),[col+"^"])
                plot(x(i), y(i)+ep(i),[col+"v"])
            else
                plot([x(i) x(i)], [y(i)-em(i) y(i)+ep(i)],[col+mark])
            end
        else    //horizontal errorbar
            plot([x(i)-em(i) x(i)+ep(i)], [y(i) y(i)],col)
            select convstr(mark)
            case "i" then
                plot([x(i)-em(i) x(i)-ep(i)], [y(i)-dy y(i)+dy],col)
                plot([x(i)+em(i) x(i)+ep(i)], [y(i)-dy y(i)+dy],col)
            case "<" then
                plot(x(i)-em(i), y(i),[col+"<"])
                plot(x(i)+ep(i), y(i),[col+">"])
            case ">" then
                plot(x(i)-em(i), y(i),[col+">"])
                plot(x(i)+ep(i), y(i),[col+"<"])
            else
                plot([x(i)-em(i) x(i)+ep(i)], [y(i) y(i)],[col+mark])
            end
        end
    end
    // hold off
    // drawnow();
endfunction