function [i,S] = nan_center(i,DIM,W)
    //  removes the mean
    // Calling Sequence
    // [z,mu] = nan_center(x,DIM,W)
    // Parameters
    // z: x with removed mean  along dimension DIM
    // mu : mean
    // x :	input data
    // DIM:	dimension
    //	1: column
    //	2: row
    //	default or []: first DIMENSION, with more than 1 element
    // W:	weights to computed weighted mean (default: [], all weights = 1)
    //	numel(W) must be equal to size(x,DIM)
    // Description
    // features:
    // - can deal with NaN's (missing values)
    // - weighting of data
    // - dimension argument
    // - compatible to Matlab and Octave
    // Examples
    // x=[0.2113249  0.0002211 0.6653811;
    //   0.7560439  0.3303271 0.6283918]
    //   s=nan_center(x)
    // See also
    // sumskipnan
    // nan_mean
    // nan_std
    // nan_detrend
    // nan_zscore
    // Authors
    //       Copyright (C) 2000-2003,2005,2009 by Alois Schloegl a.schloegl@ieee.org
    //       H. Nahrstaedt - 2010


    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.


    //       $Id$
    //       Copyright (C) 2000-2003,2005,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This is part of the NaN-toolbox. For more details see
    //       http://biosig-consulting.com/matlab/NaN/

    if or(size(i)==0) then
        error("or(size(x)==0)");
    end;

    if nargin < 3 then
        W = [];
    end;

    if nargin > 1 then
        [S,N] = sumskipnan(i,DIM,W);
    else
        [S,N] = sumskipnan(i,[],W);
    end;

    S = S./N;

    szi = size(i);
    szs = size(S);

    if length(szs) < length(szi) then
        szs(length(szs)+1:length(szi)) = 1;
    end;

    i = i - repmat(S,szi./szs);		// remove mean
endfunction
