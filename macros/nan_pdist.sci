function y = nan_pdist (x, metric, varargin)
    // Return the distance between any two rows in x.
    // Calling Sequence
    //  y = pdist (x)
    //  y = pdist (x, metric)
    //  y = pdist (x, metric_function)
    //  y = pdist (x, metric, metricarg, ...)
    //  Parameters
    //  x: x is the n x d matrix representing q row vectors of size d
    //  y: output
    //  metric: metric is an optional argument specifying how the distance is  computed It can be any of the following ones, defaulting to  "euclidean"
    //  "euclidean":  Euclidean distance (default).
    //   "seuclidean":  Standardized Euclidean distance. Each coordinate in the sum of squares is inverse weighted by the sample variance of that  coordinate.
    //   "mahalanobis": Mahalanobis distance: see also mahalanobis.
    //   "cityblock": City Block metric, aka Manhattan distance.
    //   "minkowski": Minkowski metric.  Accepts a numeric parameterp: for p=1  this is the same as the cityblock metric, with p=2 (default) it   is equal to the euclidean metric.
    //  "cosine"; One minus the cosine of the included angle between rows, seen as  vectors.
    //  "correlation":  One minus the sample correlation between points (treated as  sequences of values).
    //   "spearman":  One minus the sample Spearman's rank correlation between  observations, treated as sequences of values.
    //   "hamming": Hamming distance: the quote of the number of coordinates that differ.
    //   "jaccard": One minus the Jaccard coefficient, the quote of nonzero coordinates that differ.
    //  "chebychev":  Chebychev distance: the maximum coordinate difference.
    //
    //  Description
    //  Return the distance between any two rows in x.
    //
    //  The output is a dissimilarity matrix formatted as a row vector
    //  y, (n-1)*n/2 long, where the distances are in
    //  the order [(1, 2) (1, 3) ... (2, 3) ... (n-1, n)].  You can
    //  use the squareform function to display the distances between
    //  the vectors arranged into an nxn matrix.
    //
    //  metric is an optional argument specifying how the distance is
    //  computed. It can be any of the following ones, defaulting to
    //  "euclidean", or a user defined function that takes two arguments
    //  x and y plus any number of optional arguments,
    //  where x is a row vector and and y is a matrix having the
    //  same number of columns as x.  metric returns a column
    //  vector where row i is the distance between x and row
    //  i of y. Any additional arguments after the metric
    //  are passed as metric (x, y, metricarg1,  metricarg2 ...).
    //
    //  Predefined distance functions are:
    //
    //
    //  "
    //  Examples
    //  // Compute the  Euclidean distance.
    //  X = rand(100, 5,"norm");
    //  D = nan_pdist(X,'euclidean');
    //
    // // Compute the Euclidean distance with each coordinate difference scaled by the standard deviation.
    //  Dstd = nan_pdist(X,'seuclidean');
    //  See also
    //  nan_linkage
    //  Authors
    //  H. Nahrstaedt - 2011
    //  Francesco Potortïs  pot at gnu.org


    // Copyright (C) 2008  Francesco Potortï¿½  <pot@gnu.org>
    // Modified to run on MATLAB by Mike Croucher (May 2010)
    // www.walkingrandomly.com
    //
    // This program is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 3, or (at your option)
    // any later version.
    //
    // This program is distributed in the hope that it will be useful, but
    // WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    // General Public License for more details.
    //
    //  You should have received a copy of the GNU General Public License
    //  along with this software; see the file COPYING.  If not, see
    //  <http://www.gnu.org/licenses/>.
    //

    if (nargin < 1)
        error("Octave:argChk",["Invalid call to pdist.  Correct usage is:\n"...
        "-- Function File: Y = pdist (X)\n" ...
        "-- Function File: Y = pdist (X, METRIC)\n" ...
        "-- Function File: Y = pdist (X, METRIC, METRICARG, ...)\n"])
    elseif ((nargin > 1)    & ~ type (metric)==10      & ~ type (metric)==13)
        error ("pdist: the distance function must be either a string or a function handle.");
    end

    if (nargin < 2)
        metric = "euclidean";
    end

    if (isempty (x))
        error ("pdist: x must be a nonempty matrix");
    elseif (length (size (x)) > 2)
        error ("pdist: x must be 1 or 2 dimensional");
    end

    y = [];
    if (type (metric)==10)
        //order = nchoosek(1:size(x,1),2);
        order=[];
        tmp=1:size(x,1);
        for n=1:size(x,1)-1
            tmp(1)=[];
            for j=1:length(tmp)
                order=[order;n,tmp(j)];
            end;
        end;

        Xi = order(:,1);
        Yi = order(:,2);
        X = x';
        metric = convstr (metric);
        select (metric)
        case "euclidean" then
            diffx = X(:,Xi) - X(:,Yi);
            y = sqrt (nan_sumsq (diffx, 1));

        case "seuclidean" then
            diffx = X(:,Xi) - X(:,Yi);
            weights = inv (diag (nan_var (x, 0,1)));
            y = sqrt (sum ((weights * diffx) .* diffx, 1));

        case "mahalanobis" then
            diffx = X(:,Xi) - X(:,Yi);
            weights = inv (moc_cov (x));
            y = sqrt (sum ((weights * diffx) .* diffx, 1));

        case "cityblock" then
            diffx = X(:,Xi) - X(:,Yi);
            y = sum (abs (diffx), 1);

        case "minkowski" then
            diffx = X(:,Xi) - X(:,Yi);
            p = 2;			// default
            if (nargin > 2)
                p = varargin(1);	// explicitly assigned
            end
            y = (sum ((abs (diffx)).^p, 1)).^(1/p);

        case "cosine" then
            prodx = X(:,Xi) .* X(:,Yi);
            weights = nan_sumsq (X(:,Xi), 1) .* nan_sumsq (X(:,Yi), 1);
            y = 1 - sum (prodx,"r") ./ sqrt (weights);

        case "correlation" then
            if (size (X,1) == 1)
                error ("pdist: correlation distance between scalars not defined")
            end
            xcorr = nan_cor (X);
            y = 1 - xcorr (sub2ind (size (xcorr), Xi, Yi))';

        case "spearman"  then
            if (size (X,1) == 1)
                error ("pdist: spearman distance between scalars not defined")
            end
            xcorr = nan_spearman (X,X);
            y = 1 - xcorr (sub2ind (size (xcorr), Xi, Yi))';

        case "hamming" then
            diffx =  (X(:,Xi) - X(:,Yi))~=0;
            y = sum (diffx, 1) / size (X,1);

        case "jaccard" then
            diffx =  (X(:,Xi) - X(:,Yi))~=0;
            weights = X(:,Xi) | X(:,Yi);
            y = sum (diffx & weights, 1) ./ sum (weights, 1);

        case "chebychev" then
            diffx = X(:,Xi) - X(:,Yi);
            y = max (abs (diffx), "r");
        end
    end

    if (isempty (y))
        // Metric is a function handle or the name of an external function
        l = size (x,1);

        //y = zeros (1, nchoosek (l, 2));
        y = zeros (1, prod ( l : -1 : l-2+1 )/ prod (1:2));
        idx = 1;
        for ii = 1:l-1
            for jj = ii+1:l
                //temp= feval (metric, x(ii,:), x, varargin(:));
                //temp= feval (x(ii,:), x, metric, varargin(:))
                temp=metric(x(ii,:),x);
                y(idx) = temp(jj);
                idx=idx+1;
            end
        end
    end

endfunction

