function o=nan_meansq(x,DIM,W)
    // calculates the mean of the squares
    // Calling Sequence
    // y = nan_meansq(x,DIM,W)
    // Parameters
    // DIM:	dimension
    //	1 STD of columns
    //	2 STD of rows
    // 	N STD of  N-th dimension
    //	default or []: first DIMENSION, with more than 1 element
    // W:	weights to compute weighted mean (default: [])
    //	if W=[], all weights are 1.
    //	number of elements in W must match size(x,DIM)
    // Description
    // features:
    // - can deal with NaN's (missing values)
    // - weighting of data
    // - dimension argument also in Octave
    // - compatible to Matlab and Octave
    //
    // See also
    //nan_sumsq
    //sumskipnan
    //nan_mean
    //nan_var
    //nan_std
    //nan_rms
    //Authors
    //	Copyright (C) 2000-2003,2009 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.

    //	Copyright (C) 2000-2003,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //	$Id$
    //       This function is part of the NaN-toolbox for Octave and Matlab
    //       http://biosig-consulting.com/matlab/NaN/
    if nargin<3,
        W = [];
    end;
    if nargin<2,
        [o,N,ssq] = sumskipnan(x,[],W);
    else
        [o,N,ssq] = sumskipnan(x,DIM,W);
    end;
    mod=ieee();
    ieee(2);
    o = ssq./N;
    ieee(mod);

endfunction
