function [R,tix]=nan_histo3(Y,W)
    // calculates histogram for multiple columns with common bin values  among all data columns, and can be useful for data compression.
    // Calling Sequence
    // R = nan_histo3(Y)
    // R = nan_histo3(Y,W)
    // [R,tix] = nan_histo3(Y)
    // Parameters
    //      Y :	data
    //      W :	weight vector containing weights of each sample, number of rows of Y and W must match. default W=[] indicates that each sample is weighted with 1. R : is a struct with th fields
    //       R :	struct with these fields
    //       R.X :the bin-values, bin-values are equal for each channel thus R.X is a column vector. If bin values should be computed separately for each data column, use nan_histo2
    //       R.H : is the frequency of occurence of value X
    //  	R.N  : are the number of valid (not %nan) samples
    //  	tix : enables compression
    //  	R.tix: provides a compressed data representation.
    //  	R.compressionratio : estimates the compression ratio
    //  	R.X(tix) and R.X(R.tix): reconstruct the orginal signal (decompression)
    // Description
    // Data compression can be performed in this way
    //
    //   	[R,tix] = histo3(Y) 	is the compression step
    //
    // The effort (in memory and speed) for compression is O(n*log(n)).
    //
    // The effort (in memory and speed) for decompression is O(n) only.
    // Examples
    // x= [ 9 9 9 9 2 2 3 3 4 5 9 ]';
    // [R,tix]=nan_histo3(x)
    // disp(R.X(R.tix))
    // Bibliography
    //  C.E. Shannon and W. Weaver "The mathematical theory of communication" University of Illinois Press, Urbana 1949 (reprint 1963).
    // See also
    //    nan_histo
    //    nan_histo2
    //    nan_histo4
    // Authors
    //	Copyright (C) 1996-2002,2008 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010


    //	$Id: histo3.m 5090 2008-06-05 08:12:04Z schloegl $
    //	Copyright (C) 1996-2002,2008 by Alois Schloegl <a.schloegl@ieee.org>
    //       This is part of the TSA-toolbox. See also
    //       http://hci.tugraz.at/schloegl/matlab/tsa/
    //       http://octave.sourceforge.net/
    //       http://biosig.sourceforge.net/
    //
    //    This program is free software: you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation, either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    //////// check input arguments /////
    [yr,yc]=size(Y);
    if nargin < 2,
        W = [];
    end;
    if ~isempty(W) & (yr ~= max(size(W))),
        error("number of rows of Y does not match number of elements in W");
    end;
    mod=ieee();
    ieee(2);
    // identify all possible X's and overall Histogram
    [sY ,idx] =  mtlb_sort(Y(:),1);
    [tmp,idx1] = mtlb_sort(idx);         // generate inverse index

    ix  = diff(sY,1,1) > 0;
    tmp = [find(ix)'; sum(~isnan(sY))];



    R.datatype = "HISTOGRAM";
    R.H = [];
    R.X = sY(tmp);
    R.N = sum(~isnan(Y),1);

    // generate inverse index
    if nargout>1,
        tix = cumsum([1;ix]);	// rank
        tix = matrix(tix(idx1),yr,yc);		// inverse sort rank
        cc  = 1;
        tmp = sum(ix)+1;
        if tmp <= 2^8;
            tix = uint8(tix);
            cc = 8/1;
        elseif tmp <= 2^16;
            tix = uint16(tix);
            cc = 8/2;
        elseif tmp <= 2^32;
            tix = uint32(tix);
            cc = 8/4;
        end;
        R.compressionratio = (prod(size(R.X)) + (yr*yc)/cc) / (yr*yc);
        R.tix = tix;
    end;


    if yc==1,
        if isempty(W)
            R.H = [tmp(1); diff(tmp)];
        else
            C = cumsum(W(idx));     // cumulative weights
            R.H = [C(tmp(1)); diff(C(tmp))];
        end;
        return;

    elseif yc>1,	// a few more steps are necessary

        // allocate memory
        H = zeros(size(R.X,1),yc);

        // scan each channel
        for k = 1:yc,
            if isempty(W)
                sY = gsort(Y(:,k),"g","i");
            else
                [sY,ix] = mtlb_sort(Y(:,k));
                C = cumsum(W(ix));
            end;
            ix = find(diff(sY,1,1) > 0);
            if size(ix,1)>0,
                tmp = [ix(:); R.N(k)];
            else
                tmp = R.N(k);
            end;

            t = 0;
            j = 1;
            if isempty(W)
                for x = tmp',
                    acc = sY(x);
                    while R.X(j)~=acc, j=j+1; end;
                    //j = find(sY(x)==R.X);   // identify position on X
                    H(j,k) = H(j,k) + (x-t);  // add diff(tmp)
                    t = x;
                end;
            else
                for x = tmp',
                    acc = sY(x);
                    while R.X(j)~=acc, j=j+1; end;
                    //j = find(sY(x)==R.X);   % identify position on X
                    H(j,k) = H(j,k) + C(x)-t;  // add diff(tmp)
                    t = C(x);

                end;

            end;
        end;

        R.H = H;
    end;
    ieee(mod);
endfunction


