function [CLASS,ERR,POSTERIOR,LOGP,COEF]=nan_classify(samples,training,classlabel,TYPE)
    // classifies sample data into categories
    // defined by the training data and its group information
    // Calling Sequence
    //  CLASS = nan_classify(sample, training, group)
    //  CLASS = nan_classify(sample, training, group, TYPE)
    //  [CLASS,ERR,POSTERIOR,LOGP,COEF] = nan_classify(...)
    // Parameters
    //  CLASS: contains the assigned group.
    //  ERR: is the classification error on the training set weighted by the
    //	prior propability of each group.
    // Description
    //  The same classifier as in TRAIN_SC are supported.
    //
    // ATTENTION: no cross-validation is applied, therefore the
    //    classification error is too optimistic (overfitting).
    //    Use XVAL instead to obtain cross-validated performance.
    // Examples
    //       loadmatfile(nan_getpath()+"/demos/data/iris.mat");
    //      x = meas;
    //      y = species;
    //      yhat = nan_classify(x,x,y);
    // See also
    // nan_train_sc
    // nan_test_sc
    // nan_xval
    // Bibliography
    // [1] R. Duda, P. Hart, and D. Stork, Pattern Classification, second ed.
    //       John Wiley & Sons, 2001.
    //  Authors
    //  Copyright (C) 2008,2009 by Alois Schloegl a.schloegl@ieee.org
    //  H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2008,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    // This program is free software; you can redistribute it and/or
    // modify it under the terms of the GNU General Public License
    // as published by the Free Software Foundation; either version 3
    // of the  License, or (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program; if not, write to the Free Software
    // Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
    if nargin<4
        TYPE = "linear";
    end;

    if (TYPE=="linear")
        TYPE = "LDA";
    elseif (TYPE=="quadratic")
        TYPE = "QDA2"; // result is closer to Matlab
    elseif (TYPE=="diagLinear")
        TYPE = "NBC";
    elseif (TYPE=="diagQuadratic")
        TYPE = "NBC";
    elseif (TYPE=="mahalanobis")
        TYPE = "MDA";
    end;
    mod=ieee();
    ieee(2);
    [classlabel,I,classgroup] = nan_grp2idx(classlabel);


    CC = nan_train_sc(training,classlabel,TYPE);
    R  = nan_test_sc(CC,samples);
    CLASS = classgroup(R.classlabel);

    if nargout>1,
        [R]  = nan_test_sc(CC,training,classlabel);
        ERR = 1-R.ACC;
    end;

    if nargout>2,
        warning("output arguments POSTERIOR,LOGP and COEF not supported")
        POSTERIOR = [];
        LOGP = [];
        COEF = [];
    end;
    ieee(mod);
endfunction

