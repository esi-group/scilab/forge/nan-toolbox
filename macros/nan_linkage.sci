function dgram = nan_linkage (d, method, distarg)
    // Produce a hierarchical clustering dendrogram
    // Calling Sequence
    // y = nan_linkage (d)
    // y= nan_linkage (d,method)
    // y= nan_linkage (x,method,metric)
    // y= nan_linkage (x,method,arglist)
    // Parameters
    // d: dissimilarity matrix relative to @var{n} observations, formatted as a ((n-1)*n/2) x 1 vector as produced by nan_pdist
    // x: contains data formatted for input to nan_pdist
    // metric:  is a metric for nan_pdist
    // arglist: is a list  containing arguments that are passed to nan_pdist.
    // Description
    // Produce a hierarchical clustering dendrogram
    //
    //
    // nan_linkage starts by putting each observation into a singleton
    // cluster and numbering those from 1 to n.  Then it merges two
    // clusters, chosen according to method, to create a new cluster
    // numbered n+1, and so on until all observations are grouped into
    // a single cluster numbered 2*n-1.  Row m of the
    // m-1x3 output matrix relates to cluster n+m: the first
    // two columns are the numbers of the two component clusters and column
    // 3 contains their distance.
    //
    // method defines the way the distance between two clusters is
    // computed and how they are recomputed when two clusters are merged:
    //
    // "single" (default)
    //
    // Distance between two clusters is the minimum distance between two
    // elements belonging each to one cluster.  Produces a cluster tree
    // known as minimum spanning tree.
    //
    // "complete"
    //
    // Furthest distance between two elements belonging each to one cluster.
    //
    // "average"
    //
    // Unweighted pair group method with averaging (UPGMA).
    // The mean distance between all pair of elements each belonging to one
    // cluster.
    //
    // "weighted"
    //
    // Weighted pair group method with averaging (WPGMA).
    // When two clusters A and B are joined together, the new distance to a
    // cluster C is the mean between distances A-C and B-C.
    //
    // "centroid"
    //
    // Unweighted Pair-Group Method using Centroids (UPGMC).
    // Assumes Euclidean metric.  The distance between cluster centroids,
    // each centroid being the center of mass of a cluster.
    //
    // "median"
    //
    // Weighted pair-group method using centroids (WPGMC).
    // Assumes Euclidean metric.  Distance between cluster centroids.  When
    // two clusters are joined together, the new centroid is the midpoint
    // between the joined centroids.
    //
    // "ward"
    //
    // Ward's sum of squared deviations about the group mean (ESS).
    // Also known as minimum variance or inner squared distance.
    // Assumes Euclidean metric.  How much the moment of inertia of the
    // merged cluster exceeds the sum of those of the individual clusters.
    // @end table
    //
    // Bibliography
    // Ward, J. H. Hierarchical Grouping to Optimize an Objective Function J. Am. Statist. Assoc. 1963, 58, 236-244, http://iv.slis.indiana.edu/sw/data/ward.pdf.
    //
    // See also
    //  nan_pdist
    //  nan_squareform
    // Authors
    // H. Nahrstaedt - 2011
    // Francesco Potortì


    // Copyright (C) 2008  Francesco Potortì  <[hidden email]>
    //
    // This software is free software; you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 3, or (at your option)
    // any later version.
    //
    // This software is distributed in the hope that it will be useful, but
    // WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    // General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this software; see the file COPYING.  If not, see
    // <http://www.gnu.org/licenses/>.

    // check the input
    if (nargin < 1) | (nargin > 3)
        error ("dgram = linkage (d)");
    end

    if (isempty (d))
        error ("linkage: d cannot be empty");
    elseif ( nargin < 3 & ~ (size(d,1)==1 | size(d,2)==1)) then
        error ("linkage: d must be a vector");
    end
    if nargin<2 then
        method="single";
    end;

    //   methods = struct("name", { "single"; "complete"; "average"; "weighted";     "centroid"; "median"; "ward" }, "distfunc", {(@(x) min(x))     // single
    //                 (@(x) max(x))     // complete
    //                 (@(x,i,j,w) sum(dmult(q=w([i,j]),x))/sum(q)) // average
    //                 (@(x) mean(x))     // weighted
    //                 (@massdist)     // centroid
    //                 (@(x,i) massdist(x,i))     // median
    //                 (@inertialdist)     // ward
    //    });
    methods_name= ["single"; "complete"; "average"; "weighted";     "centroid"; "median"; "ward"];

    mask =  (convstr (method)==methods_name);
    if (~ or (mask))
        error ("linkage: %s: unknown method", method);
    end
    //dist = {methods.distfunc}{mask};
    dist=find(mask);

    if (nargin == 3)
        if (type(distarg)==10) //string
            d = nan_pdist (d, distarg);
        elseif (type (distarg)==15) //list
            if length(distarg)==1 then
                d = nan_pdist (d, distarg(:));
            elseif length(distarg)==2 then
                d = nan_pdist (d, distarg(1), distarg(2));
            else
                error(" ");
            end
        else
            error (" ");
        end
    end

    d = nan_squareform (d, "tomatrix");      // dissimilarity NxN matrix
    n = size (d,1);       // the number of observations
    diagidx = sub2ind ([n,n], 1:n, 1:n); // indices of diagonal elements
    d(diagidx) = %inf; // consider a cluster as far from itself
    // For equal-distance nodes, the order in which clusters are
    // merged is arbitrary.  Rotating the initial matrix produces an
    // ordering similar to Matlab's.
    cname = n:-1:1; // cluster names in d
    d = moc_rot90 (d, 2); // exchange low and high cluster numbers
    weight = ones (1, n); // cluster weights
    dgram = zeros (n-1, 3); // clusters from n+1 to 2*n-1
    for cluster = n+1:2*n-1
        // Find the two nearest clusters
        [m midx] = mtlb_min (d(:));
        [r, c] = ind2sub (size (d), midx);
        // Here is the new cluster
        dgram(cluster-n, :) = [cname(r) cname(c) d(r, c)];
        // Put it in place of the first one and remove the second
        cname(r) = cluster;
        cname(c) = [];
        // Compute the new distances
        //newd = dist (d([r c], :), r, c, weight);
        select(method)
        case "single" then newd=mtlb_min(d([r c], :));
        case "complete" then newd=mtlb_max(d([r c], :));
        case "average" then
            q=weight([r,c]);
            newd=mtlb_sum(diag(q)*d([r c], :))/mtlb_sum(q);
        case "weighted" then newd=mtlb_mean(d([r c], :));
        case "centroid" then newd=massdist(d([r c], :), r, c, weight);
        case "median" then newd=massdist(d([r c], :), r);
        case "ward" then newd=inertialdist(d([r c], :), r, c, weight);
        end
        newd(r) = %inf; // take care of the diagonal element
        // Put distances in place of the first ones, remove the second ones
        d(r,:) = newd;
        d(:,r) = newd';
        d(c,:) = [];
        d(:,c) = [];
        // The new weight is the sum of the components' weights
        weight(r) = weight(r)+weight(c);
        weight(c) = [];
    end
    // Sort the cluster numbers, as Matlab does
    dgram(:,1:2) = mtlb_sort (dgram(:,1:2), 2);

    // Check that distances are monotonically increasing
    if (or (diff (dgram(:,3)) < 0))
        warning (sprintf("clustering linkage: cluster distances do not monotonically increase\n     you should probably use a method different from %s\n", method));
    end

endfunction

// Take two row vectors, which are the Euclidean distances of clusters I
// and J from the others.  Column I of second row contains the distance
// between clusters I and J.  The centre of gravity of the new cluster
// is on the segment joining the old ones. W are the weights of all
// clusters. Use the law of cosines to find the distances of the new
// cluster from all the others.
function y = massdist (x, i, j, w)
    x = x.^2; // squared Euclidean distances
    c2 = x(2, i); // squared distance between I and J
    if (nargin < 4) // median distance
        qi = 0.5; // equal weights ("weighted")
    else // centroid distance
        qi = 1 / (1 + w(j) / w(i)); // the cluster weights
    end
    y = sqrt (qi*x(1,:) + (1-qi)*(x(2,:) - qi*c2));
endfunction

// Take two row vectors, which are the inertial distances of clusters I
// and J from the others.  Column I of second row contains the inertial
// distance between clusters I and J. The centre of gravity of the new
// cluster K is on the segment joining I and J.  W are the weights of
// all clusters.  Convert inertial to Euclidean distances, then use the
// law of cosines to find the Euclidean distances of K from all the
// other clusters, convert them back to inertial distances and return
// them.
function y = inertialdist (x, i, j, w)
    x =x.^ 2; // squared inertial distances
    wi = w(i); wj = w(j); // the cluster weights
    sij = wi + wj; // sum of weights of I and J
    c2 = x(2,i) * sij / wi / wj; // squared Eucl. dist. between I and J
    s = [wi + w; wj + w]; // sum of weights for all cluster pairs
    p = [wi * w; wj * w];      // product of weights for all cluster pairs
    x = x.*( s ./ p);        // convert inertial dist. to squared Eucl.
    qi = wi/sij; // normalise the weights of I and J
    // Squared Euclidean distances between all clusters and new cluster K
    x = qi*x(1,:) + (1-qi)*(x(2,:) - qi*c2);
    y = sqrt (x * sij .* w ./ (sij + w)); // convert Eucl. dist. to inertial
endfunction


