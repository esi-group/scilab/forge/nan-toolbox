function [table] = nan_crosstab(x1,x2,x3,x4)
    //Cross-tabulation
    // Calling Sequence
    // [table] = nan_crosstab(x1,x2)
    // [table] = nan_crosstab(x1,x2,x3)
    // [table] = nan_crosstab(x1,x2,x3,x4)
    // Description
    // nan_crosstab return a table which containts the number of equal symbols across all input vectors.
    // The first dimenstion in table counts the symbols in x1, the second dimenstion counts the symbols in x2 and so on...
    // Examples
    // x = [1 1 2 3 1]; y = [1 2 5 3 1];
    // // number of the symbols 1,2,3,4,5 in x and y
    // table= nan_crosstab(x,y)
    // Authors
    // H. Nahrstaedt - 2013

    if nargin<2 | nargin>4 then
        error("Two, three or four input vectors allowed.");
    end;
    x1_group=nan_grp2idx(x1);
    x2_group=nan_grp2idx(x2);
    if length(x1_group) ~= length(x2_group) then
        error("All input vectors must have the same length!.");
    end;

    if nargin==3 then
        x3_group=nan_grp2idx(x3);
        if length(x1_group) ~= length(x3_group) then
            error("All input vectors must have the same length!.");
        end;
    end
    if nargin==4 then
        x4_group=nan_grp2idx(x4);
        if length(x1_group) ~= length(x4_group) then
            error("All input vectors must have the same length!.");
        end;
    end

    if nargin==2 then
        table=zeros(max(x1_group),max(x2_group));
    elseif nargin==3 then
        table=zeros(max(x1_group),max(x2_group),max(x3_group));
    elseif nargin==4 then
        table=zeros(max(x1_group),max(x2_group),max(x3_group),max(x4_group));
    end;
    if nargin==2 then
        for i=1:max(x1_group)
            for j=1:max(x2_group)
                for k=1:length(x1_group)
                    if or(x1_group(k)==i) & or(x2_group(k)==j) then
                        table(i,j)=table(i,j) + 1;
                    end;
                end;
            end;
        end;
    elseif nargin==3 then
        for i=1:max(x1_group)
            for j=1:max(x2_group)
                for k=1:max(x3_group)
                    for l=1:length(x1_group)
                        if or(x1_group(l)==i) & or(x2_group(l)==j) & or(x3_group(l)==k) then
                            table(i,j,k)=table(i,j,k) + 1;
                        end;
                    end;
                end;
            end;
        end;
    elseif nargin==4 then
        for i=1:max(x1_group)
            for j=1:max(x2_group)
                for k=1:max(x3_group)
                    for l=1:max(x4_group)
                        for m=1:length(x1_group)
                            if or(x1_group(m)==i) & or(x2_group(m)==j) & or(x3_group(m)==k) & or(x4_group(m)==l) then
                                table(i,j,k,l)=table(i,j,k,l) + 1;
                            end;
                        end;
                    end;
                end;
            end;
        end;
    end
endfunction