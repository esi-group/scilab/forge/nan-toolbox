function RES = nan_bland_altman(data,groups,arg3)
    //  shows the Bland-Altman plot of two columns of measurements
    //   and computes several summary results.
    // Calling Sequence
    //   nan_bland_altman(m1, m2 [,group])
    //   nan_bland_altman(data [, group])
    //   R = nan_bland_altman(...)
    // Parameters
    //   m1,m2: are two colums with the same number of elements
    //	containing the measurements. m1,m2 can be also combined
    //       in a single two column data matrix.
    //   group [optional] : indicates which measurements belong to the same group
    //	This is useful to account for repeated measurements.
    //
    // Bibliography
    // [1] JM Bland and DG Altman, Measuring agreement in method comparison studies.
    //       Statistical Methods in Medical Research, 1999; 8; 135.
    //       doi:10.1177/09622802990080204
    //   [2] P.S. Myles, Using the Bland– Altman method to measure agreement with repeated measures
    //   British Journal of Anaesthesia 99(3):309–11 (2007)
    //   doi:10.1093/bja/aem214
    // Examples
    // nan_bland_altman(rand(30,1,'normal'),rand(30,1,'normal'));
    // Authors
    // Copyright (C) 2010 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://pub.ist.ac.at/~schloegl/matlab/NaN/

    // This program is free software; you can redistribute it and/or
    // modify it under the terms of the GNU General Public License
    // as published by the Free Software Foundation; either version 3
    // of the  License, or (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program; if not, write to the Free Software
    // Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
    if nargin<2, groups = []; end;
    if nargin<3, arg3 = []; end;

    if (size(data,2)==1)
        data  = [data, groups];
        groups = arg3;
    end;


    D = data * [1;-1];
    M = data * [1;1]/2;

    RES.corrcoef = nan_corrcoef(data(:,1),data(:,2),"spearman");
    [REs.cc,RES.p] = nan_corrcoef(M,D,"spearman");
    if (RES.p<0.05)
        warning("A regression model according to section 3.2 [1] should be used");
        //// TODO: implement support for this type of data.
        RES.a = [ones(size(data,1),1),D]\M;
        RES.b = [ones(size(data,1),1),M]\D;
    end;

    if isempty(groups)
        G = [1:size(data,1)]';
        m = ones(size(data,1),1);
        d = D;
        RES.Bias = nan_mean(d,1);
        RES.Var  = nan_var(d);

    elseif ~isempty(groups)
        //// TODO: this is not finished
        warning("analysis of data with repetitions is experimental - it might yield incorrect results - you are warned.!")
        [G,I,J] = moc_unique (groups);
        R       = zeros(data);
        m       = repmat(%nan,length(G),1);
        n       = repmat(%nan,length(G),1);
        d       = repmat(%nan,length(G),1);
        d2      = repmat(%nan,length(G),1);
        data2   = repmat(%nan,length(G),size(data,2));
        SW2     = repmat(%nan,length(G),size(data,2));
        for i   = 1:length(G),
            ix         = find(groups==G(i));
            n(i)       = length(ix);
            //               IX((i-1)*N+1:i*N) = ix(ceil(rand(N,1)*n(i)));
            [R(ix,:), data2(i,:)] = nan_center(data(ix,:),1);
            d(i)       = nan_mean(D(ix,:),1);
            m(i)       = nan_mean(M(ix,:),1);
            d2(i)      = nan_mean(D(ix,:).^2,1);
            RES.SW2(i,:)   = nan_var(data(ix,:),[],1);
            RES.avg(i,:)   = nan_mean(data(ix,:),1);
        end;
        W = 1 ./n(J);
        RES.SSW = sumskipnan(R.^2,1,W);
        RES.SSB = nan_var(data,[],1,W)*sum(W)*(sum(W)-1);
        RES.sigma2_w= RES.SSW/(sum(W)*(length(G)-1));
        RES.sigma2_u= RES.SSB/(sum(W)*(length(G)-1)) - RES.sigma2_w/(length(G));
        try
            RES.group = nan_bland_altman(data2);        //FIXME: this plot shows incorrect interval, it does not account for the group/repeated samples.
        catch

        end;
        RES.repeatability_coeff1 = 2.77*sqrt(nan_var(R,1,1));        // variance with factor group removed
        RES.repeatability_coeff = 2.77*sqrt(nan_mean(SW2,1));        // variance with factor group removed
        RES.std_d_ = nan_std(d);
        RES.std_D_ = nan_std(D);
        RES.std_m_ = nan_std(m);
        RES.n = n;
        return;

        D = d;
        M = m;
        //	RES.sigma2_dw =

        RES.Bias = nan_mean(d,1,[],n);
    end;


    plot(M,D,"o", [min(M),max(M)]', [0,0]',"k--", [min(M),max(M)]', [1,1,1; 0,1.96,-1.96]'*[RES.Bias;nan_std(D)]*[1,1], "k-");
    xlabel("mean");
    ylabel("difference");

endfunction
