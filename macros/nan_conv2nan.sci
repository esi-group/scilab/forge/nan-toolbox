function [C,N,c] = nan_conv2nan(X,Y,arg3)
    // calculates 2-dim convolution between X and Y
    // Calling Sequence
    // [...] = nan_conv2nan(X,Y);
    // [C]   = nan_conv2nan(X,Y);
    // Description
    // X and Y can contain missing values encoded with NaN.
    // NaN's are skipped, NaN do not result in a NaN output.
    // The output gives NaN only if there are insufficient input data
    //Authors
    //Copyright (C) 2000-2005,2010 by Alois Schloegl a.schloegl@ieee.org
    //H. Nahrstaedt - 2010

    //    This function is part of the NaN-toolbox
    //    http://www.dpmi.tu-graz.ac.at/~schloegl/matlab/NaN/

    //	$Id$
    //	Copyright (C) 2000-2005,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://www.dpmi.tu-graz.ac.at/~schloegl/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.

    if nargin~=2,
        error("Error CONV2NAN: incorrect number of input arguments");
    end;
    mod=ieee();
    ieee(2);
    m = isnan(X);
    n = isnan(Y);

    X(m) = 0;
    Y(n) = 0;

    C = moc_conv2(X,Y);         // 2-dim convolution
    N = moc_conv2(bool2s(~m),bool2s(~n));     // normalization term
    c = moc_conv2(ones(X),ones(Y)); // correction of normalization

    if nargout==1,
        C = C.*c./N;
    elseif nargout==2,
        N = N./c;
    end;
    ieee(mod);
endfunction
