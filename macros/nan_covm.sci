function [CC,NN] = nan_covm(X,Y,Mode,W);
    // generates covariance matrix
    // Calling Sequence
    // nan_covm(X,Mode);
    // nan_covm(X,Y,Mode);
    // nan_covm(...,W);
    //   C = nan_covm(...);
    //   [C,N] = nan_covm(...);
    // Parameters
    // W : weighted crosscorrelation
    // Mode = 'M' : minimum or standard mode [default]
    // 	C = X'*X; or X'*Y correlation matrix
    // Mode = 'E': extended mode
    // 	C = [1 X]'*[1 X]; // l is a matching column of 1's
    // 	C is additive, i.e. it can be applied to subsequent blocks and summed up afterwards
    // 	the mean (or sum) is stored on the 1st row and column of C
    // Mode = 'D' or 'D0': detrended mode
    //	the mean of X (and Y) is removed. If combined with extended mode (Mode='DE'),
    // 	the mean (or sum) is stored in the 1st row and column of C.
    // 	The default scaling is factor (N-1).
    // Mode = 'D1' : is the same as 'D' but uses N for scaling.
    //  C: is the scaled by N in Mode M and by (N-1) in mode D.
    //  [C,N]: C is not scaled, provides the scaling factor N
    //	C./N gives the scaled version.
    // Description
    // X and Y can contain missing values encoded with NaN.
    // NaN's are skipped, NaN do not result in a NaN output.
    // The output gives NaN only if there are insufficient input data
    //
    // nan_covm(X,Mode);
    //      calculates the (auto-)correlation matrix of X
    //
    // nan_covm(X,Y,Mode);
    //      calculates the crosscorrelation between X and Y
    //
    // See also
    //    nan_decovm
    //    nan_xcovf
    //
    // Authors
    //      H. Nahrstaedt
    //      Copyright (C) 2000-2005,2009 by Alois Schloegl a.schloegl@ieee.org

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; if not, write to the Free Software
    //    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    global FLAG_NANS_OCCURED;

    if nargin < 3 then
        W = [];
        if nargin == 2 then
            if or(type(Y) == [1 5 8]) then
                Mode="M";
            else
                Mode=Y;
                Y=[];
            end
        elseif nargin == 1 then
            Mode = "M";
            Y = [];
        elseif nargin == 0 then
            error("Missing argument(s)");
        end
    elseif nargin == 3 & or(type(Y) == [1 5 8]) & ~or(type(Mode) == [1 5 8]) then
        W = [];
    elseif nargin == 3 & ~or(type(Y) == [1 5 8]) & or(type(Mode) == [1 5 8]) then
        W = Mode;
        Mode = Y;
        Y = [];
    elseif nargin == 4 & ~or(type(Mode) == [1 5 8]) & or(type(Y) == [1 5 8]) then
    else
        error("invalid input arguments");
    end;

    Mode = convstr(Mode,"u");

    [r1,c1]=size(X);
    if ~isempty(Y) then
        [r2,c2]=size(Y);
        if r1~=r2,
            error("X and Y must have the same number of observations (rows).");
        end;
    else
        [r2,c2] = size(X);
    end;

    // persistent mexFLAG2;
    // persistent mexFLAG;
    // if isempty(mexFLAG2)
    // 	mexFLAG2 = exist("covm_mex","file");
    // end;
    // if isempty(mexFLAG)
    // 	mexFLAG = exist("sumskipnan_mex","file");
    // end;
    try
        mexFLAG2=type(covm_mex)==130;
    catch
        mexFLAG2=%f;
    end;
    try
        mexFLAG=type(sumskipnan_mex)==130;
    catch
        mexFLAG=%f;
    end;

    if ~isempty(W)
        W = W(:);
        if (r1~=length(W))
            error("Error COVM: size of weight vector does not fit number of rows");
        end;
        //w = spdiags(W(:),0,numel(W),numel(W));
        //nn = sum(W(:));
        nn = sum(W);
    else
        nn = r1;
        // 	if nn==1 & c1>1 then
        // 	  X=X';
        // 	  [r1,c1]=size(X);
        // 	  nn = r1;
        //           if ~isempty(Y)
        //              Y=Y';
        //              [r2,c2]=size(Y);
        // 	  else
        //               [r2,c2]=size(X);
        // 	  end;
        //          end;
    end;


    if mexFLAG2 & mexFLAG & ~isempty(W),
        // the mex-functions here are much slower than the m-scripts below
        // however, the mex-functions support weighting of samples.
        if isempty(FLAG_NANS_OCCURED),
            // mex-files require that FLAG_NANS_OCCURED is not empty,
            // otherwise, the status of NAN occurence can not be returned.
            FLAG_NANS_OCCURED = %f;  // default value
        end;

        if part(Mode,1) == "D" | part(Mode,1) == "E",
            [S1,N1] = sumskipnan(X,1,W);
            if ~isempty(Y)
                [S2,N2] = sumskipnan(Y,1,W);
            else
                S2 = S1; N2 = N1;
            end;
            if part(Mode,1)=="D", // detrending mode
                X  = X - ones(r1,1)*(S1./N1);
                if ~isempty(Y)
                    Y  = Y - ones(r1,1)*(S2./N2);
                end;
            end;
        end;

        [CC,NN] = covm_mex(real(X), real(Y), FLAG_NANS_OCCURED, W);
        //// complex matrices
        if ~isreal(X) & ~isreal(Y)
            [iCC,inn] = covm_mex(imag(X), imag(Y), FLAG_NANS_OCCURED, W);
            CC = CC + iCC;
        end;
        if isempty(Y) then Y = X; end;
        if ~isreal(X)
            [iCC,inn] = covm_mex(imag(X), real(Y), FLAG_NANS_OCCURED, W);
            CC = CC - %i*iCC;
        end;
        if ~isreal(Y)
            [iCC,inn] = covm_mex(real(X), imag(Y), FLAG_NANS_OCCURED, W);
            CC = CC + %i*iCC;
        end;

        if part(Mode,1) == "D" & ~part(Mode,2) == "1" then  //  "D0"
            NN = max(NN-1,0);
        end;
        if part(Mode,1)=="E", // extended mode
            NN = [nn, N2; N1', NN];
            CC = [nn, S2; S1', CC];
        end;


    elseif ~isempty(W),
        error("Error COVM: weighted COVM requires sumskipnan_mex and covm_mex but it is not available");

        // weighted covm without mex-file support
        // this part is not working.

    elseif ~isempty(Y),
        if (~part(Mode,1)=="D" & ~part(Mode,1)=="E"), // if Mode == M
            NN = bool2s(X==X)'*bool2s(Y==Y);
            FLAG_NANS_OCCURED = or(NN(:)<nn);
            X(isnan(X)) = 0; // skip NaN's
            Y(isnan(Y)) = 0; // skip NaN's
            CC = X'*Y;
        else  // if or(Mode=="D") | or(Mode=="E"),
            [S1,N1] = sumskipnan(X,1);
            [S2,N2] = sumskipnan(Y,1);
            NN = bool2s(X==X)'*bool2s(Y==Y);

            if part(Mode,1) == "D", // detrending mode
                X  = X - ones(r1, 1) * (S1 ./ N1);
                Y  = Y - ones(r1, 1) * (S2 ./ N2);
                if part(Mode,2) == "1" then  //  "D1"
                    NN = NN;
                else   //  "D0"
                    NN = max(NN-1,0);
                end;
            end;

            X(isnan(X)) = 0; // skip NaN's
            Y(isnan(Y)) = 0; // skip NaN's
            CC = X'*Y;

            if part(Mode,1)=="E", // extended mode
                NN = [nn, N2; N1', NN];
                CC = [nn, S2; S1', CC];
            end;
        end;

    else
        if part(Mode, 1) <> "D" && part(Mode, 1) <> "E" then // if Mode == M
            tmp = bool2s(X==X);
            NN  = tmp'*tmp;
            X(isnan(X)) = 0; // skip NaN's
            CC = X'*X;
            FLAG_NANS_OCCURED = or(NN(:)<nn);
        else  // if or(Mode=="D") | or(Mode=="E"),
            [S,N] = sumskipnan(X,1);
            tmp = bool2s(X==X);;
            NN  = tmp'*tmp;
            if part(Mode,1)=="D", // detrending mode
                X  = X - ones(r1,1)*(S./N);
                if part(Mode,2)=="1",  //  "D1"
                    NN = NN;
                else  //  "D0"
                    NN = max(NN-1,0);
                end;
            end;

            X(isnan(X)) = 0; // skip NaN's
            CC = X'*X;
            if part(Mode,1)=="E", // extended mode
                NN = [nn, N; N', NN];
                CC = [nn, S; S', CC];
            end;
        end
    end;

    if nargout<2 & NN~=0 then
        CC = CC./NN; // unbiased
    end;
endfunction
