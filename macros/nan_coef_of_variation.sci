function cv=nan_coef_of_variation(i,DIM)
    //  returns STD(X)/MEAN(X)
    // Calling Sequence
    // cv=nan_coef_of_variation(x [,DIM])
    // Parameters
    //  cv: std(x)/mean(x)
    // See also
    // sumskipnan
    // nan_mean
    // nan_std
    //   Bibliography
    //   http://mathworld.wolfram.com/VariationCoefficient.html
    // Authors
    //	Copyright (C) 1997-2003 by Alois Schloegl a.schloegl@ieee.org
    //   H. Nahrstaedt

    //	$Id$
    //	Copyright (C) 1997-2003 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    if nargin<2,
        DIM = find(size(i)>1,1);
        if isempty(DIM), DIM=1; end;
    end;
    mod=ieee();
    ieee(2);
    [S,N,SSQ] = sumskipnan(i,DIM);

    // sqrt((SSQ-S.*S./N)./max(N-1,0))/(S./N);    // = std(i)/mean(i)

    cv = sqrt(SSQ.*N./(S.*S)-1);

    //if flag_implicit_unbiased_estim,
    cv = cv.*sqrt(N./max(N-1,0));
    //end;
    ieee(mod);
endfunction
