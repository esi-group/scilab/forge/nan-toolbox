function alpha=flag_impl_significance(alpha)
    //  sets and gets default alpha (level) of any significance test
    // Calling Sequence
    // alpha = flag_impl_significance()
    // flag_impl_significance(alpha)
    // alpha = flag_impl_significance(alpha)
    // Parameters
    // alpha: alpha-level for the significance test. Default is 0.01.
    // Description
    // The use of FLAG_IMPLICIT_SIGNIFICANCE is in experimental state.
    // flag_impl_significance might even become obsolete.
    //
    // FLAG_IMPLICIT_SIGNIFICANCE sets and gets default alpha (level) of any significance test
    // The default alpha-level is stored in the global variable FLAG_implicit_significance
    // The idea is that the significance must not be assigned explicitely.
    // This might yield more readable code.
    //
    // Choose alpha low enough, because in alpha*100// of the cases, you will
    // reject the Null hypothesis just by change. For this reason, the default
    // alpha is 0.01.
    //
    // See also
    // nan_corrcoef
    // nan_partcorrcoef
    // Authors
    // Copyright (C) 2000-2002,2009,2010 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2000-2002,2009,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/
    //
    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    global FLAG_IMPLT_SIGNIFICANCE;
    DEFAULT_ALPHA = 0.01;

    if isempty(FLAG_IMPLT_SIGNIFICANCE) then
        FLAG_IMPLT_SIGNIFICANCE = DEFAULT_ALPHA;  // default value
    end

    if nargin > 0 then
        warning("Warning: flag_impl_significance is in an experimental state\n");
        warning("It might become obsolete.\n");
        FLAG_IMPLT_SIGNIFICANCE = alpha;
    end;

    alpha = FLAG_IMPLT_SIGNIFICANCE;
endfunction
