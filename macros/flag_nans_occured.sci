function flag = flag_nans_occured()
    // checks whether the last call(s) to sumskipnan or covm
    // contained any not-a-numbers in the input argument.
    // Calling Sequence
    // [flag]=flag_nans_occured()
    // Description
    // checks whether the last call(s) to sumskipnan or covm
    // contained any not-a-numbers in the input argument. Because many other
    // functions like mean, std, etc. are also using sumskipnan,
    // also these functions can be checked for NaN's in the input data.
    //
    // A call to FLAG_NANS_OCCURED() resets also the flag whether NaN's occured.
    // Only sumskipnan or covm can set the flag again.
    //
    // See also
    // sumskipnan
    // nan_covm
    // Authors
    // Copyright (C) 2009 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software: you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation, either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    global FLAG_NANS_OCCURED;

    ////// check whether FLAG was already defined
    if isempty(FLAG_NANS_OCCURED) then
        FLAG_NANS_OCCURED = %f;  // default value
    end;

    flag = FLAG_NANS_OCCURED;		// return value
    FLAG_NANS_OCCURED = %f;		// reset flag
endfunction
