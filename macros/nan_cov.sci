function CC = nan_cov(X,Y,Mode)
    // calculates covariance matrix
    // Calling Sequence
    // C = nan_cov(X [,Mode]);
    // C = nan_cov(X,Y [,Mode]);
    // Parameters
    // C : correlation matrix
    // Mode = 0: [default] scales C by (N-1)
    // Mode = 1: scales C by N.
    // Description
    // X and Y can contain missing values encoded with NaN.
    // NaN's are skipped, NaN do not result in a NaN output.
    // The output gives NaN only if there are insufficient input data
    // The mean is removed from the data.
    //
    // C = nan_cov(X [,Mode]);
    //      calculates the (auto-)correlation matrix of X
    //
    // C = nan_cov(X,Y [,Mode]);
    //      calculates the crosscorrelation between X and Y.
    //      C(i,j) is the correlation between the i-th and jth
    //      column of X and Y, respectively.
    //
    //   NOTE: Octave and Matlab have (in some special cases) incompatible implemenations.
    //       This implementation follows Octave. If the result could be ambigous or
    //       incompatible, a warning will be presented in Matlab. To avoid this warning use:
    //       a) use COV([X(:),Y(:)]) if you want the traditional Matlab result.
    //       b) use C = COV([X,Y]), C = C(1:size(X,2),size(X,2)+1:size(C,2)); if you want to be compatible with this software.
    //
    // See also
    // nan_covm
    // nan_cor
    // nan_corrcoef
    // sumskipnan
    // Bibliography
    // http://mathworld.wolfram.com/Covariance.html
    //Authors
    //	Copyright (C) 2000-2003,2005,2009 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2000-2003,2005,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    mod=ieee();
    ieee(2);
    if nargin==1
        Mode = 0;
        Y = [];
    elseif nargin==2,
        // if and(size(Y)==1) & or(Y==[0,1]); 	// This is not compatible with octave
        // short-circuit evaluation is required
        // but for compatibility to matlab, && is avoided
        SW = and(size(Y)==1);
        if SW, SW = or(Y==[0,1]); end;
        if SW,
            Mode = Y;
            Y = [];
        else
            Mode = 0;
        end;
    elseif nargin==3,

    else
        ieee(mod);
        error("Error COV: invalid number of arguments\n");
    end;

    // if ~exists('OCTAVE_VERSION') & ~isempty(Y) & (size(X,2)+size(Y,2)~=2),
    //         // COV in Matlab is differently defined than COV in Octave.
    //         // For compatibility reasons, this branch reflects the difference.
    //         printf('Warning NaN/COV: This kind of use of COV is discouraged because it produces different results for Matlab and Octave. \n');
    //         printf('  (a) the traditional Matlab result can be obtained with:  C = COV([X(:),Y(:)]).\n');
    //         printf('  (b) the traditional Octave result can be obtained with:  C = COV([X,Y]); C = C(1:size(X,2),size(X,2)+1:size(C,2)).\n');
    //
    //         if length(Y)~=length(X),
    //                 error('The lengths of X and Y must match.');
    //         end;
    //         X = [X(:),Y(:)];
    //         Y = [];
    // end;

    if isempty(Y)
        CC = nan_covm(X,["D"+string(bool2s(Mode>0))]);
    else
        CC = nan_covm(X,Y,["D"+string(bool2s(Mode>0))]);
    end;
    ieee(mod);
endfunction
