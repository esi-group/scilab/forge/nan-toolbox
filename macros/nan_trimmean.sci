function Q=nan_trimmean(Y,p,DIM)
    //  calculates the trimmed mean by removing the upper and lower
    // (p/2)% samples.
    // Calling Sequence
    //  Q = nan_trimmean(Y,p)
    //  Q = nan_trimmean(Y,p,DIM)
    //  Parameters
    //  Q: returns the trimmean along dimension DIM of sample array Y.
    //  p:  If p is a vector, the trimmean for each p is computed.
    //  Description
    //  nan_trimmean calculates the trimmed mean by removing the fraction of p/2 upper and
    // p/2 lower samples. Missing values (encoded as NaN) are ignored and not taken into account.
    // The same number from the upper and lower values are removed, and is compatible to various
    // spreadsheet programs including GNumeric [1], LibreOffice, OpenOffice and MS Excel.
    //
    //  Bibliography
    //  [1] http://www.fifi.org/doc/gnumeric-doc/html/C/gnumeric-trimmean.html
    // See also
    // nan_percentile
    // nan_quantile
    // Authors
    //  Copyright (C) 2009,2010 by Alois Schloegl a.schloegl@ieee.org
    //  H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2009,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    if nargin<3,
        DIM = [];
    end;
    if isempty(DIM),
        DIM = find(size(Y)>1,1);
        if isempty(DIM), DIM = 1; end;
    end;

    if nargin<2,
        error("At least two argument neeeded!");
    else
        mod=ieee();
        ieee(2);

        sz = size(Y);
        if DIM>length(sz),
            sz = [sz,ones(1,DIM-length(sz))];
        end;

        D1 = prod(sz(1:DIM-1));
        D2 = length(p);
        D3 = prod(sz(DIM+1:length(sz)));
        Q  = repmat(%nan,[sz(1:DIM-1),D2,sz(DIM+1:length(sz))]);
        for k = 0:D1-1,
            for l = 0:D3-1,
                xi = k + l * D1*sz(DIM) + 1 ;
                xo = k + l * D1*D2;
                t  = Y(xi:D1:xi+D1*sz(DIM)-1);
                t  = mtlb_sort(t(~isnan(t)));
                N  = max(size(t));
                for m=1:D2,
                    n  = floor(N*p(m)/2);
                    f  = mtlb_sum(t(1+n:N-n))/(N-2*n);
                    Q(xo + m*D1) = f;
                end;
            end;
        end;
        ieee(mod);
    end;

endfunction
