function [h] = nan_fscatter3(X,Y,Z,C,cmap);
    // Plots point cloud data
    // Calling Sequence
    // [h] = nan_fscatter3(X,Y,Z,C,cmap);
    // X: data vector (x-axis)
    // Y: data vector (y-axis)
    // Z: data vector (z-axis)
    // C: group vector
    // cmap: optional colormap
    // h: handles to the line objects
    // Description
    // Plots point cloud data in cmap color classes and 3 Dimensions,
    // X,Y,Z,C are vectors of the same length
    // Examples
    // deff("[x,y,z]=sph(alp,tet)",["x=r*cos(alp).*cos(tet)+orig(1)*ones(tet)";..
    //      "y=r*cos(alp).*sin(tet)+orig(2)*ones(tet)";..
    //      "z=r*sin(alp)+orig(3)*ones(tet)"]);
    // r=1; orig=[0 0 0];
    // [x,y,z]=eval3dp(sph,linspace(-%pi/2,%pi/2,40),linspace(0,%pi*2,20));
    // X = [x(:)*.5 x(:)*.75 x(:)];
    // Y = [y(:)*.5 y(:)*.75 y(:)];
    // Z = [z(:)*.5 z(:)*.75 z(:)];
    //S = repmat([1 .75 .5]*10,length(x),1);
    //C = repmat([1 2 3],length(x),1);
    //nan_fscatter3(X,Y,Z,C);
    //
    // loadmatfile(nan_getpath()+"/demos/data/iris.mat");
    // nan_fscatter3(meas(:,1),meas(:,2),meas(:,3),nan_grp2idx(species))
    //
    // Authors
    // Felix Morsdorf, Jan 2003 (last update Oct. 2010), Remote Sensing Laboratory Zuerich
    // H. Nahrstaedt

    if nargin <4
        error("At least 4 Parameter needed!");
    elseif nargin == 4
        numclass = 128; // Number of color classes
        cmap = rainbowcolormap(numclass);
        siz = 1;
    elseif nargin == 5
        numclass = max(size(cmap));
        siz = 5;
        if numclass == 1
            siz = cmap;
            numclass = 128;
            cmap = hsvcolormap(numclass);
        end
    elseif nargin == 6
        numclass = max(size(cmap));
        if numclass == 1
            siz = cmap;
            numclass = 128;
            cmap = hsvcolormap(numclass);

        end
    end



    // avoid too many calculations

    mins = min(C);
    maxs = max(C);
    minz = min(Z);
    maxz = max(Z);
    minx = min(X);
    maxx = max(X);
    miny = min(Y);
    maxy = max(Y);

    // construct colormap :

    col = cmap;

    // determine index into colormap

    ii = floor( (C - mins ) * (numclass-1) / (maxs - mins) );
    ii = ii + 1;

    f=gcf();f.color_map=cmap;

    k = 0;o = k;
    for j = 1:numclass
        jj = (ii(:)== j);
        if or(jj)
            k = k + 1;
            plot3d(X(jj),Y(jj),Z(jj));//,'.','color',col(j,:),'markersize',siz);
            h=gce();
            h.surface_mode="off";
            h.mark_mode="on";
            h.mark_foreground=color(floor(cmap(j,1)*255),floor(cmap(j,2)*255),floor(cmap(j,3)*255));
            h.mark_size=siz;
            if ~isempty(h)
                o = o+1;
                hp(o) = h;
            end
        end
    end
    // caxis([min(C) max(C)])
    // axis equal;rotate3d on;view(3);
    // box on
    // hcb = colorbar('location','east');
endfunction
