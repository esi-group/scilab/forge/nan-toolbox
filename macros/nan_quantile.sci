function Q=nan_quantile(Y,q,DIM,method)
    //  calculates the quantiles of histograms and sample arrays.
    // Calling Sequence
    //  Q = nan_quantile(Y,q)
    //  Q = nan_quantile(Y,q,DIM)
    //  Q = nan_quantile(HIS,q)
    //  Parameters
    //   Y :    vector or matrix of reals
    //   q :    probability (scalar or vector of probabilities numbers)
    //   DIM :
    //  Description
    //  Q = nan_quantile(Y,q,DIM)
    //
    //     returns the q-th quantile along dimension DIM of sample array Y.
    //     size(Q) is equal size(Y) except for dimension DIM which is size(Q,DIM)=length(Q)
    //
    //  Q = nan_quantile(HIS,q)
    //
    //     returns the q-th quantile from the histogram HIS.
    //     HIS must be a HISTOGRAM struct as defined in HISTO2 or HISTO3.
    //     If q is a vector, the each row of Q returns the q(i)-th quantile
    //
    // See also
    // nan_percentile
    // nan_histo2
    // nan_histo3
    // Authors
    //    Copyright (C) 1996-2003,2005,2006,2007,2009 by Alois Schloegl a.schloegl@ieee.org
    //    H. Nahrstaedt - 2010, 2011


    //	$Id$
    //	Copyright (C) 1996-2003,2005,2006,2007,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    if nargin<3,
        DIM = [];
    end;
    if isempty(DIM),
        DIM = find(size(Y)>1,1);
        if isempty(DIM), DIM = 1; end;
    end;


    if nargin<2,
        error("to less input parameters");

    else
        mod=ieee();
        ieee(2);
        [q, rix]  =  mtlb_sort(q(:)');        // sort quantile values
        [tmp,rix] = mtlb_sort(rix);  // generate reverse index

        SW = typeof(Y)=="st";
        //if SW, SW = isfield(Y,'datatype'); end;
        if SW,  allfields=getfield(1,Y);SW = or(allfields(3:$)=="datatype"); end;
        if SW, SW = (Y.datatype=="HISTOGRAM"); end;
        if SW,
            [yr,yc]=size(Y.H);
            Q = repmat(%nan,length(q),yc);
            //if ~isfield(Y,'N');
            allfields=getfield(1,Y);
            if ~or(allfields(3:$)=="N")
                Y.N = sum(Y.H,1);
            end;

            for k1 = 1:yc,
                tmp = Y.H(:,k1)>0;
                h = full(Y.H(tmp,k1));
                t = Y.X(tmp,min(size(Y.X,2),k1));

                N = Y.N(k1);
                t2(1:2:2*length(t)) = t;
                t2(2:2:2*length(t)) = t;
                if and(size(h)>1)
                    x2 = cumsum(h,1);
                else
                    x2 = cumsum(h);
                end
                x(1)=0;
                x(2:2:2*length(t)) = x2;
                x(3:2:2*length(t)) = x2(1:$-1);

                // Q(q < 0 | 1 < q,:) = NaN;  % already done at initialization
                Q(q==0,k1) = t2(1);
                Q(q==1,k1) = t2($);
                n = 1;
                for k2 = find( (0 < q) & (q < 1) )
                    while (q(k2)*N > x(n)),
                        n=n+1;
                    end;

                    if q(k2)*N==x(n)
                        // mean of upper and lower bound
                        Q(k2,k1) = (t2(n)+t2(n+1))/2;
                    else
                        Q(k2,k1) = t2(n);
                    end;
                end;
                Q = Q(rix,:);   // order resulting quantiles according to original input q
            end;


        elseif or(type(Y)==[1 5 8 17]),
            sz = size(Y);
            if DIM>length(sz),
                sz = [sz,ones(1,DIM-length(sz))];
            end;
            if DIM==1;
                f  = zeros(length(q),1);
            else
                f  = zeros(1,length(q));
            end;
            f( (q < 0) | (1 < q) ) = %nan;
            D1 = prod(sz(1:DIM-1));
            D3 = prod(sz(DIM+1:length(sz)));
            Q  = repmat(%nan,[sz(1:DIM-1),length(q),sz(DIM+1:length(sz))]);
            for k = 0:D1-1,
                for l = 0:D3-1,
                    xi = k + l * D1*sz(DIM) + 1 ;
                    xo = k + l * D1*length(q) + 1;
                    t  = Y(xi:D1:xi+D1*sz(DIM)-1);
                    t  = t(~isnan(t));
                    N  = length(t);

                    if (N==0)
                        f(:) = %nan;
                    else
                        t  = mtlb_sort(t);
                        t2(1:2:2*length(t)) = t;
                        t2(2:2:2*length(t)) = t;
                        x = floor((1:2*length(t))/2);
                        //f(q < 0 | 1 < q) = NaN;  % for efficiency its defined outside loop
                        f(q==0) = t2(1);
                        f(q==1) = t2($);
                        n = 1;
                        for k2 = find( (0 < q) & (q < 1) )
                            while (q(k2)*N > x(n)),
                                n = n+1;
                            end;

                            if q(k2)*N==x(n)
                                // mean of upper and lower bound
                                f(k2) = (t2(n) + t2(n+1))/2;
                            else
                                f(k2) = t2(n);
                            end;
                        end;
                    end;
                    Q(xo:D1:xo + D1*length(q) - 1) = f(rix);
                end;
            end;

        else
            ieee(mod);
            error("Error QUANTILES: invalid input argument\n");
        end;
        ieee(mod);

    end;


endfunction
