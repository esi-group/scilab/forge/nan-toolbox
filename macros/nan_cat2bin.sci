function [B,BLab]=nan_cat2bin(D, Label, MODE)
    //   converts categorial into binary data
    //  Calling Sequence
    //   B = nan_cat2bin(C);
    //   [B,BinLabel] = nan_cat2bin(C,Label);
    //   [B,BinLabel] = nan_cat2bin(C,Label,MODE)
    // Parameters
    //  C        categorial data
    //  B        binary data
    //  Label    description of each column in C
    //  BinLabel description of each column in B
    //  MODE     default [], ignores NaN
    //           'notIgnoreNAN' includes binary column for NaN
    //           'IgnoreZeros'  zeros do not get a separate category
    //           'IgnoreZeros+NaN' zeros and NaN are ignored
    //  Description
    //   each category of each column in D is converted into a logical column
    //
    //  Examples
    //     nan_cat2bin([1;2;5;1;5])
    //   Authors
    //   Copyright (C) 2009 by Alois Schloegl a.schloegl@ieee.org
    //   H. Nahrstaedt

    //	$Id$
    //	Copyright (C) 2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    // This program is free software; you can redistribute it and/or
    // modify it under the terms of the GNU General Public License
    // as published by the Free Software Foundation; either version 3
    // of the  License, or (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program; if not, write to the Free Software
    // Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
    if nargin<3,
        MODE = [];
    end;
    mex_flag=%f;
    try
        mex_flag=(type(histo_mex)==130);
    catch
        mex_flag=%f;
    end
    mod=ieee();
    ieee(2);

    // convert data
    B = [];

    c     = 0;
    k1    = 0;
    BLab  = list();
    for m = 1:size(D,2)
        if mex_flag then
            h = histo_mex(D(:,m));
        else
            h = nan_histo3(D(:,m));
        end;

        x = h.X(h.H>0);
        if (MODE=="notIgnoreNaN")
            ;
        elseif (MODE=="IgnoreZeros")
            x = x(x~=0);
        elseif (MODE=="IgnoreZeros+NaN")
            x = x((x~=0) & (x==x));
        else
            x = x(x==x);
        end;
        for k = 1:size(D,1),
            if ~isnan(D(k,m))
                B(k, c + find(D(k,m)==x)) = 1;
            elseif isnan(x($)),
                B(k, c + length(x)) = 1;
            end;
        end;

        c = c + length(x);
        if nargout>1,
            for k = 1:length(x),
                k1 = k1+1;
                if isempty(Label)
                    BLab(k1) = ["#"+string(m)+":"+string(x(k))];
                else
                    BLab(k1) = [Label(m)+":"+string(x(k))];
                end;
            end;
        end;
    end;
    ieee(mod);
endfunction
