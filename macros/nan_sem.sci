function [SE,M]=nan_sem(x,DIM, W)
    //  calculates the standard error of the mean
    // Calling Sequence
    // [SE,M] = nan_sem(x [, DIM [,W]])
    //  Parameters
    // SE: standard error in dimension DIM
    // M:  mean
    // DIM:	dimension
    //	1: SEM of columns
    //	2: SEM of rows
    // 	N: SEM of  N-th dimension
    //	default or []: first DIMENSION, with more than 1 element
    // W:	weights to compute weighted mean and s.d. (default: [])
    //	if W=[], all weights are 1.
    //	number of elements in W must match size(x,DIM)
    // Description
    //   calculates the standard error (SE) in dimension DIM
    //   the default DIM is the first non-single dimension
    //   M returns the mean.
    //   Can deal with complex data, too.
    //
    // features:
    // - can deal with NaN's (missing values)
    // - weighting of data
    // - dimension argument
    // - compatible to Matlab and Octave
    //
    // See also
    //sumskipnan
    //nan_mean
    //nan_var
    //nan_std
    //Authors
    //	Copyright (C) 2000-2003,2008,2009 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.

    //	Copyright (C) 2000-2003,2008,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //	$Id$
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/
    mod=ieee();
    ieee(2);
    if nargin>2,
        [S,N,SSQ] = sumskipnan(x,DIM,W);
    elseif nargin>1,
        [S,N,SSQ] = sumskipnan(x,DIM);
    else
        [S,N,SSQ] = sumskipnan(x);
    end

    M  = S./N;
    SE = (SSQ.*N - real(S).^2 - imag(S).^2)./(N.*N.*(N-1));
    SE(SE<=0) = 0; 	// prevent negative value caused by round-off error
    SE = sqrt(real(SE));
    ieee(mod);
endfunction