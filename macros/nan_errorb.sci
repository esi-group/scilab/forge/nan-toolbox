function nan_errorb(x,y,varargin)
    // plot nice healthy error bars
    // Calling Sequence
    // nan_errorbr(Y,E)
    // nan_errorb(X,Y,E)
    // nan_errorb(X,Y,'Parameter','Value',...)
    // Parameters
    // Optional Parameters:
    //    horizontal: will plot the error bars horizontally rather than vertically
    //    top: plot only the top half of the error bars (or right half for horizontal)
    //    barwidth: the width of the little hats on the bars (default scales with the data!)
    //              barwidth is a scale factor not an absolute value.
    //    linewidth: the width of the lines the bars are made of (default is 2)
    //    points: will plot the points as well, in the same colors.
    //    color: specify a particular color for all the bars to be (default is black, this can be anything like 'blue' or [.5 .5 .5])
    //    multicolor: will plot all the bars a different color (thanks to my linespecer function)
    //                colormap: in the case that multicolor is specified, one
    //                           may also specify a particular colormap to
    //                           choose the colors from.
    // Description
    // It is possible to plot nice error bars on top of a bar plot .
    // This function plots what I would consider to be nice error bars as the
    // default, with no modifications necessary.
    // It also plots, only the error bars, and in black.
    //
    //
    // nan_errorbar(Y,E) plots Y and draws an error bar at each element of Y. The
    // error bar is a distance of E(i) above and below the curve so that each
    // bar is symmetric and 2*E(i) long.
    // If Y and E are a matrices, errob groups the bars produced by the elements
    // in each row and plots the error bars in their appropriate place above the
    // bars.
    //
    // nan_errorbar(X,Y,E) plots Y versus X with
    // symmetric error bars 2*E(i) long. X, Y, E must
    // be the same size. When they are vectors, each error bar is a distance of E(i) above
    // and below the point defined by (X(i),Y(i)).
    //
    // Examples
    // y=rand(1,5)+1; e=rand(1,5)/4;
    // bar(y,'green');
    // nan_errorb(y,e);
    //
    // defining x and y
    // x=linspace(0,2*%pi,8); y=sin(x); e=rand(1,8)/4;
    //  plot(x,y,'k','linewidth',2);
    // nan_errorb(x,y,e)
    //
    // group plot:
    // values=abs(rand(2,3))+1; errors=rand(2,3)/1.5+0;
    // nan_errorb(values,errors,'top');
    //
    // Authors
    // Jonathan Lansey Jan 2009
    // H. Nahrstaedt - 2011
    //



    //// Acknowledgments
    // Thank you to the AP-Lab at Boston University for funding me while I
    // developed these functions. Thank you to the AP-Lab, Avi and Eli for help
    // with designing and testing them.
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Jonathan Lansey Jan 2009,     questions to Lansey at gmail.com          //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //// first things first
    //save the initial hold state of the figure.
    // hold_state = ishold;
    // if ~hold_state
    //     cla;
    // end


    //// If you are plotting errobars on Matlabs grouped bar plot
    if size(x,1)>1 & size(x,2)>1 // if you need to do a group plot
        // Plot bars
        num2Plot=size(x,2);
        e=y; y=x;
        bar(x,0.9);
        //bar(x, 'edgecolor','k', 'linewidth', 2);
        handles_bars = gce();
        //hold on
        for i = 1:num2Plot
            x =handles_bars.children(i).data;
            //x=x(:);
            //x = mtlb_mean(x([1 2],:));
            x =handles_bars.children(i).data(:,1)'-handles_bars.children(i).x_shift;

            //       Now the recursive call!
            errorb(x(:),y(:,i), e(:,i),"barwidth",1/(num2Plot),varargin(:)); //errorb(x,y(:,i), e(:,i),'barwidth',1/(4*num2Plot),varargin{:});
        end
        //     if ~hold_state
        //         hold off;
        //     end
        return; // no need to see the rest of the function
    else
        x=x(:)';
        y=y(:)';
        num2Plot=length(x);
    end
    //// Check if X and Y were passed or just X

    if ~isempty(varargin)
        if type(varargin(1))==10
            justOneInputFlag=1;
            e=y; y=x; x=1:num2Plot;
        else
            justOneInputFlag=0;
            e=varargin(1)(:)';
        end
    else // not text arguments, not even separate 'x' argument
        e=y; y=x; x=1:length(e);
        justOneInputFlag=0;
    end

    //hold on; // axis is already cleared if hold was off
    //// Check that your vectors are the proper length
    if num2Plot~=length(e) | num2Plot~=length(y)
        error("your data must be vectors of all the same length")
    end

    //// Check that the errors are all positive
    signCheck=min(0,min(e(:)));
    if signCheck<0
        error("your error values must be greater than zero")
    end

    //// In case you don't specify color:
    color2Plot = [ 0 0 0];
    // set all the colors for each plot to be the same one. if you like black
    lineStyleOrder=list();
    for kk=1:num2Plot
        lineStyleOrder(kk)=color2Plot;
    end

    //// Initialize some things before accepting user parameters
    horizontalFlag=0;
    topFlag=0;
    pointsFlag=0;
    barFactor=1;
    linewidth=2;
    colormapper="jet";
    multicolorFlag=0;

    //// User entered parameters
    //  if there is just one input, then start at 1,
    //  but if X and Y were passed then we need to start at 2
    k = 1 + 1 - justOneInputFlag; //
    //
    while k <= length(varargin) & type(varargin(k))==10
        select (convstr(varargin(k)))
        case "horizontal" then
            horizontalFlag=1;
            if justOneInputFlag // need to switch x and y now
                x=y; y=1:num2Plot; // e is the same
            end
        case "color" then //  '': can be 'ampOnly', 'heat'
            color2Plot = varargin(k + 1);
            //       set all the colors for each plot to be the same one
            for kk=1:num2Plot
                lineStyleOrder(kk)=color2Plot;
            end
            k = k + 1;
        case "linewidth" then//  '': can be 'ampOnly', 'heat'
            linewidth = varargin(k + 1);
            k = k + 1;
        case "barwidth" then//  '': can be 'ampOnly', 'heat'
            barFactor = varargin(k + 1);
            //         barWidthFlag=1;
            k = k + 1;
        case "points" then
            pointsFlag=1;
        case "multicolor" then
            multicolorFlag=1;
        case "colormap" then// used only if multicolor
            colormapper = varargin(k + 1);
            k = k + 1;
        case "top" then
            topFlag=1;
        else
            warning("Dude, you put in the wrong argument");
            //         p_ematError(3, 'displayRose: Error, your parameter was not recognized');
        end
        k = k + 1;
    end

    if multicolorFlag
        lineStyleOrder=linspecer(num2Plot,colormapper);
    end

    //// Set the bar's width if not set earlier
    if num2Plot==1
        //   defaultBarFactor=how much of the screen the default bar will take up if
        //   there is only one number to work with.
        defaultBarFactor=20;
        a=gca();p=a.data_bounds;
        if horizontalFlag
            barWidth=barFactor*(p(4)-p(3))/defaultBarFactor;
        else
            barWidth=barFactor*(p(2)-p(1))/defaultBarFactor;
        end
    else // is more than one datum
        if horizontalFlag
            barWidth=barFactor*(y(2)-y(1))/4;
        else
            barWidth=barFactor*(x(2)-x(1))/4;
        end
    end

    //// Plot the bars
    for k=1:num2Plot
        if horizontalFlag
            ex=e(k);
            esy=barWidth/2;
            //       the main line
            if ~topFlag | x(k)>=0  //also plot the bottom half.
                plot([x(k)+ex x(k)],[y(k) y(k)],"color",lineStyleOrder(k),"linewidth",linewidth);
                //       the hat
                plot([x(k)+ex x(k)+ex],[y(k)+esy y(k)-esy],"color",lineStyleOrder(k),"linewidth",linewidth);
            end
            if ~topFlag | x(k)<0  //also plot the bottom half.
                plot([x(k) x(k)-ex],[y(k) y(k)],"color",lineStyleOrder(k),"linewidth",linewidth);
                plot([x(k)-ex x(k)-ex],[y(k)+esy y(k)-esy],"color",lineStyleOrder(k),"linewidth",linewidth);
                //rest?
            end
        else //plot then vertically
            ey=e(k);
            esx=barWidth/2;
            //         the main line
            if ~topFlag | y(k)>=0 //also plot the bottom half.
                plot([x(k) x(k)],[y(k)+ey y(k)],"color",lineStyleOrder(k),"linewidth",linewidth);
                //       the hat
                plot([x(k)+esx x(k)-esx],[y(k)+ey y(k)+ey],"color",lineStyleOrder(k),"linewidth",linewidth);
            end
            if ~topFlag | y(k)<0 //also plot the bottom half.
                plot([x(k) x(k)],[y(k) y(k)-ey],"color",lineStyleOrder(k),"linewidth",linewidth);
                plot([x(k)+esx x(k)-esx],[y(k)-ey y(k)-ey],"color",lineStyleOrder(k),"linewidth",linewidth);
            end
        end
    end
    //
    //// plot the points, very simple

    if pointsFlag
        for k=1:num2Plot
            plot(x(k),y(k),"o","markersize",8,"color",lineStyleOrder(k),"MarkerFaceColor",lineStyleOrder(k));
        end
    end

    // drawnow;
    // // return the hold state of the figure
    // if ~hold_state
    //     hold off;
    // end

endfunction



function lineStyleOrder=linspecer(N,varargin)

    // default colormap
    A=hotcolormap(50);

    //// interperet varagin
    if type(varargin(1))==10
        A=eval(varargin(1)+"colormap(50)");
    end

    ////

    if N<=0
        lineStyleOrder=list();
        return;
    end
    lineStyleOrder=list();
    values=round(linspace(1,50,N));
    for n=1:N
        lineStyleOrder(n) = A(values(n),:);
    end


endfunction



