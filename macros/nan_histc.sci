function [n, idx] = nan_histc (data, edges, dim)
    //Produce histogram counts.
    // Calling Sequence
    // n = nan_histc(y,edges);
    // n = nan_histc(y,edges,dim)
    // [n,idx] = nan_histc (...)
    // Description
    // When y is a vector, the function counts the number of elements of
    // y that fall in the histogram bins defined by edges.  This must be
    // a vector of monotonically non-decreasing values that define the edges of the
    // histogram bins.  So, n(k) contains the number of elements in
    // y for which edges(k) <= y < edges(k+1).
    // The final element of n contains the number of elements of y
    // that was equal to the last element of edges.
    //
    // When y is a N-dimensional array, the same operation as above is
    // repeated along dimension dim.  If this argument is given, the operation
    // is performed along the first non-singleton dimension.
    //
    // If a second output argument is requested an index matrix is also returned.
    // The idx matrix has same size as y.  Each element of idx
    // contains the index of the histogram bin in which the corresponding element
    // of y was counted.
    // Examples
    //
    // x = -2.9:0.1:2.9;
    // y = rand(10000,1,'normal');
    // n_elements = nan_histc(y,x);
    // c_elements = cumsum(n_elements);
    // bar(x,c_elements);
    // Authors
    // Copyright (C) 2009, Søren Hauberg
    // Copyright (C) 2009 VZLU Prague
    // H. Nahrstaedt - 2010


    //
    // This file is part of Octave.
    //
    // Octave is free software; you can redistribute it and/or modify it
    // under the terms of the GNU General Public License as published by
    // the Free Software Foundation; either version 3 of the License, or (at
    // your option) any later version.
    //
    // Octave is distributed in the hope that it will be useful, but
    // WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    // General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with Octave; see the file COPYING.  If not, see
    // <http://www.gnu.org/licenses/>.
    // Check input
    if (nargin < 2)
        error ("n = nan_histc(y,edges)");
    end

    sz = size (data);
    if (nargin < 3)
        dim = find (sz > 1, 1);
        if (isempty (dim))
            dim = 1;
        end
    end

    if (~isreal (data))
        error ("nan_histc: first argument must be real a vector");
    end

    // Make sure 'edges' is sorted
    num_edges = length (edges);
    if (num_edges == 0)
        error ("nan_histc: edges must not be empty")
    end

    if (isreal (edges))
        edges = edges (:);
        if ( ~and(diff(edges)>0) | edges(1) > edges($))
            warning ("nan_histc: edge values not sorted on input");
            edges = mtlb_sort (edges);
        end
    else
        error ("nan_histc: second argument must be a vector");
    end

    nsz = sz;
    nsz (dim) = num_edges;

    // the splitting point is 3 bins

    //  if (num_edges <= 3)

    // This is the O(M*N) algorithm.

    // Allocate the histogram
    n = mtlb_zeros (nsz);

    // Allocate 'idx'
    if (nargout > 1)
        idx = mtlb_zeros (sz);
    end

    // Prepare indices
    idx1 = cell (1, dim-1);
    for k = 1:length(idx1)
        idx1{k} = 1:sz(k);
    end
    
    idx2 = cell (length (sz) - dim);
    for k = 1:length(idx2)
        idx2{k} = 1:sz(k+dim);
    end

    // Compute the histograms
    for k = 1:num_edges-1
        b = (edges (k) <= data & data < edges (k+1));
        if length(idx1)==0
            n( k, idx2{:}) = sum(b, dim);
        elseif length(idx2)==0
            n(idx1{:}, k) = sum(b, dim);
        else
            n(idx1{:}, k, idx2{:}) = sum(b, dim);
        end
        if (nargout > 1)
            idx (b) = k;
        end
    end
    b = (data == edges ($));
    if length(idx1)==0
        n(num_edges, idx2{:}) = sum(b, dim);
    elseif length(idx2)==0
        n(idx1{:}, num_edges) = sum(b, dim);
    else
        n(idx1{:}, num_edges, idx2{:}) = sum(b, dim);
    end
    if (nargout > 1)
        idx (b) = num_edges;
    end

    //   else
    //
    //     // This is the O(M*log(N) + N) algorithm.
    //
    //     // Look-up indices.
    //     idx = lookup (edges, data);
    //     // Zero invalid ones (including NaNs). data < edges(1) are already zero.
    //     idx(~ (data <= edges($))) = 0;
    //
    //     iidx = idx;
    //
    //     // In case of matrix input, we adjust the indices.
    //     if (~ isvector (data))
    //       nl = prod (sz(1:dim-1));
    //       nn = sz(dim);
    //       nu = prod (sz(dim+1:$));
    //       if (nl ~= 1)
    //         iidx = (iidx-1) * nl;
    //         iidx += matrix (kron (ones (1, nn*nu), 1:nl), sz);
    //       endif
    //       if (nu ~= 1)
    //         ne =length (edges);
    //         iidx += matrix (kron (nl*ne*(0:nu-1), ones (1, nl*nn)), sz);
    //       end
    //     end
    //
    //     // Select valid elements.
    //     iidx = iidx(idx ~= 0);
    //
    //     // Call accumarray to sum the indexed elements.
    //     n = moc_accumarray (iidx(:), 1, nsz);
    //
    //   end

endfunction



