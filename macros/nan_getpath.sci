function [path]= nan_getpath()
    // Returns the path to the current module.
    // Calling Sequence
    //   path = nan_getpath ( )
    //  Parameters
    // path : a 1-by-1 matrix of strings, the path to the current module.
    //   Examples
    //    path = nan_getpath ( )
    // Authors
    // Holger Nahrstaedt

    [lhs, rhs] = argn()
    //apifun_checkrhs ( "nan_getpath" , rhs , 0:0 )
    //apifun_checklhs ( "nan_getpath" , lhs , 1:1 )

    path = get_function_path("nan_getpath")
    path = fullpath(fullfile(fileparts(path),".."));
endfunction
