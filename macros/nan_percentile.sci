function Q=nan_percentile(Y,q,DIM)
    //  calculates the percentiles of histograms and sample arrays.
    // Calling Sequence
    //  Q = nan_percentile(Y,q)
    //  Q = nan_percentile(Y,q,DIM)
    //  Q = nan_percentile(HIS,q)
    // Description
    //  Q = nan_percentile(Y,q,DIM)
    //
    //     returns the q-th percentile along dimension DIM of sample array Y.
    //     size(Q) is equal size(Y) except for dimension DIM which is size(Q,DIM)=length(Q)
    //
    //  Q = nan_percentile(HIS,q)
    //
    //     returns the q-th percentile from the histogram HIS.
    //     HIS must be a HISTOGRAM struct as defined in HISTO2 or HISTO3.
    //     If q is a vector, the each row of Q returns the q(i)-th percentile
    //
    // See also
    // nan_histo2
    // nan_histo3
    // nan_quantile
    // Authors
    // Copyright (C) 1996-2003,2005,2006,2007 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 1996-2003,2005,2006,2007 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.

    if nargin==2,
        Q = nan_quantile(Y,q/100);

    elseif nargin==3,
        Q = nan_quantile(Y,q/100,DIM);

    else
        error("At least two arguments needed!");

    end;



endfunction
