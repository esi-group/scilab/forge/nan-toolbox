function [model,y] = nan_kmeans(X,num_centers,Init_centers)
    // K-means clustering algorithm.
    // Calling Sequence
    //  [model,y] = nan_kmeans(X,num_centers)
    //  [model,y] = nan_kmeans(X,num_centers,Init_centers)
    // Description
    //  [model,y] = nan_kmeans(X,num_centers) runs K-means clustering
    //   where inital centers are randomly selected from the
    //   input vectors X. The output are found centers stored in
    //   structure model.
    //
    //  [model,y] = nan_kmeans(X,num_centers,Init_centers) uses
    //   init_centers as the starting point.
    // Parameters
    // Input:
    //  X [num_data x dim]: Input vectors.
    //  num_centers [1x1]: Number of centers.
    //  Init_centers [1x1]: Starting point of the algorithm.
    //
    // Output:
    //  model [struct]: Found clustering:
    //   .X [num_centers x dim]: Found centers.
    //
    //   .y [1 x num_centers]: Implicitly added labels 1..num_centers.
    //   .t [1x1]: Number of iterations.
    //   .MsErr: [1xt] Mean-Square error at each iteration.
    //
    //  y [1 x num_data]: Labels assigned to data according to
    //   the nearest center.
    //
    // Examples
    //  X = [rand(100,2,'normal')+ones(100,2);   rand(100,2,'normal')-ones(100,2)];
    //  [model,idx] = nan_kmeans( X, 2 );
    //  plot(X(idx==1,1),X(idx==1,2),'r.','MarkerSize',12)
    //  plot(X(idx==2,1),X(idx==2,2),'b.','MarkerSize',12)
    //  ctrs=model.X;
    //  plot(ctrs(:,1),ctrs(:,2),'ko', 'MarkerSize',12,'LineWidth',2)
    //  plot(ctrs(:,1),ctrs(:,2),'kx', 'MarkerSize',12,'LineWidth',2);
    // legend('Cluster 1','Cluster 2','Centroids',  'Location','NW')
    // Authors
    // Written by Vojtech Franc and Vaclav Hlavac,
    // H. Nahrstaedt - 2010

    // (c) Statistical Pattern Recognition Toolbox, (C) 1999-2003,
    // Written by Vojtech Franc and Vaclav Hlavac,
    // <a href="http://www.cvut.cz">Czech Technical University Prague</a>,
    // <a href="http://www.feld.cvut.cz">Faculty of Electrical engineering</a>,
    // <a href="http://cmp.felk.cvut.cz">Center for Machine Perception</a>

    // Modifications:
    // 18-dec-2008, VF, fix: Init_centers argument used as the initial solution
    // 17-jun-2007, VF, renamed from kmeans to cmeans to avoid conflicts with stats toolbox
    // 12-may-2004, VF
    X=X';
    [dim,num_data] = size(X);

    // random inicialization of class centers
    //-----------------------------------------------
    if nargin < 3,
        inx=randperm(num_data);
        model.X = X(:,inx(1:num_centers));
        model.y = 1:num_centers;
        model.K = 1;
    else
        if num_centers ~= size(Init_centers,2)
            error("Argument num_centers must be equal to the number of columns of Init_centers.");
        end
        model.X = Init_centers;
        model.y = 1:num_centers'
    end

    model.fun = "knnclass";

    old_y = zeros(1,num_data);
    t = 0;
    model.MsErr=[];
    // main loop
    //-------------------------
    while 1,

        t = t+1;

        // classificitation
        y = knnclass( X, model );

        // computation of class centers
        err = 0;
        for i=1:num_centers,
            inx = find(y == i);

            if ~isempty(inx),

                // compute approximation error
                err = err + sum(sum((X(:,inx) - model.X(:,i)*ones(1,length(inx)) ).^2));

                // compute new centers
                model.X(:,i) = sum(X(:,inx),2)/length(inx);
            end
        end

        // Number of iterations and Mean-Square Error
        model.t = t;
        model.MsErr(t) = err/num_data;

        if sum( abs(y - old_y) ) == 0,
            model.X = model.X';
            return;
        end

        old_y = y;
    end
    model.X = model.X';

endfunction


function retval = randperm (n)
    if (nargin == 1 & sum(length(n))==1 & floor (n) == n)
        if (n >= 0)
            [junk, retval] = mtlb_sort (rand (1, n));
        else
            error ("randperm: argument must be non-negative");
        end
    else
        error ("retval = randperm (n)");
    end

endfunction


function model=knnrule(data,K)
    // KNNRULE Creates K-nearest neighbours classifier.
    //
    // Synopsis:
    //  model=knnrule(data)
    //  model=knnrule(data,K)
    //
    // Description:
    //  It creates model of the K-nearest neighbour classifier.
    //
    // Input:
    //  data.X [dim x num_data] Prototypes (training) data.
    //  data.y [1 x num_data] Labels of training data.
    //  K [1x1] Number of the nerest neighbours (default 1).
    //
    // Output:
    //  model [struct] Model of K-NN classifier.
    //   .X = data.X.
    //   .y = data.y.
    //   .K = K.
    //   .num_data [1x1] number of prototypes.
    //   .fun [string] Contains 'knnclass'.
    //
    // Example:
    //  data=load('riply_trn');
    //  model=knnrule(data,1);
    //  figure; ppatterns(data); pboundary(model);
    //
    // See also
    //  KNNCLASS.
    //

    //data=c2s(data);

    if nargin <2, K=1; end

    model=data;
    model.fun="knnclass";
    model.K=K;
    model.num_data = size(data.X,2);

endfunction

function y = knnclass(X,model)
    // KNNCLASS k-Nearest Neighbours classifier.
    //
    // Synopsis:
    //  y = knnclass(X,model)
    //
    // Description:
    //  The input feature vectors X are classified using the K-NN
    //  rule defined by the input model.
    //
    // Input:
    //  X [dim x num_data] Data to be classified.
    //  model [struct] Model of K-NN classfier:
    //   .X [dim x num_prototypes] Prototypes.
    //   .y [1 x num_prototypes] Labels of prototypes.
    //   .K [1x1] Number of used nearest-neighbours.
    //
    // Output:
    //  y [1 x num_data] Classified labels of testing data.
    //
    // Example:
    //  trn = load('riply_trn');
    //  tst = load('riply_tst');
    //  ypred = knnclass(tst.X,knnrule(trn,5));
    //  cerror( ypred, tst.y )
    //
    // See also
    //  KNNRULE.
    //

    // (c) Statistical Pattern Recognition Toolbox, (C) 1999-2003,
    // Written by Vojtech Franc and Vaclav Hlavac,
    // <a href="http://www.cvut.cz">Czech Technical University Prague</a>,
    // <a href="http://www.feld.cvut.cz">Faculty of Electrical engineering</a>,
    // <a href="http://cmp.felk.cvut.cz">Center for Machine Perception</a>

    // Modifications:
    // 19-may-2003, VF
    // 18-sep-2002, V.Franc

    //X=c2s(X);
    //model=c2s(model);

    if ~isfield(model,"K"), model.K=1; end;

    y = knnclass_mex(X,model.X,model.y, model.K);

endfunction;
