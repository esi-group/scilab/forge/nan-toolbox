function p = tcdf(x,n)
// returns student cumulative distribtion function
// Calling Sequences
// cdf = tcdf(x,DF);
// Parameters
// Computes the CDF of the students distribution 
//    with DF degrees of freedom 
// x,DF must be matrices of same size, or any one can be a scalar. 
//
// See also
// normcdf
// tpdf
// tinv
// Authors
// Copyright (C) 2000-2003,2009 by Alois Schloegl a.schloegl@ieee.org
//  H. Nahrstaedt - 2010


//	$Id$
//	Copyright (C) 2000-2003,2009 by Alois Schloegl <a.schloegl@ieee.org>	
//    	This is part of the NaN-toolbox. For more details see
//       http://biosig-consulting.com/matlab/NaN/

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; If not, see <http://www.gnu.org/licenses/>.


// check size of arguments
if and(size(x)==1)
	x = mtlb_repmat(x,size(n));
elseif and(size(n)==1)
	n = mtlb_repmat(n,size(x));
elseif and(size(x)==size(n))
	;	//// OK, do nothing
else
	error('size of input arguments must be equal or scalar')
end; 	
		
// allocate memory
p = zeros(size(x,1),size(x,2));
p(isinf(x) & (n>0)) = 1;

// workaround for invalid arguments in BETAINC
ix   = isnan(x) | ~(n>0);
p(ix)= %nan;

ix    = (x > -%inf) & (x < %inf) & (n > 0);
p(ix) = betainc (n(ix) ./ (n(ix) + x(ix).^2), n(ix)/2, 1/2) / 2;

ix    = find(x>0);
p(ix) = 1 - p(ix);

// shape output
p = matrix(p,size(x));

endfunction
