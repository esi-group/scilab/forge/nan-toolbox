function [o] = nanmean(i,DIM)
// nanmean same as mean but ignores NaN's. 
// Calling Sequence
// Y = nanmean(x [,DIM])
// Parameters
// DIM:	dimension
//	1 sum of columns
//	2 sum of rows
//	default or []: first DIMENSION with more than 1 element
// Y:	resulting mean
// Description
// NANMEAN is OBSOLETE; use MEAN instead. NANMEAN is included 
//    to provide backward compatibility 
// See also
// mean
// sumskipnan
// nansum
// Authors
// Copyright (C) 2009 by Alois Schloegl <a.schloegl@ieee.org>	
// H. Nahrstaedt - 2010

//	$Id: nanmean.m 6973 2010-02-28 20:19:12Z schloegl $
//    	Copyright (C) 2009 by Alois Schloegl <a.schloegl@ieee.org>	
//    	This is part of the NaN-toolbox. For more details see
//       http://biosig-consulting.com/matlab/NaN/
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; If not, see <http://www.gnu.org/licenses/>.

if nargin>1
        [o,n] = sumskipnan(i,DIM);
else
        [o,n] = sumskipnan(i);
end;
o=o./n;
endfunction