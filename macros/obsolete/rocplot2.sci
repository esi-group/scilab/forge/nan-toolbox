function auc = rocplot(y,x,params)
//plotroc draws the recevier operating characteristic(ROC) curve.
//
//auc = plotroc(training_label, training_instance [, libsvm_options -v cv_fold]) 
//  Use cross-validation on training data to get decision values and plot ROC curve.
//
//auc = plotroc(testing_label, testing_instance, model) 
//  Use the given model to predict testing data and obtain decision values
//  for ROC
//
// Example:
//   
// 	load('heart_scale.mat'); 
// 	rocplot(heart_scale_label, heart_scale_inst,'-v 5');
//
//	[y,x] = readsparse('heart_scale');
//   	model = svmtrain(y,x);
// 	rocplot(y,x,model);
	rand('state',0); // reset random seed
	if nargin < 2
		help plotroc
		return
	elseif isempty(y) | isempty(x)
		error('Input data is empty');
	elseif sum(y == 1) + sum(y == -1) ~= length(y)
		error('ROC is only applicable to binary classes with labels 1, -1'); // check the trainig_file is binary
	elseif exists('params') & type(params)~=10
		model = params;
		[predict_label,mse,deci] = svmpredict(y,x,model); // the procedure for predicting
		auc = roc_curve(deci,y);
	else
		if ~exists('params')
			params = [];
		end
		[param,fold] = proc_argv(params); // specify each parameter
		if fold <= 1
			error('The number of folds must be greater than 1');
		else	
			[deci,label_y] = get_cv_deci(y,x,param,fold); // get the value of decision and label after cross-calidation
			auc = roc_curve(deci,label_y); // plot ROC curve
		end
	end
endfunction


function [AREA,d,SEN,SPEC,ACC] = auc(d,c,color);
// AUC calculates Area-under-ROC curve
//
// function [AREA,TH,SEN,SPEC,ACC] = auc(d,c,color);
// d     DATA
// c     CLASS, vector with 0 and 1 
// color optional, plots ROC curve
// 
// function [AREA,TH,SEN,SPEC,ACC]=auc(d1,d0,color);
// d1    DATA of class 1 
// d2    DATA of class 0
// color optional, plots ROC curve
// 
// OUTPUT:
// AREA    area under ROC curve
// TH      Threshold
// SEN     sensitivity
// SPEC    specificity
// ACC     accuracy

//	$Id: auc.m,v 1.3 2005/04/29 14:32:04 schloegl Exp $
//	Copyright (c) 1997-2003,2005 by  Alois Schloegl <a.schloegl@ieee.org>	
//    	This is part of the BIOSIG-toolbox http://biosig.sf.net/
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the
// Free Software Foundation, Inc., 59 Temple Place - Suite 330,
// Boston, MA  02111-1307, USA.
//

MODE = and(size(d)==size(c)) & and(and((c==1) | (c==0)));
d=d(:);
c=c(:);
        
if ~MODE
        d2=c;
        c=[ones(size(d));zeros(size(d2))];
        d=[d;d2];
        printf('Warning AUC: size of input arguments do not fit\n');
end;        

if nargin<3
        color='-';
end;

// handle (ignore) NaN's  
c = c(~isnan(d));
d = d(~isnan(d));

[d,I] = mtlb_sort(d);
x = c(I);

FN   = cumsum(x==1)/sum(x==1);
TN   = cumsum(x==0)/sum(x==0);
AREA = diff(FN)' * (TN(1:$-1)+TN(2:$))/2;

if nargin>2,  
        plot2d((1-TN($:-1:1))*100,(1-FN($:-1:1))*100);
end;

if nargout<3, return; end; 
TP = 1-FN;
SEN = TP./(TP+FN);

if nargout<4, return; end; 
FP = 1-TN;
SPEC= TN./(TN+FP);

if nargout<5, return; end; 
ACC = (TP+TN)./(TP+TN+FP+FN);
endfunction

function [SEN,SPEC,d,ACC,AREA,YI,c]=roc(d,c,color);
// ROC receiver operator curve and derived statistics.
// [...] = roc(d,c);
// d     DATA
// c     CLASS, vector with 0 and 1 
// 
// [...]=roc(d1,d0);
// d1    DATA of class 1 
// d2    DATA of class 0
// 
// [SEN, SPEC, TH, ACC, AUC,Yi,idx]=roc(...);
// OUTPUT:
// SEN     sensitivity
// SPEC    specificity
// TH      Threshold
// ACC     accuracy
// AUC     area under ROC curve
// Yi 	  max(SEN+SPEC-1), Youden index 
// c	  TH(c) is the threshold that maximizes Yi 

//	$Id: roc.m,v 1.2 2005/04/29 14:32:04 schloegl Exp $
//	Copyright (c) 1997-2003,2005,2007 by  Alois Schloegl <a.schloegl@ieee.org>	
//    	This is part of the BIOSIG-toolbox http://biosig.sf.net/
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the
// Free Software Foundation, Inc., 59 Temple Place - Suite 330,
// Boston, MA  02111-1307, USA.
//


// Problem : Wenn die Schwellwerte mehrfach vorkommen, kann es zu Ambiguiten kommen, welche sich auf die AUC auswirken.

MODE = and(size(d)==size(c)) & and(and((c==1) | (c==0)));
d=d(:);
c=c(:);
        
if ~MODE
        d2=c;
        c=[ones(size(d));zeros(size(d2))];
        d=[d;d2];
        fprintf(2,'Warning ROC: XXX\n')        
end;        

// handle (ignore) NaN's  
c = c(~isnan(d));
d = d(~isnan(d));

if nargin<3
        color='-';
end;

[D,I] = sort(d);
x = c(I);

// identify unique elements
if 0,
        printf('Warning ROC: please take care\n');
        tmp= find(diff(D)>0);
        tmp=mtlb_sort([1; tmp; tmp+1; length(d)]);//]',2*length(tmp+2),1);
        //D=d(tmp);
end;

FNR = cumsum(x==1)/sum(x==1);
TPR = 1-FNR;

TNR = cumsum(x==0)/sum(x==0);
FPR = 1-TNR;

FN = cumsum(x==1);
TP = sum(x==1)-FN;

TN = cumsum(x==0);
FP = sum(x==0)-TN;

SEN = TP./(TP+FN);
SPEC= TN./(TN+FP);
ACC = (TP+TN)./(TP+TN+FP+FN);

// SEN = [FN TP TN FP SEN SPEC ACC D];

//fill(TN/sum(x==0),FN/sum(x==1),'b');
//SEN=SEN(tmp,:);
//ACC=ACC(tmp);
//d=D(tmp);
d=D;

//plot(SEN(:,1)*100,SPEC*100,color);
plot(FPR*100,TPR*100,color);
//plot(FP*100,TP*100,color);
// fill([1; FP],[0;TP],'c');

//ylabel('Sensitivity (true positive ratio) [//]');
//xlabel('1-Specificity (false positive ratio) [//]');

// area under the ROC curve
AREA = -diff(FPR)' * (TPR(1:end-1)+TPR(2:end))/2;

// Youden index
[YI,c] = max(SEN+SPEC-1);
endfunction

function [resu,fold] = proc_argv(params)
	resu=params;
	fold=5;
	if ~isempty(params) & ~isempty(regexp(params,'-v'))
        [fold_val,fold_start,fold_end] = regexp(params,'-v\s+\d+','match','start','end');
        if ~isempty(fold_val)
            [temp1,fold] = strread([fold_val(:)],'%s %u');
            resu([fold_start:fold_end]) = [];
        else
            error('Number of CV folds must be specified by ""-v cv_fold""');
        end
    end
endfunction

function [deci,label_y] = get_cv_deci(prob_y,prob_x,param,nr_fold)
	l=length(prob_y);
	deci = ones(l,1);
	label_y = ones(l,1);	 
	rand_ind = randperm(l); 
	for i=1:nr_fold // Cross training : folding
		test_ind=rand_ind([floor((i-1)*l/nr_fold)+1:floor(i*l/nr_fold)]');
		train_ind = [1:l]';
		train_ind(test_ind) = [];
		model = svmtrain(prob_y(train_ind),prob_x(train_ind,:),param);	   
		[predict_label,mse,subdeci] = svmpredict(prob_y(test_ind),prob_x(test_ind,:),model);
		deci(test_ind) = subdeci.*model.Label(1);
		label_y(test_ind) = prob_y(test_ind);
	end
endfunction

function auc = roc_curve(deci,label_y)
	//[val,ind] = mtlb_sort(deci,'descend');
        label_y=cl101(label_y);
        [val,ind] = gsort(deci*label_y(1));
	roc_y = label_y(ind);
	stack_x = cumsum(roc_y == -1)/sum(roc_y == -1);
	stack_y = cumsum(roc_y == 1)/sum(roc_y == 1);
	auc = sum((stack_x(2:length(roc_y),1)-stack_x(1:length(roc_y)-1,1)).*stack_y(2:length(roc_y),1))

        //Comment the above lines if using perfcurve of statistics toolbox
        //[stack_x,stack_y,thre,auc]=perfcurve(label_y,deci,1);
	plot(stack_x,stack_y);
	xlabel('False Positive Rate');
	ylabel('True Positive Rate');
	title(['ROC curve of (AUC = ' string(auc) ' )']);
endfunction

function [CL101,Labels] = cl101(classlabel)
	//// convert classlabels to {-1,1} encoding 


	if (and(classlabel>=0) & and(classlabel==fix(classlabel)) & (size(classlabel,2)==1))
		M = max(classlabel);
		if M==2, 
			CL101 = (classlabel==2)-(classlabel==1); 
		else	
			CL101 = zeros(size(classlabel,1),M);
			for k=1:M, 
				//// One-versus-Rest scheme 
				CL101(:,k) = 2*bool2s(classlabel==k) - 1; 
			end; 
		end; 	
		CL101(isnan(classlabel),:) = %nan; //// or zero ??? 
	        Labels = min(classlabel):M;
	elseif and((classlabel==1) | (classlabel==-1)  | (classlabel==0) )
		CL101 = classlabel; 
		M = size(CL101,2); 
                Labels = 1:M;
	else 
		classlabel,
		error('format of classlabel unsupported');
	end; 
	

endfunction

