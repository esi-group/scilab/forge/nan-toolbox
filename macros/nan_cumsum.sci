function x=nan_cumsum(x,DIM)
    //  Cumulative sum while skiping NaN's.
    // Calling Sequence
    // y = nan_cumsum(x,DIM)
    // Parameters
    // DIM:	dimension
    //	1 cumsum of columns
    //	2 cumsum of rows
    // 	N cumsum of  N-th dimension
    //	default or []: first DIMENSION, with more than 1 element
    // x:  input data
    // y:	resulting sum
    // Description
    // NaN values are not thown away, but skipped.
    // Examples
    // x=[1 2 %nan 5];
    // cumsum(x)
    // ans  =
    //
    //    1.    3.    Nan   Nan
    //  cumsum(thrownan(x))
    //    ans  =
    //
    //    1.    3.    8.
    //  nan_cumsum(x)
    //  ans  =
    //
    //    1.    3.    Nan   8.
    //
    // See also
    // sumskipnan
    // Authors
    //	Copyright (C) 2000-2003,2008,2009 by Alois Schloegl a.schloegl@ieee.org
    //   H.  Nahrstaedt


    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.


    //	$Id$
    //	Copyright (C) 2000-2003,2008,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/
    mod=ieee();
    ieee(2);
    if nargin<2,
        DIM = [];
    end;
    if isempty(DIM),
        DIM = find(size(x)>1,1);
        if isempty(DIM), DIM=1; end;
    end;

    i = isnan(x);
    x(i) = 0;

    if nargin==2,

        x = cumsum(x,DIM);
        x(i) = %nan;
    elseif nargin==1

        x = cumsum(x,DIM);
        x(i) = %nan;
    else
        ieee(mod);
        error("error");
    end;

    ieee(mod);

endfunction
