function R=nan_tiedrank(X,DIM)
    // compute rank of samples, the mean value is used in case of ties
    //  Calling Sequences
    //    R = nan_tiedrank(X)
    //    Parameters
    //	R:  rank of vector X
    // Description
    //  this function is just a wrapper for RANKS, and provided for compatibility
    //  with the statistics toolbox of matlab(tm)
    //  Examples
    //  //Counting from smallest to largest, the two 20 values are 2nd and 3rd, so they both get rank 2.5 (average of 2 and 3):
    //  nan_tiedrank([10 20 30 40 20]);
    //
    // See also
    // nan_ranks
    // Authors
    // Copyright (C) 2009,2010 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010


    //	$Id$
    //	Copyright (C) 2009,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.

    if nargin>2,
        error("more than 2 input argument is currently not supported ")
    end;

    if nargout>1,
        error("more than 1 output argument is currently not supported ")
    end;
    mod=ieee();
    ieee(2);
    if nargin<2,
        DIM = [];
    end;
    if isempty(DIM),
        DIM = find(size(X)>1,1);
        if isempty(DIM), DIM = 1; end;
    end
    if (DIM<1), DIM = 1; end; //// Hack, because min([])=0 for FreeMat v3.5

    R = nan_ranks(X,DIM);
    ieee(mod);
endfunction
