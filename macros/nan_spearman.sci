function r = nan_spearman(x,y)
    //  Spearman's rank correlation coefficient.
    // Calling Sequence
    // [R,p,ci1,ci2] = CORRCOEF(x, [y, ] 'Rank');
    // Description
    // This function is replaced by CORRCOEF.
    // Significance test and confidence intervals can be obtained from CORRCOEF.
    //
    // For some (unknown) reason, in previous versions Spearman's rank correlation
    //   r = nan_corrcoef(ranks(x)).
    //
    // But according to [1], Spearman's correlation is defined as
    //   r = 1-6*sum((ranks(x)-ranks(y)).^2)/(N*(N*N-1))
    // The results are different. Here, the later version is implemented.
    //
    // See also
    // nan_corrcoef
    // nan_rankcorr
    // Bibliography
    // [1] http://mathworld.wolfram.com/SpearmanRankCorrelationCoefficient.html
    // [2] http://mathworld.wolfram.com/CorrelationCoefficient.html
    // Authors
    // Copyright (C) 2000-2002 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //    $Id$
    //    Copyright (C) 2000-2002 by Alois Schloegl <a.schloegl@ieee.org>
    //    This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.


    // warning('SPEARMAN might become obsolete; use CORRCOEF(...,''Spearman'') instead');
    if nargin < 2
        r = nan_corrcoef(x,"Spearman");
    else
        r = nan_corrcoef(x,y,"Spearman");
    end
endfunction
