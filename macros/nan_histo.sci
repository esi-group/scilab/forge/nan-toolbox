function [H,X]=nan_histo(Y,Mode,opt_plot)
    // calculates histogram for each column
    // Calling Sequence
    // [H,X] = nan_histo(Y,Mode)
    // Parameters
    //   input:
    //   Mode :
    //	'rows' : frequency of each row
    //	'1x'   : single bin-values (default Mode)
    //	'nx'   : separate bin-values for each column
    //      'plot':	if one input parameter is 'plot', the histogramm bar(X,H) will be plotted
    //   output:
    //   X : are the bin-values
    //   H : is the frequency of occurence of value X
    // Description
    // more histogram-based results can be obtained by hist2res
    //
    // Bibliography
    //  C.E. Shannon and W. Weaver "The mathematical theory of communication" University of Illinois Press, Urbana 1949 (reprint 1963).
    // Examples
    // [H,X] =  nan_histo( [ 9 9 9 9 2 2 3 3 4 5 9 ]' )
    //
    // See also
    //   nan_histo2
    //   nan_histo3
    //   nan_histo4
    //   nan_hist2res
    // Authors
    //	Copyright (C) 1996-2002,2008 by Alois Schloegl a.schloegl@ieee.org
    //	H. Nahrstaedt - 2010

    //	$Id: histo.m 5148 2008-06-27 10:36:37Z schloegl $
    //	Copyright (C) 1996-2002,2008 by Alois Schloegl <a.schloegl@ieee.org>
    //    	This is part of the TSA-toolbox
    //	http://hci.tugraz.at/~schloegl/matlab/tsa/
    //
    //    This program is free software: you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation, either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    if (nargin == 0),
        error("At least 1 parameter required");
    end;
    mod=ieee();
    ieee(2);
    in_par=["Y","Mode","opt_plot"];
    in_par_min=1;
    plotting=0;
    if (nargin>in_par_min)
        if (type(evstr(in_par(nargin)))==10)
            if convstr(evstr(in_par(nargin)))=="plot",   plotting=1; end;
            clear evstr(in_par(nargin));
            nargin=nargin-1;
        end;
    end;
    clear in_par;


    if nargin<2,
        Mode="1x";
    end;
    Mode=convstr(Mode);

    if (Mode=="rows")
        R = nan_histo4(Y);

    elseif (Mode=="column")
        R = nan_histo4(Y');
        R.X = R.X';

    elseif (Mode=="1x")
        R = nan_histo3(Y);

    elseif (Mode=="nx")
        R = nan_histo2(Y);

    end;


    H = R.H;
    X = R.X;

    if plotting,
        if or(size(X)==1),
            //if exist('OCTAVE_VERSION')<5,
            //        bar(R.X,R.H,'stacked');
            //else
            bar(R.X,R.H);
            a=gca();
            a.auto_ticks(1)="on";
            //end
        else
            warning("2-dim X-values not supported!")
            //bar3(R.X,R.H);
        end;
    end;
    ieee(mod);
endfunction
