function [Y,Z] = nan_filter(B,A,X,z);
    //   is able to filter data with missing values encoded as NaN.
    //  Calling Sequence
    //  [Y,Z] = nan_filter(B,A,X [, Z]);
    //
    // Describtion
    // If X contains no missing data, nan_filter should behave like filter.
    // NaN-values are handled gracefully.
    //
    // See also
    // sumskipnan
    // nan_conv
    //
    // Authors
    // Copyright (C) 2000-2002,2010 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2011
    //



    //	$Id$
    //	Copyright (C) 2005,2011 by Alois Schloegl <alois.schloegl@gmail.com>
    //       This function is part of the NaN-toolbox available at
    //       http://pub.ist.ac.at/~schloegl/matlab/NaN/ and
    //	http://octave.svn.sourceforge.net/viewvc/octave/trunk/octave-forge/extra/NaN/inst/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    mod=ieee();
    ieee(2);
    na = max(size(A));
    nb = max(size(B));
    if or(size(X)==1)
        nc = 1;
    else
        nc = size(X,2);
    end;

    if nargin<4,
        [t,Z.S] = filter(B,A,zeros(na+nb,nc));
        [t,Z.N] = filter(B,A,zeros(na+nb,nc));
    elseif or(type(z)==[1 4 8]),
        Z.S = z;
        [t, Z.N] = filter(B, A, zeros(na+nb,nc));
    elseif typeof(z)=="st",
        Z = z;
    end;

    NX        = isnan(X);
    X(NX)     = 0;

    [Y , Z.S] = filter(B, A,   X, Z.S);
    [NY, Z.N] = filter(B, A, ~NX, Z.N);
    Y = (sum(B)/sum(A)) * Y./NY;
    ieee(mod);
endfunction

