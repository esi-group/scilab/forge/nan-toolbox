function [auc, auh, acc0, accm, thrm, thrs, acc, sens, speci, hull] = nan_rocplot(scores, classes, plottype, Nthr)
    //plot a Receiver Operating Characteristic (ROC) curve
    // Calling Sequence
    //   nan_rocplot(scores, classes);
    //   nan_rocplot(scores, classes, plottype);
    //   nan_rocplot(scores, classes, plottype, Nthr);
    //   [AUC AUH] = nan_rocplot(scores, classes);
    //   [AUC AUH acc0 accM thrM thr acc sens speci hull] = nan_rocplot(...);
    // Parameters
    // scores : output of the classifier
    // classes : Boolean vector of the same size as scores
    // plottype:  controls what is plotted, it defaults to 1, where:
    //   0 : gives no plot (useful to get AUC without creating a plot)
    //   1 : gives a standard ROC curve, with sensitivity vs (1 - specificity)
    //   2 : gives my preferred convention, with sensitivity vs specificity
    // Nthr : is an optional number of thresholds to consider (points in the ROC),
    // if left off, all unique values in scores are considered, or, if there are
    // more than 100 of those, then 100 values equally spaced from min to max of
    // scores are used. Alternatively, Nthr can be a vector of thresholds.
    //  AUC :  area under the ROC curve,
    //  AUH : area under the convex hull
    //  acc0 : accuracy at a threshold of zero
    //  accM :  max accuracy from the tested set of thresholds
    //  thrM : threshold which leads to the max accuracy from the tested set of thresholds
    //  thr : considered thresholds,
    //  acc : corresponding accuracies
    //  sens : sensitivities
    //  speci :specificities
    //  hull: convex hull
    //Description
    // ROC curves illustrate the performance on a binary classification problem
    // where classification is based on simply thresholding a set of scores at
    // varying levels. Low thresholds give high sensitivity but low specificity,
    // high thresholds give high specificity but low sensitivity; the ROC curve
    // plots this trade-off over a range of thresholds.
    //
    // classes is a Boolean vector of the same size as scores, which should
    // be true where scores >= threshold should yield true, e.g. true for
    // patients, and false for healthy control subjects, in medical diagnosis.
    // If you have two vectors of scores, e.g. patient and control, first do:
    //   scores  = [control(:); patient(:)];
    //   classes = [false(length(control), 1); true(length(patient), 1)];
    //
    // AUC is the area under the ROC curve, a measure of overall accuracy,
    // which gives the probability that the classifier would rank a randomly
    // chosen true instance (e.g. patient) higher than a random false one
    // (e.g. control subject).
    //
    // AUH is the area under the convex hull of the ROC curve, which is of
    // interest because it is theoretically possible to operate at any point on
    // the convex hull of the points in an ROC curve (by using some proportional
    // selection of classifiers that operate at two points defining the relevant
    // section of the convex hull. This is just a more complex version of the
    // logic that gives the null line for an ROC plot; if a threshold of -inf
    // gives (speci=0,sens=1) and +inf gives (1,0), then using -inf for half of
    // your data and +inf for the other half is expected to give (0.5,0.5) if
    // you have equal numbers of true and false instances).
    //
    // Also recorded in the plot legend, and optionally returned, are acc0, the
    // accuracy at a threshold of zero, which is of special importance
    // in some algorithms, e.g. if your scores come from a linear classifier
    // like a Support Vector Machine which can give scores(i) = w'*x(:, i) - b,
    // as equivalent to testing for w'*x(:, i) > b, and accM, the max accuracy
    // from the tested set of thresholds, which occurs at the threshold thrM.
    //
    // The function can also return a vector of all considered thresholds, along
    // with the corresponding accuracies, sensitivities and specificities, in
    // the variables thr, acc, sens, speci. The output hull contains the indices
    // into sens and speci that give the convex hull. These arguments can then be
    // used to plot multiple ROC curves and/or convex hulls on the same axis,
    // e.g. after calling rocplot twice (with plottype 0), you could do:
    //   plot(speci1,sens1,'b', speci2,sens2,'r', [0 1],[1 0],'g');
    // Authors
    // Copyright 2009 Ged Ridgway
    // H. Nahrstaedt - 2010


    //
    // See also: roc, prec_rec, get_concave_ROC, auroc, bookmaker
    //
    // These can be found in the MATLAB Central File Exchange:
    // roc             http://www.mathworks.com/matlabcentral/fileexchange/19950
    // roc             http://www.mathworks.com/matlabcentral/fileexchange/21318
    // prec_rec        http://www.mathworks.com/matlabcentral/fileexchange/21528
    // get_concave_ROC http://www.mathworks.com/matlabcentral/fileexchange/21382
    // auroc           http://www.mathworks.com/matlabcentral/fileexchange/19468
    // bookmaker       http://www.mathworks.com/matlabcentral/fileexchange/5648
    // There is also a book "Ordinal Data Modeling", by Valen Johnson, with
    // companion code  http://www.mathworks.com/matlabcentral/fileexchange/2264
    mod=ieee();
    ieee(2);
    scores = scores(:);
    // need max thr greater than max(scores) so roc sens drops to zero
    maxscoreplus = max(scores) + %eps;//ps(max(scores));
    classes = classes(:);
    if or(double((classes)==1) ~= double(classes))
        ieee(mod);
        error("Classes vector must contain true (or 1) and false (or 0) only")
    end
    classes=bool2s(classes);

    if ~exists("Nthr") | isempty(Nthr)
        if length(scores) > 100
            Nthr = 100; // no need for more points in plot
        else
            thrs = unique([scores;maxscoreplus]);
            Nthr = length(thrs);
        end
    end
    if ~exists("thrs")
        if ~sum(length(Nthr))==1 // user-specified set of thrs to consider
            thrs = unique(Nthr); // (also sorts)
            Nthr = length(thrs);
            // thrs(Nthr) = maxscoreplus; // commented out, assuming expert user.
        else
            thrs = linspace(min(scores), maxscoreplus, Nthr);
        end
    end
    thrs = thrs(:);

    acc = zeros(Nthr, 1); sens = acc; speci = sens;
    for n = 1:Nthr
        thr = thrs(n);
        result = bool2s(scores >= thr);
        acc(n) = mean(bool2s(result == classes));
        sens(n) = sum(bool2s(result == 1 & classes == 1))/sum(bool2s(classes == 1));
        speci(n) = sum(bool2s(result == 0 & classes == 0))/sum(bool2s(classes == 0));
    end
    // area under ROC curve
    auc = trapz(speci, sens);
    // test special case of 0 threshold, since training often assumes this
    result0 = bool2s(scores >= 0);
    acc0 = mean(bool2s(result0 == classes));
    sens0 = sum(bool2s(result0 == 1 & classes == 1))/sum(bool2s(classes == 1));
    speci0 = sum(bool2s(result0 == 0 & classes == 0))/sum(bool2s(classes == 0));
    // threshold giving highest overall accuracy
    [accm indm] = max(acc);
    thrm = thrs(indm);
    sensm = sens(indm);
    specim = speci(indm);
    // compute convex hull of ROC curve and area under hull
    h_speci=[speci;0];
    h_sens=[sens;0];
    [hull] = ConvHull2D([speci;0], [sens;0]);
    hull = unique(hull);
    auh = polyarea([0;h_speci(hull)],[0;h_sens(hull)]);
    if hull($)==length(h_speci);
        hull($) = [];
    end;

    // don't close loop
    // sanity check:
    if abs(trapz(speci(hull), sens(hull)) - auh) > %eps
        warning("rocplot:auh Area under ROC convex hull may be incorrect");
    end
    if ~exists("plottype") | isempty(plottype)
        plottype = 1;
    end
    select plottype
    case 0, return // no plot
    case 1 // standard TPR (= sens) vs FPR (= 1-speci)
        plot(1-speci,sens,"b", 1-speci(hull),sens(hull),"c", ...
        1-speci0,sens0,"ro", 1-specim,sensm,"ms", 1-[0 1],[1 0],"g");
        xlabel("False Positive Rate (1 - speciificity)");
        ylabel("True Positive Rate (Sensitivity)");
        legpos = 4;//'SE';
        //axis([0 1 0 1])
    case 2 // my convention, sens vs speci
        plot(speci,sens,"b", speci(hull),sens(hull),"c", ...
        speci0,sens0,"ro", specim,sensm,"ms", [0 1],[1 0],"g");
        xlabel("specificity");
        ylabel("Sensitivity");
        legpos = 3;//'SW';
        //axis([0 1.05 0 1.05])
    end
    legend(sprintf("ROC Curve, AUC = %.3g", auc),   sprintf("Conv Hull, AUC = %.3g", auh), ...
    sprintf("Acc(0) = %.3g", acc0),   sprintf("Max Acc %.3g", accm), "Chance", ...
    legpos);

    if nargout == 0, clear auc, end // quieten rocplot(scores,classes) without ;
    ieee(mod);
endfunction


function z = trapz (x, y, dim)
    if (nargin < 1) | (nargin > 3)

        error ("z = trapz (x, y, dim)");
    end

    nd = length(size (x));
    sz = size (x);

    have_x = %f;
    have_dim = %f;
    if (nargin == 3)
        have_x = %t;
        have_dim = %t;
    end
    if (nargin == 2)
        if (~ and(size(x)==size(y)) & sum(length(y))==1)
            dim = y;
            have_dim = %t;
        else
            have_x = %t;
        end
    end

    if (~ have_dim)
        // Find the first singleton dimension.
        dim = 0;
        while (dim < nd & sz(dim+1) == 1)
            dim=dim+1;
        end
        dim=dim+1;
        if (dim > nd)
            dim = 1;
        end
    else
        dim = floor (dim);
        if (dim < 1 | dim > nd)
            error ("trapz: invalid dimension along which to sort");
        end
    end

    //   n = sz(dim);
    //   idx1 = list ();
    //   for i = 1:nd
    //     idx1(i) = 1:sz(i);
    //   end
    //   idx2 = idx1;
    //   idx1(dim) = 2 : n;
    //   idx2(dim) = 1 : (n - 1);

    if nd~=2 then
        error ("dim must be 2");
    end;
    n = sz(dim);
    idx1_1=1:sz(1);
    idx1_2=1:sz(2);

    idx2_1=idx1_1;
    idx2_2=idx1_2;
    if (dim==1) then
        idx1_1=2:n;
        idx2_1 = 1 : (n - 1);
    else
        idx1_2=2:n;
        idx2_2 = 1 : (n - 1);
    end;

    if (~ have_x)
        z = 0.5 * sum (x(idx1_1,idx1_2) + x(idx2_1,idx2_2), dim);
    else
        if ( ~ and(size(x)==size(y)))
            error ("trapz: x and y must have same shape");
        end
        z = 0.5 * sum ((x(idx1_1,idx1_2) - x(idx2_1,idx2_2)) .* (y(idx1_1,idx1_2) + y(idx2_1,idx2_2)), dim);
    end
endfunction

function a = polyarea (x, y, dim)
    if (nargin ~= 2 & nargin ~= 3)
        error ("a = polyarea (x, y, dim)");
    elseif (and(size(x)==size(y)))
        if (nargin == 2)
            lshift_ind=[2:length(y),1];rshift_ind=[length(y),1:(length(y)-1)];
            //a = abs (sum (x .* (shift (y, -1) - shift (y, 1)))) / 2;
            a = abs (sum (x .* ( y(lshift_ind) - y(rshift_ind)))) / 2;
        else
            //a = abs (sum (x .* (shift (y, -1, dim) - shift (y, 1, dim)), dim)) / 2;
        end
    else
        error ("polyarea: x and y must have the same shape");
    end
endfunction


function chull=ConvHull2D(x,y)
    //  chull=ConvHull2D(x,y) returns indices into the x and y vectors of the points on
    //     the convex hull. Colinear points wont be part of the convex hull.
    //
    //        Input:
    //                x[nx1] : vectors of x points
    //                y[nx1] : vectors of y points
    //        Output:
    //                chull[mx1]: index of convex hull points
    //
    //     Example:
    //       N=20;
    //       x=rand(N,1);
    //       y=rand(N,1);
    //       chull=ConvHull2D(x,y);
    //       figure(1)
    //       hold on
    //       plot(x,y,'k.')
    //       plot(x(chull),y(chull));
    //       title('Convex Hull')
    //
    //
    //     See also convhulln, delaunay, voronoi,
    //              polyarea, qhull.
    //
    //
    // For info/questions/suggestions : giaccariluigi@msn.com
    //
    // Visit:  http://giaccariluigi.altervista.org/blog/
    //
    // Author: Luigi Giaccari
    // Last Update: 30/4/2009
    // Creation: 27/11/2008


    //Possible improvemets:
    //   -each iteration update cros productvectors instead of points id. Should
    //   be faster when the p2-p1 vector stays the same.
    //   -seed a stable front before starting the main loop. This avoid the c>2
    //   check at each iteration.
    //   -Substitute the if else (if c>2) statement with if elseif c>2. Can it
    //   be faster?
    //


    //How does it work?
    //First delete points that can not be part of the convex hull than apply the
    //cross product algorithom

    //Errors check:
    if nargin ~= 2
        error("ConvHull2D:NotEnoughInputs", "Needs  2 inputs.");
    end

    if ~and(size(x)==size(y))
        error("ConvHull2D:InputSizeMismatch x,y have to be the same size.");
    end
    if length(size(x)) > 2 | length(size(y)) > 2
        error("MATLAB:ConvHull2D:HigherDimArray x,y cannot be arrays of dimension greater than two.");
    end





    N=length(x);

    //quadrilater for point exclusion
    pb=zeros(4,1);

    //tollerances for point exclusion
    toll=[%inf;-%inf;-%inf;-%inf];



    //// Building the quadrilater
    //we find the maximum and minum for (x+y) and (x-y)
    // points inside this polygon will be deleted.
    //Deleting points costs time, generally slow down the algorithm for a small
    //number of points, but make it faster for large models


    // Notes on the use of a for-loop
    //
    //   MATLAB's implementation of Just-in-Time (JIT) accelerators make it
    //   feasible to use a for loop as an alternative to a vectorised statement
    //   in this situation. Depending on the shape and condition of the dataset,
    //   the for-loop may be slightly faster or slower. In the event that an
    //   alternative release of MATLAB alters the performance of the JIT
    //   accelerators for this situation; the user may wish to substitute the
    //   vectorised code below.
    //
    // // x_plus_y = x+y; x_minus_y = x-y;
    //
    // [scratch,ind] = min(x_plus_y); pb(1,:) = [x(ind),y(ind)];
    //
    // [scratch,ind] = max(x_minus_y); pb(2,:) = [x(ind),y(ind)];
    //
    // [scratch,ind] = max(x_plus_y); pb(3,:) = [x(ind),y(ind)];
    //
    // [scratch, ind] = min(x_minus_y); pb(4,:) = [x(ind),y(ind)];
    //


    for i=1:N
        if x(i)+y(i)<toll(1)//maximum of differentia
            pb(1,1)=x(i);pb(1,2)=y(i);
            toll(1)=x(i)+y(i);
        end
        if x(i)-y(i)>toll(2)//down left
            pb(2,1)=x(i);pb(2,2)=y(i);
            toll(2)=x(i)-y(i);
        end
        if x(i)+y(i)>toll(3)//maximum of sum
            pb(3,1)=x(i);pb(3,2)=y(i);
            toll(3)=x(i)+y(i);
        end
        if  -x(i)+y(i)>toll(4)//top right
            pb(4,1)=x(i);pb(4,2)=y(i);
            toll(4)=-x(i)+y(i);
        end
    end


    // Setting tollerances
    // Tollerances decides with point escapes the quadrilater and go building the convex hull
    // They should be set using a semiplane test, using edges of quadrilater
    // but it is easier and computational faster using a rectangle inscribed in it.

    // simplified quadrilater boundaries
    Mx=min(pb([2,3],1));//max x coordinate
    My=min(pb([3,4],2));//max y coordinate
    mx=max(pb([1,4],1));//max x coordinate
    my=max(pb([1,2],2));//max y coordinate

    //deleting internal points
    test=(x>=Mx | x<=mx | y>=My | y<=my);
    index=1:N;
    index=(index(test));//these ones are survivors
    x=x(test);//coordinates of survivors
    y=y(test);
    N=length(x);

    //sorting the survivors
    [y,sindex] = mtlb_sort(y);
    x = x(sindex);

    //// Building the convex hull applying brute crossproduct algorithom
    //The algorithom could have been started here but generally many points are
    //deleted in the previsious process.
    //now that the number of points should be greatly minor it is possible to apply a
    //brute algorithom


    tch=zeros(N,1);//temporay convex hull store
    //this is a waste of memory points of the convex hull should
    //be less than N but preallocation gives more speed



    //This if statement tries to deal with colinear points
    colinear=%f;
    if y(1)==y(2) | y($)==y($-1)

        //count the number of ymin colinear points
        i=1;
        while y(i)==y(i+1)
            i=i+1;
        end
        if i>1
            //sort colinear points
            [x(1:i),xid]=mtlb_sort(x(1:i),"descend");
            tempindex=sindex(xid);
            sindex(1:i)=tempindex;
            //this points are colinear wont be part of convex hull
            sindex(2:i-1)=[];
            y(2:i-1)=[];
            x(2:i-1)=[];

        end
        i=1;
        if x(1)>x(2)//check if x have been sorted
            //count the number of ymax colinear points
            while y($-i+1)==y($-i)
                i=i+1;
            end
            if i>1
                //sort colinear points
                colinear=%t;
                [x($-i+1:$),xid]=mtlb_sort(x($-i+1:$),"ascend");
                tempindex=sindex($-i+1:$);
                sindex($-i+1:$)=tempindex(xid);
                //this points are colinear wont be part of convex hull
                sindex($-i+2:$-1)=[];
                x($-i+2:$-1)=[];
                y($-i+2:$-1)=[];

            end
        else//there are only ymax colinear points
            //count the number of ymax colinear points
            while y($-i+1)==y($-i)
                i=i+1;
            end
            if i>1
                //sort colinear points
                colinear=%t;
                [x($-i+1:$),xid]=mtlb_sort(x($-i+1:$),"descend");
                tempindex=sindex($-i+1:$);
                sindex($-i+1:$)=tempindex(xid);
                //this points are colinear wont be part of convex hull
                sindex($-i+2:$-1)=[];
                x($-i+2:$-1)=[];
                y($-i+2:$-1)=[];
            end
        end
        N=length(x);
    end

    //checking orientation for cross product
    if x(1)>x(2) //clockwise

        orientation=1;
    else
        orientation=-1;//counterclockwise
    end


    // figure(1)
    // hold on
    // axis equal
    // plot(x,y,'k.')
    // id=1:N;
    // text(x,y,num2str(id'));


    c=2;//convex hull vector iterator
    i=2;//points vector iterator

    //the first edge to start  the convex hull
    tch(1)=1;
    tch(2)=2;
    p1=tch(1);
    p2=tch(2);

    while i<=N-1

        //cross product indexing
        // p1   p2   p3
        //<------.-------->
        // c-1   c   i+1

        p3=i+1;

        //debug
        //      figure(1)
        //   plot([x(p1),x(p2)],[y(p1),y(p2)],'-b')
        //    plot([x(p2),x(p3)],[y(p2),y(p3)],'r-')
        //


        //cross product
        cp=orientation*((x(p1)-x(p2))*(y(p3)-y(p2))-(x(p3)-x(p2))*(y(p1)-y(p2)));

        if cp>0   //add point to chull
            //go forward
            tch(c+1)=i+1; //add point to temporary chull
            p1=p2;
            p2=p3;
            c=c+1;
            i=i+1;//try next point
        else//remove  point from temporary chull
            //go back
            if c>2//an exclusion at first iteration may create problems
                c=c-1;
                p2=p1;
                p1=tch(c-1);
            else
                tch(2)=i+1;
                p2=i+1;
                i=i+1;//try next point

            end

        end


    end



    //debug
    // figure(1)
    // hold on
    // axis equal
    // plot(x,y,'k.')
    // plot(x(tch(1:c)),y(tch(1:c)),'r');
    // plot(x(tch(1:c)),y(tch(1:c)));
    // title('Convex Hull','fontsize',13)
    //



    //first half done let's make the second going down

    p1=c-1;
    p1=tch(p1);
    p2=tch(c);

    //checking y colinearity
    if colinear | p1==i-1;//avoid null cross product at first loop
        i=i-1;
    end



    while i>1
        //cross product indexing
        // p1   p2   p3
        //<------.-------->
        // c-1   c   i-1

        p3=i-1;


        //debug
        //      figure(1)
        //   plot([x(p1),x(p2)],[y(p1),y(p2)],'-b')
        //    plot([x(p2),x(p3)],[y(p2),y(p3)],'r-')
        //

        //cross product
        cp=orientation*((x(p1)-x(p2))*(y(p3)-y(p2))-(x(p3)-x(p2))*(y(p1)-y(p2)));
        if cp>0
            //go forward

            tch(c+1)=i-1; //add point to temporary chull
            p1=p2;
            p2=p3;
            c=c+1;
            i=i-1;//try next point

        else  //remove  point from temporary chull


            if c>2//an exclusion at first iteration may create problems
                //go back
                c=c-1;
                p2=p1;
                p1=tch(c-1);
            else
                //can not go back use brute force
                tch(2)=i-1;
                p2=i-1;
                i=i-1;//try next point

            end

        end
    end

    // Re-index to undo the sorting and deleting
    chull=index(sindex(tch((1:c))))';//convexhull

endfunction
