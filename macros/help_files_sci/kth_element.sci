function x = kth_element(X,k [,flag])
//   C-MEX implementation of kth element - this function is part of the NaN-toolbox. 
//   Calling Sequence
//   x = kth_element(X,k [,flag])
//   Parameters
//   X  :  data vector, must be double/real 
//   k  :  which element should be selected
//   flag [optional]: 
//	 0: data in X might be reorded (partially sorted) in-place and 
//	    is slightly faster because no local copy is generated
//	    data with NaN is not correctly handled. 	
//       1: data in X is never modified in-place, but a local copy is used.    
//	    data with NaN is not correctly handled. 	
//	 2: copies data and excludes all NaN's, the copying might be slower 
//	    than 1, but it enables a faster selection algorithm. 
//	    This is the save but slowest option	
// Output: 
//   x: sort(X)(k)
//
//   Bibliography
//   [1] https://secure.wikimedia.org/wikipedia/en/wiki/Selection_algorithm
//   Authors
//    Copyright (C) 2010 Alois Schloegl a.schloegl@ieee.org
//   H. Nahrstaedt - 2010


//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, see <http://www.gnu.org/licenses/>.
//
//
//
//    $Id$
//    Copyright (C) 2010 Alois Schloegl <a.schloegl@ieee.org>
//    This function is part of the NaN-toolbox
//    http://biosig-consulting.com/matlab/NaN/
//