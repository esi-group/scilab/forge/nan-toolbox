function [o,count,SSQ] = sumskipnan_mex(x,DIM,flag,W);
// sumskipnan: sums all non-NaN values
// Calling Sequence
//	[o,count,SSQ] = sumskipnan_mex(x,DIM,flag,W);
// Parameters
// Input:
//  x : data array
//  DIM (optional) : dimension to sum
//  flag (optional): is actually an output argument telling whether some NaN was observed
// W (optional): weight vector to compute weighted sum (default 1)
//
// Output:
// o (weighted) : sum along dimension DIM
// count: count  of valid elements
// SSQ: sums of squares
// Description
// SUMSKIPNAN uses two techniques to reduce errors: 
// 1) long double (80bit) instead of 64-bit double is used internally
// 2) The Kahan Summation formula is used to reduce the error margin from N*eps to 2*eps 
//        The latter is only implemented in case of stride=1 (column vectors only, summation along 1st dimension). 
// Authors
//     Copyright (C) 2009 Alois Schloegl a.schloegl@ieee.org
//   H. Nahrstaedt

//
//
//    $Id: sumskipnan_mex.cpp 7487 2010-07-28 09:28:54Z schloegl $
//    Copyright (C) 2009 Alois Schloegl <a.schloegl@ieee.org>
//    This function is part of the NaN-toolbox
//    http://hci.tugraz.at/~schloegl/matlab/NaN/