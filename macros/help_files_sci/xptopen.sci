function  x = xptopen(filename,readwrite,x)
// Read and write in stata fileformat
// Calling Sequence
//   x = xptopen(filename)
//   x = xptopen(filename,'r')
//		read filename and return variables in struct x
//   xptopen(filename,'w',x)
//		save fields of struct x in filename
//   x = xptopen(filename,'a',x)
//		append fields of struct x to filename
//
//   Bibliography
//   
// [1]	TS-140 THE RECORD LAYOUT OF A DATA SET IN SAS TRANSPORT (XPORT) FORMAT
//	http://support.sas.com/techsup/technote/ts140.html
// [2] IBM floating point format
//	http://en.wikipedia.org/wiki/IBM_Floating_Point_Architecture
// [3] see http://old.nabble.com/Re%3A-IBM-integer-and-double-formats-p20428979.html
// [4] STATA File Format
//	http://www.stata.com/help.cgi?dta
//	http://www.stata.com/help.cgi?dta_113
//   
// Authors   
//   Copyright (C) 2010 Alois Schloegl a.schloegl@ieee.org
//   H. Nahrstaedt - 2010

//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, see <http://www.gnu.org/licenses/>.
//
//    $Id: xptopen.cpp 7960 2010-11-28 23:59:26Z schloegl $
//    Copyright (C) 2010 Alois Schloegl <a.schloegl@ieee.org>
//    This function is part of the NaN-toolbox
//    http://biosig-consulting.com/matlab/NaN/
//
//-------------------------------------------------------------------