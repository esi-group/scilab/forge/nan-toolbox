function [num,status,strarray] = str2array(s,cdelim,rdelim,ddelim)
//   C-MEX implementation of STR2ARRAY - this function is part of the NaN-toolbox. 
// Calling Sequence
//	[...] = str2array(s)
//	[...] = str2array(sa)
//	[...] = str2array(s,cdelim)
//	[...] = str2array(s,cdelim,rdelim)
//	[...] = str2array(s,cdelim,rdelim,ddelim)
//	[num,status,strarray] = str2array(...)
// Parameters
// Input:
//  s :	        char string 
//  sa :	        cell array of strings 
//  cdelim:	column delimiter
//  rdelim:	row delimiter
//  ddelim :     decimal delimiter
//
// Output:
// 
//Authors
//  Copyright (C) 2010 Alois Schloegl a.schloegl@ieee.org
//  H. Nahrstaedt - 2010

//    $Id: STR2ARRAY.cpp 7142 2010-03-30 18:48:06Z schloegl $
//    Copyright (C) 2010 Alois Schloegl <a.schloegl@ieee.org>
//    This function is part of the NaN-toolbox
//    http://biosig-consulting.com/matlab/NaN/
//
-------------------------------------------------------------------
//   Actually, it also fixes a problem in STR2ARRAY.m described here:
//   http://www-old.cae.wisc.edu/pipermail/help-octave/2007-December/007325.html
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, see <http://www.gnu.org/licenses/>.
//-----------------------------------------------------