function R=nan_kurtosis(i,DIM)
    //  estimates the kurtosis
    // Calling Sequence
    // y = kurtosis(x,DIM)
    // Parameters
    // DIM:	dimension
    //	1: STATS of columns
    //	2: STATS of rows
    //	default or []: first DIMENSION, with more than 1 element
    // y:  kurtosis of x in dimension DIM
    // Description
    // features:
    // - can deal with NaN's (missing values)
    // - dimension argument
    // - compatible to Matlab and Octave
    //
    // See also
    // sumskipnan
    // nan_var
    // nan_std
    // nan_skewness
    // nan_moment
    // nan_statistic
    // flag_impl_skip_nan
    // Bibliography
    // http://mathworld.wolfram.com/
    // Authors
    //	Copyright (C) 2000-2003 by Alois Schloegl a.schloegl@ieee.org
    //  H. Nahrstaedt - 2010

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.

    //	$Id$
    //	Copyright (C) 2000-2003 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox for Octave and Matlab
    //       http://biosig-consulting.com/matlab/NaN/
    mod=ieee();
    ieee(2);
    if nargin==1,
        DIM=min(find(size(i)>1));
        if isempty(DIM), DIM=1; end;
    end;

    [R.SUM,R.N,R.SSQ] = sumskipnan(i,DIM);	// sum

    R.MEAN 	= R.SUM./R.N;			// mean
    R.SSQ0	= R.SSQ - real(R.SUM).*real(R.MEAN) - imag(R.SUM).*imag(R.MEAN);	// sum square with mean removed

    //if flag_implicit_unbiased_estim;    //// ------- unbiased estimates -----------
    n1 	= max(R.N-1,0);			// in case of n=0 and n=1, the (biased) variance, STD and SEM are INF
    //else
    //    n1	= R.N;
    //end;
    R.VAR  	= R.SSQ0./n1;	     		// variance (unbiased)
    //R.STD	= sqrt(R.VAR);		     	// standard deviation

    i       = i - repmat(R.MEAN,size(i)./size(R.MEAN));
    //R.CM3 	= sumskipnan(i.^3,DIM)./n1;
    R.CM4 	= sumskipnan(i.^4,DIM)./n1;
    //R.SKEWNESS = R.CM3./(R.STD.^3);
    R = R.CM4./(R.VAR.^2)-3;
    ieee(mod);
endfunction
