function Q=nan_iqr(Y,DIM)
    //  calculates the interquartile range
    // Calling Sequence
    //  Q = nan_iqr(Y)
    //  Q = nan_iqr(Y,DIM)
    //  Q = nan_iqr(HIS)
    // Parameters
    // Q:   IQR along dimension DIM of sample array Y.
    // HIS: must be a HISTOGRAM struct as defined in HISTO2 or HISTO3.
    // Description
    //   Missing values (encoded as NaN) are ignored.
    // Examples
    //
    // x = grand(100,100,'nor',0,1);
    // s = nan_std(x);
    // s_IQR = 0.7413*nan_iqr(x);
    // efficiency = (norm(s-1)./norm(s_IQR-1)).^2
    // efficiency =
    //  0.3297
    //
    // See also
    // nan_percentile
    // nan_quantile
    // nan_histo2
    // nan_histo3
    // Authors
    //	Copyright (C) 2009 by Alois Schloegl a.schloegl@ieee.org
    //	H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    if nargin<2,
        DIM = [];
    end;
    if isempty(DIM),
        DIM = min(find(size(Y)>1));
        if isempty(DIM), DIM = 1; end;
    end;


    if nargin<1,
        error("At least 1 argument needed!");

    else
        Q = nan_quantile(Y,[1,3]/4,DIM);
        Q = diff(Q,1,DIM);
    end;


endfunction
