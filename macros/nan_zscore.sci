function [i,v,m] = nan_zscore(i,DIM)
    // removes the mean and normalizes the data  to a variance of 1.
    // Calling Sequence
    // [z,r,m] = nan_zscore(x,DIM)
    // Parameters
    // x : data
    // DIM:	dimension  1 - STATS of columns 2 - STATS of rows default or []- first DIMENSION, with more than 1 element
    //   z:   z-score of x along dimension DIM
    //   r:   is the inverse of the standard deviation
    //   m:   is the mean of x
    // Description
    // Can be used for Pre-Whitening of the data, too.
    //
    // The data x can be reconstrated with
    //
    //     x = z*diag(1./r) + repmat(m,size(z)./size(m))
    //
    //     z = x*diag(r) - repmat(m.*v,size(z)./size(m))
    //
    // See also
    // sumskipnan
    // nan_mean
    // nan_std
    // nan_detrend
    // Bibliography
    // [1] http://mathworld.wolfram.com/z-Score.html
    // Authors
    //  H. Nahrstaedt
    //  Copyright (C) 2000-2003,2009 by Alois Schloegl a.schloegl@ieee.org

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.


    //	$Id$
    //	Copyright (C) 2000-2003,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    if or(size(i)==0); error("or(size(i)==0)"); end;
    mod=ieee();
    ieee(2);
    if nargin<2
        DIM=[];
    end
    if nargin<3
        W = [];
    end
    if isempty(DIM),
        DIM=min(find(size(i)>1));
        if isempty(DIM), DIM=1; end;
    end;


    // pre-whitening
    m = nan_mean(i,DIM,W);
    i = i-repmat(m,size(i)./size(m));  // remove mean
    v = 1 ./sqrt(nan_mean(i.^2,DIM,W));
    i = i.*repmat(v,size(i)./size(v)); // scale to var=1
    ieee(mod);
endfunction

