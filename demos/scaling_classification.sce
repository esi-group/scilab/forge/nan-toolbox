demopath = get_absolute_file_path("scaling_classification.sce");
    mode(-1);

	if ~isfile(demopath+'/data/svmguide4')
		if MSDOS
			printf('Download http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass/svmguide4;  and save in local directory %s\nPress any key to continue ...\n',demopath+'/data/');
		else 
		        unix('cd '+demopath+'/data; wget http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass/svmguide4'); 
		end;         
	end;        

	if ~isfile(demopath+'/data/svmguide4.t')
		if MSDOS
			printf('Download http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass/svmguide4.t;  and save in local directory %s\nPress any key to continue ...\n',demopath+'/data/');
		else 
		        unix('cd '+demopath+'/data; wget http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass/svmguide4.t'); 
		end;         
	end;        
mode(1);lines(0);

	  try
		  if type(train)~=130
		         error('No SVM training algorithm available. Install libsvm for scilab.\n');
		 end;
	catch
	     error('No SVM training algorithm available. Install libsvm for scilab.\n');
         end
    
// Welcome to the NaN Toolbox for Scilab This demonstration 
// shows how importand the correct scaling is for classification
//
// Press any key to continue...
 
halt('Press return'); clc;
    
// We are using a demo dataset from  http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets
// Preprocessing: Original data: an application on traffic light signals from Georges Bonga at University of Applied Sciences, Berlin.     
//  # of classes: 6 
//  # of data: 300 / 312 (testing)
//  # of features: 10
// We can load the data using readsparse
    
[label_vector, instance_vector] = libsvmread(demopath+'/data/svmguide4');
instance_vector=(full(instance_vector));
[label_vector_test, instance_vector_test] = libsvmread(demopath+'/data/svmguide4.t');
instance_vector_test=(full(instance_vector_test));

// Now we will plotting some feature combination
// Press any key to continue...
halt('Press return'); clc;  mode(-1);
    
    xfull=full(instance_vector);
    scf(0);clf(0);
    subplot(2,2,1)
    nan_gscatter(xfull(:,1),xfull(:,2),label_vector-min(label_vector)+1,'bgrcmyk','.',3)
    title("scatterplot of feature No. 1 vs 2");xlabel("feature No 1");ylabel("feature No 2");
    subplot(2,2,2)
    nan_gscatter(xfull(:,2),xfull(:,4),label_vector-min(label_vector)+1,'bgrcmyk','.',3)
     title("scatterplot of feature No. 2 vs 4");xlabel("feature No 2");ylabel("feature No 4");
    subplot(2,2,3)
    nan_gscatter(xfull(:,8),xfull(:,10),label_vector-min(label_vector)+1,'bgrcmyk','.',3)
    title("scatterplot of feature No. 8 vs 10");xlabel("feature No 8");ylabel("feature No 10");
    subplot(2,2,4)
    nan_gscatter(xfull(:,3),xfull(:,9),label_vector-min(label_vector)+1,'bgrcmyk','.',3)
     title("scatterplot of feature No. 3 vs 9");xlabel("feature No 3");ylabel("feature No 9");
    mode(1);lines(0);
// Press any key to continue...
halt('Press return'); clc; 
 
// At first we will train and test the classificator without any scaling
model=svmtrain(label_vector,instance_vector,'-s 1 -c 1 -t 0 -d 1 -q');

[label,acc,prob_est]=svmpredict(label_vector_test,instance_vector_test,model);

// The result is realy bad
// Press any key to continue...
halt('Press return'); clc;

// In next step we will scale the traindataset to [0,1] and then we scale the test dataset seperatlly to [0,1]
instance_vector_scale=(instance_vector - mtlb_repmat(min(instance_vector,'r'),size(instance_vector,1),1))*diag(1 ./(max(instance_vector,'r')-min(instance_vector,'r'))');
model=svmtrain(label_vector,instance_vector_scale,'-s 1 -c 1 -t 0 -d 1 -q');

instance_vector_test_sc=(instance_vector_test - mtlb_repmat(min(instance_vector_test,'r'),size(instance_vector_test,1),1))*diag(1 ./(max(instance_vector_test,'r')-min(instance_vector_test,'r'))');
[label,acc,prob_est]=svmpredict(label_vector_test,instance_vector_test_sc,model);

// The result is even worser then without scaling
// Press any key to continue...
halt('Press return'); clc;

// In next step we will scale the traindataset to [0,1] and then use the same scaling for the test dataset

min_tr=min(instance_vector,'r');
max_tr=max(instance_vector,'r');
instance_vector_scale=(instance_vector - mtlb_repmat(min_tr,size(instance_vector,1),1))*diag(1 ./(max_tr-min_tr)');
model=svmtrain(label_vector,instance_vector_scale,'-s 1 -c 1 -t 0 -d 1 -q';

instance_vector_test_sc=(instance_vector_test - mtlb_repmat(min_tr,size(instance_vector_test,1),1))*diag(1 ./(max_tr-min_tr)');
[label,acc,prob_est]=svmpredict(label_vector_test,instance_vector_test_sc,model);

// The result is now good. This shows the importants of correct scalling
// Press any key to continue...
halt('Press return'); clc;

// Now we will check if train_sc and test_sc are doing well
mode(-1);
classlabel=label_vector+4;
classlabel(classlabel==5)=4;
classlabel(classlabel==6)=5;
classlabel(classlabel==7)=6;

classlabel_test=label_vector_test+4;
classlabel_test(classlabel_test==5)=4;
classlabel_test(classlabel_test==6)=5;
classlabel_test(classlabel_test==7)=6;
mode(1);
 MODE.TYPE='SVM';
 MODE.options='-s 0 -c 1 -t 0 -d 1 -q';
CC = nan_train_sc(instance_vector,classlabel,MODE);
tt = nan_test_sc(CC,instance_vector_test);
nan_kappa(classlabel_test,tt.classlabel)
