mode(-1)
// Compares performance  


//x=rand(1000000,1);
stacksize(1e7);
result = [];
for N = [5000 ]
    for dim = [10 50 100 150 200]

    d = rand(N,dim,'norm'); // data
    d(:,1)=%nan;

    tic(); M_nan=sumskipnan(d);x = toc();
    tic();M_nan2=sum(thrownan(d));y = toc();
    
    result = [result; [N dim x y]];
    printf("size %d x %d: sumskipnan: %1.3f sec, sum(thrownan()) %1.3f sec\n",N,dim,x,y);
    
    end
end



//
scf();clf();
plot(result(result(:,1)==5000,2),result(result(:,1)==5000,3),'r');

plot(result(result(:,1)==5000,2),result(result(:,1)==5000,4),'b');
legend("sumskipnan","sum(thrownan())");
xlabel("dim");
ylabel("Training time [sec]");
title("sumskipnan performance");






