demopath = get_absolute_file_path("rocplot_demo.sce");


// 	if ~isfile(demopath+'data/iris.data')
// 		if MSDOS
// 			printf('Download http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data and save in local directory %s\nPress any key to continue ...\n',pwd());
// 		else 
// 		        unix('wget http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data'); 
// 		end;         
// 	end;        
//         tmp = fopen('iris.data'); species=fread(tmp,[1,inf],'uint8=>char'); fclose(tmp); 
//         [meas,tmp,species]=str2double(species,',');        
//         meas = meas(:,1:4);
//         species = species(:,5);        
// else 
//         load fisheriris; 
// end; 

loadmatfile(demopath+"data/iris.mat");
species_string=[];
N=max(species.dims);
group_st=["Iris-setosa","Iris-versicolor","Iris-virginica"];
groups_sp=zeros(N,1);
for i=1:N
if species.entries(i)==group_st(1) then
  groups_sp(i)=1;
elseif species.entries(i)==group_st(2) then
   groups_sp(i)=2;
else
   groups_sp(i)=3;
end;
species_string=[species_string;species.entries(i)];
end

// iris data, 2 classes and 2 features
x = meas(51:$,1:2);   
// versicolor=0, virginica=1 
y = bool2s((1:100)'>50);             



CC=nan_train_sc(x,y,'SVM');
tt=nan_test_sc(CC,x);
scf(0);clf(0);
if(size(tt.output,2)==1)
    nan_rocplot(tt.output,y);
else
    nan_rocplot(tt.output(:,2),y);
    
end