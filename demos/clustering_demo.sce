demopath = get_absolute_file_path("clustering_demo.sce");
    mode(1);lines(0);
    
// Welcome to the NaN Toolbox for Scilab This demonstration 
// shows clustering using k-means on the Old Faithful data set.
// We will load data and plot it
	
X=fscanfMat(demopath+"data/faithful.txt");

scf(0);
plot(X(:,1),X(:,2),'g+');
title("Old Faithful data set")
//  In the next step we cluster the data

[model,idx] = nan_kmeans( X, 2 );

scf(1);
plot(X(idx==1,1),X(idx==1,2),'rx')
plot(X(idx==2,1),X(idx==2,2),'bx')
ctrs=model.X;
plot(ctrs(:,1),ctrs(:,2),'ko', 'MarkerSize',12,'LineWidth',2)
plot(ctrs(:,1),ctrs(:,2),'kx', 'MarkerSize',12,'LineWidth',2);
legend('Cluster 1','Cluster 2','Centroids',  'Location','NW')

title("clustered dataset");

