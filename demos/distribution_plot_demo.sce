mode(-1)
//  
//Normal Probability Plots
x = grand(25,1,"nor",10,1);

scf(1);clf(1);
nan_normplot(x)

scf(2);clf(2);
x=grand(100,1,"exp",10);
nan_normplot(x);

//Quantile-Quantile Plots
scf(3);clf(3);
x=grand(50,1,"poi",10);
y=grand(100,1,"poi",5);

nan_qqplot(x,y);

x = grand(100,1,"nor",0,1); 
y =grand(100,1,"bet",5,100);
nan_qqplot(x,y);

//Cumulative Distribution Plots
y = grand(100,1,'chi', 10);
nan_cdfplot(y)

x = 0:0.1:30;

[P,Q]=cdfchi("PQ",x,ones(x)*10);

plot(x,P,'m')
legend('Empirical','Theoretical','Location','NW')
xgrid(1);

//Other Probability Plots

// x = grand(100,1,"nor",0,1); 
// y =grand(100,1,"bet",5,100);
// 
// probplot('bet',[x1 x2])
// legend('normal Sample','beta Sample','Location','NW')