demopath = get_absolute_file_path("train_demo.sce");
mode(-1);

loadmatfile(demopath+"data/iris.mat");
scf();
nan_gscatter(meas(:,1), meas(:,2), species,'rgb','osd');
xlabel('Sepal length');
ylabel('Sepal width');
title("classifier error");
N = size(meas,1);


CC=nan_train_sc(meas(:,1:2),nan_grp2idx(species),'WienerHopf');
tt=nan_test_sc(CC,meas(:,1:2),nan_grp2idx(species));

bad = ~(tt.classlabel(:)==nan_grp2idx(species));

ldaResubErr = sum(bad) / N;

disp(ldaResubErr);

plot(meas(bad,1), meas(bad,2), 'kx');

[x,y] = meshgrid(4:.1:8,2:.1:4.5);
x = x(:);
y = y(:);
j = nan_test_sc(CC,[x y]);
scf();
nan_gscatter(x,y,j.classlabel,'rgb','osd');
xlabel('Sepal length');
ylabel('Sepal width');
title("classifier output");