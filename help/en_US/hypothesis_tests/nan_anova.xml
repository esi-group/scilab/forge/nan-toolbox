<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from nan_anova.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="nan_anova" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>nan_anova</refname>
    <refpurpose>Perform a one-way analysis of variance (ANOVA)</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pval, f, df_b, df_w] = anova (y)
   [pval, f, df_b, df_w] = anova (y,alpha)
   [pval, f, df_b, df_w] = anova (y,g)
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Perform a one-way analysis of variance (ANOVA).  The goal is to test
whether the population means of data taken from k different
groups are all equal.
   </para>
   <para>
Data may be given in a single vector y with groups specified by
a corresponding vector of group labels g (e.g., numbers from 1
to k).  This is the general form which does not impose any
restriction on the number of data in each group or the group labels.
   </para>
   <para>
If y is a matrix and g is omitted, each column of y
is treated as a group.  This form is only appropriate for balanced
ANOVA in which the numbers of samples from each group are all equal.
   </para>
   <para>
Under the null of constant means, the statistic f follows an F
distribution with df_b and df_w degrees of freedom.
   </para>
   <para>
The p-value (1 minus the CDF of this distribution at @var{f}) is
returned in @var{pval}.
   </para>
   <para>
If no output argument is given, the standard one-way ANOVA table is
printed.
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
y=[17,25,22,26;19,27,21,24;20,18,19,30;24,22,26,28];
nan_anova(y,0.01);

One-way ANOVA Table:

Source of Variation   Sum of Squares    df  Empirical Var
*********************************************************
Between Groups              104.0000     3        34.6667
Within Groups               118.0000    12         9.8333
---------------------------------------------------------
Total                       222.0000    15

Test Statistic f              3.5254
p-value                       0.0487
Fcrit (alpha=0.0100)          5.9525

y_list=list(y(:,1),y(:,2),y(:,3),y(:,4));
nan_anova(y_list,0.01);

y_vec=y(:);
groups=[1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4];
nan_anova(y_vec,groups,0.01);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 1995-2011 Kurt Hornik</member>
   <member>H. Nahrstaedt - 2012</member>
   </simplelist>
</refsection>
</refentry>
