// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
x=[0.2113249 0.0002211 0.6653811;0.7560439 0.4453586 0.6283918];

computed = nan_var(x);
expected = variance(x,'r');
assert_checkalmostequal ( computed , expected ,  %eps );

computed = nan_var(x,[],2);
expected = variance(x,'c');
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_var(x(:));
expected = variance(x);
assert_checkalmostequal ( computed , expected ,  %eps );

x=[1 1 %nan];
computed = nan_var(x);
expected = 0;
assert_checkalmostequal ( computed , expected ,  %eps );

x=[1 1 %nan];
computed = nan_var(x');
expected = 0;
assert_checkalmostequal ( computed , expected ,  %eps );

computed = isnan(nan_var(0));
expected = %T;
assert_checkequal ( computed , expected );
