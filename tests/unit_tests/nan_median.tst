// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//

A=[1,2,10;7,7.1,7.01];

computed = nan_median(A);
expected = median(A,'r');
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_median(A,2);
expected = median(A,'c');
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_median(A(:));
expected = median(A);
assert_checkalmostequal ( computed , expected , %eps );

A=[1 1 %nan];
computed = nan_median(A);
expected = 1;
assert_checkalmostequal ( computed , expected , %eps );

A=[1 1 %nan];
computed = nan_median(A');
expected = 1;
assert_checkalmostequal ( computed , expected , %eps );

A=matrix([-9 3 -8 6 74 39 12 -6 -89 23 65 34],[2,3,2]);
computed = nan_median(A,3);
expected = median(A,3);
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_median(A);
expected = median(A,'m');
assert_checkalmostequal ( computed(:) , expected(:) , %eps );

computed = nan_median([1,%nan,3,%inf,-%inf]);
expected = 2;
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_median([1,%nan,3,%inf,4,-%inf]);
expected = 3;
assert_checkalmostequal ( computed , expected , %eps );

x = [1, 2, 3, 4, 5, 6];
x2 = x';
y = [1, 2, 3, 4, 5, 6, 7];
y2 = y';

assert_checktrue(nan_median (x) == nan_median (x2) & nan_median (x) == 3.5);
assert_checktrue(nan_median (y) == nan_median (y2) & nan_median (y) == 4);
assert_checktrue(nan_median ([x2, 2*x2]) == [3.5, 7]);
assert_checktrue(nan_median ([y2, 3*y2]) == [4, 12]);
assert_checkequal(nan_median ([1,2,%nan;4,5,6;%nan,8,9]), [2.5, 5, 7.5]);

// Test input validation
// error nan_median ();
// error nan_median (1, 2, 3);
// error nan_median ({1:5});
// error nan_median (true(1,5));
// error nan_median (1, ones(2,2));
// error nan_median (1, 1.5);
// error nan_median (1, 0);
