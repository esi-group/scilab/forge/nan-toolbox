mode(-1)
// NANTEST checks several mathematical operations and a few 
// statistical functions for their correctness related to NaN's.
// e.g. it checks norminv, normcdf, normpdf, sort, matrix division and multiplication.
//
//
// see also: NANINSTTEST
//
// REFERENCE(S): 
// [1] W. Kahan (1996) Lecture notes on the Status of "IEEE Standard 754 for 
//     Binary Floating-point Arithmetic. 
//


//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; If not, see <http://www.gnu.org/licenses/>.

//	$Id$
//	Copyright (C) 2000-2004,2009 by Alois Schloegl <a.schloegl@ieee.org>
//       This script is part of the NaN-toolbox
//       http://biosig-consulting.com/matlab/NaN/

//FLAG_WARNING = warning;
//printf('off');

try
	x = rand([3,4,5],'normal'); 
	x(~isnan(x)) = 0;
catch
	printf('WARNING: NANTEST fails for 3-DIM matrices. \n');
end;
try
	[s,n] = sumskipnan([%nan,1,4,5]);
catch
	printf('WARNING: SUMSKIPNAN is not avaible. \n');
end;

// check NORMPDF, NORMCDF, NORMINV
x = [-%inf,-2,-1,-.5,0,.5,1,2,3,%inf,%nan]';
if exists('normpdf'),
        q(1) = sum(isnan(normpdf(x,2,0)))>sum(isnan(x));
        if q(1),
                error('NORMPDF cannot handle v=0.\n');
                error('-> NORMPDF should be replaced\n');
        end;
end;

if exists('normcdf'),
        q(2) = sum(isnan(normcdf(x,2,0)))>sum(isnan(x));
        if q(2),
                error('NORMCDF cannot handle v=0.\n');
                error('-> NORMCDF should be replaced\n');
        end;
end;

if exists('norminv'),
        p = [-%inf,-.2,0,.2,.5,1,2,%inf,%nan];
        q(3) = sum(~isnan(norminv(p,2,0)))<4;
        if q(3),
                error('NORMINV cannot handle  correctly v=0.\n');
                error('-> NORMINV should be replaced\n');
        end;
        q(4) = ~isnan(norminv(0,%nan,0)); 
        q(5) = or(norminv(0.5,[1 2 3],0)~=(1:3));
end;

if exists('tpdf'),
        q(6) = ~isnan(tpdf(%nan,4));
        if q(6),
                error('TPDF(NaN,4) does not return NaN\n');
                error('-> TPDF should be replaced\n');
        end;
end;

if exists('tcdf'),
        try
                q(7) = ~isnan(tcdf(%nan,4));
        catch
                q(7) = 1;
        end;
        if q(7),
                error('TCDF(NaN,4) does not return NaN\n');
                error('-> TCDF should be replaced\n');
        end;
end;

if exists('tinv'),
        try
                q(8) = ~isnan(tinv(%nan,4));
        catch
                q(8) = 1;
        end;
        if q(8),
                error('TINV(NaN,4) does not return NaN\n');
                error('-> TINV should be replaced\n');
        end;
end;

q(9) = isreal(double(2+3*%i));
if q(9)
	error('DOUBLE rejects imaginary part\n-> this can affect SUMSKIPNAN\n');
end; 

try 
        x = matrix(1:6,3,2); 
        [cc,nn] = nan_covm(x+%i*x,'e');
        q(10) = 0; 
catch
        q(10) = 1; 
end; 

if 0,
////////// MOD 
if exists('pmodulo')>1,
        if (mod(5,0))~=0,
                error('WARNING: MOD(x,0) does not return 0.\n');
        end;
        if isnan(mod(5,0)),
                error('WARNING: MOD(x,0) returns NaN.\n');
        end;
        if isnan(mod(5,%inf)),
                error('WARNING: MOD(x,INF) returns NaN.\n');
        end;
end;
////////// REM 
if exists('modulo')>1,
        if (modulo(5,0))~=0,
                error('WARNING: REM(x,0) does not return 0.\n');
        end;
        if isnan(modulo(5,0)),
                error('WARNING: REM(x,0) returns NaN.\n');
        end;
        if isnan(modulo(5,%inf)),
                error('WARNING: REM(x,INF) returns NaN.\n');
        end;
end;
end; 

////////// NANSUM(NAN) - this test addresses a problem in Matlab 5.3, 6.1 & 6.5
if exists('nansum'),
        if isnan(nansum(%nan)),
                printf('Warning: NANSUM(NaN) returns NaN instead of 0\n');
                printf('-> NANSUM should be replaced\n');
        end;
end;
////////// NANSUM(NAN) - this test addresses a problem in Matlab 5.3, 6.1 & 6.5
if exists('nanstd'),
        if ~isnan(nanstd(0)),
                error('Warning: NANSTD(x) with isscalar(x) returns 0 instead of NaN\n');
                error('-> NANSTD should be replaced\n');
        end;
end;
////////// GEOMEAN - this test addresses a problem in Octave
if exists('nan_geomean'),
        if isnan(nan_geomean((0:3)')),
                error('Warning: GEOMEAN([0,1,2,3]) NaN instead of 0\n');
                error('-> GEOMEAN should be replaced\n');
        end;
end;
////////// HARMMEAN - this test addresses a problem in Octave
if exists('nan_harmmean'),
        if isnan(nan_harmmean(0:3)),
                error('Warning: HARMMEAN([0,1,2,3]) NaN instead of 0\n');
                error('-> HARMMEAN should be replaced\n');
        end;
end;
////////// BITAND - this test addresses a problem in Octave
if exists('bitand')>1,
        if isnan(bitand(2^33-1,13)),
                error('BITAND can return NaN. \n');
        end;
end;
////////// BITSHIFT - this test addresses a problem in Octave
if exists('bitshift'),
        if isnan(bitshift(5,30,32)),
                error('BITSHIFT can return NaN.\n');
        end;
end;
////////// ALL - this test addresses a problem in some old Octave and FreeMat v3.5
if or(%nan)==%t,
	error('WARNING: ANY(NaN) returns 1 instead of 0\n');
end;
if or([])==%t,
	error('WARNING: ANY([]) returns 1 instead of 0\n');
end;
////////// ALL - this test addresses a problem in some old Octave and FreeMat v3.5
if and(%nan)==%f,
	error('WARNING: ALL(NaN) returns 0 instead of 1\n');
end;
if and([])==%f,
	error('WARNING: ALL([]) returns 0 instead of 1\n');
end;
	
////////// SORT - this was once a problem in Octave Version < 2.1.36 ////////
if ~and(isnan(mtlb_sort([3,4,%nan,3,4,%nan]))==[%f,%f,%f,%f,%t,%t]), 
        error('Warning: SORT does not handle NaN.');
end;

////////// commutativity of 0*NaN	////// This test adresses a problem in Octave
x=[-2:2;4:8]';
y=x;y(2,1)=%nan;y(4,2)=%nan;
B=[1,0,2;0,3,1];
if ~and(and(isnan(y*B)==isnan(B'*y')')),
        printf('WARNING: 0*NaN within matrix multiplication is not commutative\n');
end;

// from Kahan (1996)
tmp = (0-3*%i)/%inf;
if isnan(tmp)
        printf('WARNING: (0-3*i)/inf results in NaN instead of 0.\n');
end;

//(roots([5,0,0])-[0;0])
//(roots([2,-10,12])-[3;2])
//(roots([2e-37,-2,2])-[1e37;1])
////////// check nan/nan   //// this test addresses a problem in Matlab 5.3, 6.1 & 6.5
p    = 4;
//tmp1 = repmat(%nan,p)/repmat(%nan,p);
//tmp2 = repmat(%nan,p)\repmat(%nan,p);
tmp3 = repmat(0,p)/repmat(0,p);
tmp4 = repmat(0,p)\repmat(0,p);
tmp5 = repmat(0,p)*repmat(%inf,p);
tmp6 = repmat(%inf,p)*repmat(0,p);
x    = rand(100,1,'normal')*ones(1,p); y=x'*x; 
tmp7 = y/y;
tmp8 = y\y;

//  if ~and(isnan(tmp1(:))),
//          printf('WARNING: matrix division NaN/NaN does not result in NaN\n');
//  end;
//  if ~and(isnan(tmp2(:))),
//          printf('WARNING: matrix division NaN\\NaN does not result in NaN\n');
//  end;
if ~and(isnan(tmp3(:))),
        //printf('WARNING: matrix division 0/0 does not result in NaN\n');
end;
if ~and(isnan(tmp4(:))),
        //printf('WARNING: matrix division 0\\0 does not result in NaN\n');
end;
if ~and(isnan(tmp5(:))),
        printf('WARNING: matrix multiplication 0*inf does not result in NaN\n');
end;
if ~and(isnan(tmp6(:))),
        printf('WARNING: matrix multiplication inf*0 does not result in NaN\n');
end;
if or(or(tmp7==%inf));
        printf('WARNING: right division of two singulare matrices return INF\n');
end;
if or(or(tmp8==%inf));
        printf('WARNING: left division of two singulare matrices return INF\n');
end;

//tmp  = [tmp1;tmp2;tmp3;tmp4;tmp5;tmp6;tmp7;tmp8];



//printf(FLAG_WARNING); 


////////// QUANTILE TEST 
d = [1 1 2 2 4 4 10 700]; 
q = [-1,0,.05,.1,.25,.49,.5,.51,.75,.8, .999999,1,2];
r = [ %nan, 1, 1, 1, 1.5, 2, 3, 4, 7, 10, 700, 700, %nan]; 
if or( nan_quantile(d, q) -  r>0)
	error('Quantile(1): failed\n');
else
	printf('Quantile(1): OK\n'); 
end; 
if exists('nan_histo3')
	H = nan_histo3(d');
else
	H.X = [1;2;4;10;700];
	H.H = [2;2;2;1;1];
	H.datatype = 'HISTOGRAM'; 
end; 	 
if or( nan_quantile(H, q)' -  r>0)
	error('Quantile(2): failed\n');
else
	printf('Quantile(2): OK\n'); 
end; 

