// <-- NO CHECK REF -->

//library(MASS)
//X <- survey$Smoke

X = ["Never" "Regul" "Occas" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never",..
    "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never",..
    "Never" "Never" "Never" "Never" "Never" "Never" "Occas" "Never" "Heavy" "Never" "Regul" "Occas",..
    "Never" "Never" "Never" "Occas" "Never" "Never" "Never" "Never" "Never" "Never" "Regul" "Never",..
    "Occas" "Never" "Never" "Never" "Never" "Never" "Regul" "Never" "Never" "Never" "Never" "Occas",..
    "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "_NAN"  "Never" "Heavy",..
    "Never" "Never" "Never" "Heavy" "Never" "Heavy" "Never" "Never" "Never" "Never" "Never" "Never",..
    "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Heavy" "Never",..
    "Regul" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Heavy" "Never" "Occas" "Never",..
    "Never" "Regul" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Heavy" "Never" "Heavy",..
    "Occas" "Occas" "Never" "Never" "Never" "Never" "Never" "Regul" "Never" "Never" "Never" "Never",..
    "Never" "Occas" "Never" "Regul" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never",..
    "Never" "Never" "Regul" "Never" "Never" "Never" "Never" "Never" "Regul" "Never" "Never" "Never",..
    "Heavy" "Never" "Never" "Regul" "Never" "Never" "Never" "Occas" "Never" "Never" "Never" "Regul",..
    "Never" "Never" "Never" "Occas" "Regul" "Never" "Never" "Never" "Regul" "Never" "Regul" "Never",..
    "Occas" "Occas" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never",..
    "Occas" "Never" "Never" "Never" "Never" "Never" "Occas" "Never" "Occas" "Never" "Never" "Never",..
    "Regul" "Occas" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never" "Never",..
    "Never" "Never" "Never" "Never" "Heavy" "Never" "Never" "Never" "Never" "Never" "Heavy" "Never",..
    "Regul" "Occas" "Never" "Never" "Never" "Never" "Never" "Never" "Never"];

 //Y <- survey$Pulse
Y = [92 104  87  %nan  35  64  83  74  72  90  80  68  %nan  66  60  %nan  89  74,..
    %nan  78  72  72  64  62  64  90  90  62  76  79  76  78  72  70  54  66,..
    %nan  72  80  %nan  %nan  72  60  80  70  %nan  84  96  60  50  55  68  78  56,..
    65  %nan  70  72  62  %nan  66  72  70  %nan  64  %nan  %nan  64  %nan  80  64  %nan,..
    68  40  88  68  76  %nan  68  %nan  66  76  98  %nan  90  76  70  75  60  92,..
    75  %nan  70  %nan  65  %nan  68  60  %nan  68  %nan  60  %nan  72  80  80  %nan  85,..
    64  67  76  80  75  60  60  70  70  83 100 100  80  76  92  59  66  %nan,..
    68  66  74  90  86  60  86  80  85  90  73  72  %nan  68  84  %nan  65  96,..
    68  75  64  60  92  64  76  80  92  69  68  76  %nan  74  %nan  84  80  %nan,..
    72  60  %nan  81  70  65  %nan  72  %nan  80  50  48  68 104  76  84  %nan  70,..
    68  87  79  70  90  72  79  65  62  63  92  60  68  72  %nan  76  80  71,..
    80  80  61  48  76  86  80  83  76  84  97  %nan  74  83  78  65  68  %nan,..
    %nan  88  %nan  75  %nan  70  88  %nan  %nan  96  80  68  70  71  80  %nan  85  88,..
    %nan  90  85];


//YX.lm <- lm(Y~X)
//   ident=unique(X)
//   lm=zeros(4,1)
//   lm(1)=nanmean(Y(find(X==ident(2))));//heavy
//   lm(2)=nanmean(Y(find(X==ident(3))))-lm(1);//Never
//   lm(3)=nanmean(Y(find(X==ident(4))))-lm(1);//Occas
//   lm(4)=nanmean(Y(find(X==ident(5))))-lm(1);//Regul
  
ident=unique(X);
X_number=zeros(X);
X_number((find(X==ident(1))))=1;
X_number((find(X==ident(2))))=2;
X_number((find(X==ident(3))))=3;
X_number((find(X==ident(4))))=4;
X_number((find(X==ident(5))))=%nan;

yA = Y(find(X==ident(1))); 
yB = Y(find(X==ident(2)));
yC = Y(find(X==ident(3)));
yD = Y(find(X==ident(4)));
yE = Y(find(X==ident(5)));// nan
  
y = list(yA,yB,yC,yD);
 
//YX.anova <- anova(YX.lm)
//print(YX.anova$Df,digits=20)
Df=[3, 187];

//print(YX.anova$F,digits=20)
F_value=0.3063759469524517387
//print(YX.anova$Pr,digits=20)
p_pair=0.82076276107847800922

[p, f, df_b, df_w] = nan_anova(y);
assert_checkalmostequal ( p , p_pair );
assert_checkalmostequal ( f , F_value );
assert_checkalmostequal ( df_b , Df(1) );
assert_checkalmostequal ( df_w , Df(2) );

[p, f, df_b, df_w] = nan_anova(Y,X_number);
assert_checkalmostequal ( p , p_pair );
assert_checkalmostequal ( f , F_value );
assert_checkalmostequal ( df_b , Df(1) );
assert_checkalmostequal ( df_w , Df(2) );
