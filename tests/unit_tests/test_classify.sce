demopath = get_absolute_file_path("test_classify.tst");
// TEST_CLASSIFY tests and compares NaN/CLASSIFY.M with the matlab version of CLASSIFY

//	$Id$
//	Copyright (C) 2009,2010 by Alois Schloegl <a.schloegl@ieee.org>	
//       This function is part of the NaN-toolbox
//       http://hci.tu-graz.ac.at/~schloegl/matlab/NaN/
mode(-1);
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the  License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.



// 	if ~isfile('iris.data')
// 	if MSDOS
// 		printf('Download http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data and save in local directory %s\nPress any key to continue ...\n',pwd());
// 	else 
// 	        unix('wget http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data'); 
//         end;         
// 	end;        
//         tmp = fopen('iris.data'); species=fread(tmp,[1,inf],'uint8=>char'); fclose(tmp); 
//         [meas,tmp,species]=str2double(species,',');        
//         meas = meas(:,1:4);
//         species = species(:,5);  
loadmatfile(demopath+"../../demos/data/iris.mat");

N=max(species.dims);
group_st=["Iris-setosa","Iris-versicolor","Iris-virginica"];
groups_sp=zeros(N,1);
for i=1:N
if species.entries(i)==group_st(1) then
  groups_sp(i)=1;
elseif species.entries(i)==group_st(2) then
   groups_sp(i)=2;
else
   groups_sp(i)=3;
end;

end

SL = meas(51:$,1);
SW = meas(51:$,2);
group = groups_sp(51:$);
scf();
nan_gscatter(SL,SW,group,'rb','v^',[],'off');

legend('Fisher versicolor','Fisher virginica','Location','NW')
       
[X,Y] = meshgrid(linspace(4.5,8),linspace(2,4));
X = X(:); Y = Y(:);

classifiers={'linear','quadratic','diagLinear','diagQuadratic','mahalanobis'};

//p = which('train_sc.m'); 
//p = fileparts(p); 
//rmpath(p); */	
err=zeros(1,max(size(classifiers)));
for k=1:max(size(classifiers))
[C1,err(1,k),P1,logp1,coeff1] = nan_classify([X Y],[SL SW],group,classifiers(k));
end; 

// addpath(p);
// for k=1:length(classifiers)
// [C2,err(2,k),P2,logp2,coeff2] = classify([X Y],[SL SW],group,classifiers{k});
// end; 

disp(err)
                                
 