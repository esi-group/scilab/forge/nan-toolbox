// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//

xy = [0 1; 0 2; 7 6; 5 6];
t = 1e-3
// eucl = @(v,m) sqrt(sumsq(repmat(v,size(m,1),1)-m,2));
deff("[x]=eucl(v,m)","x=sqrt(nan_sumsq(repmat(v,size(m,1),1)-m,2))");


assert_checkalmostequal(nan_pdist(xy),  [1.000 8.602 7.071 8.062 6.403 2.000],%eps,t);
assert_checkalmostequal(nan_pdist(xy,eucl),         [1.000 8.602 7.071 8.062 6.403 2.000],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"euclidean"),  [1.000 8.602 7.071 8.062 6.403 2.000],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"seuclidean"), [0.380 2.735 2.363 2.486 2.070 0.561],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"mahalanobis"),[1.384 1.967 2.446 2.384 1.535 2.045],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"cityblock"),  [1.000 12.00 10.00 11.00 9.000 2.000],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"minkowski"),  [1.000 8.602 7.071 8.062 6.403 2.000],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"minkowski",3),[1.000 7.763 6.299 7.410 5.738 2.000],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"cosine"),     [0.000 0.349 0.231 0.349 0.231 0.013],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"correlation"),[0.000 2.000 0.000 2.000 0.000 2.000],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"spearman"),   [0.000 2.000 0.000 2.000 0.000 2.000],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"hamming"),    [0.500 1.000 1.000 1.000 1.000 0.500],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"jaccard"),    [1.000 1.000 1.000 1.000 1.000 0.500],%eps,t);
assert_checkalmostequal(nan_pdist(xy,"chebychev"),  [1.000 7.000 5.000 7.000 5.000 2.000],%eps,t);
