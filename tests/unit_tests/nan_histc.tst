// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
data = linspace (0, 10, 1001);
computed = nan_histc (data, 0:10);
expected = [repmat(100, 1, 10), 1];
assert_checkalmostequal ( computed , expected , %eps );

data = repmat (linspace (0, 10, 1001), [2, 1, 3]);
computed = nan_histc (data, 0:10, 2);
expected = repmat ([repmat(100, 1, 10), 1], [2, 1, 3]);
assert_checkalmostequal ( computed(:) , expected(:) , %eps );

// %!error histc ();
// %!error histc (1);
// %!error histc (1, 2, 3, 4);
// %!error histc ([1:10 1+i], 2);
// %!error histc (1:10, []);
// %!error histc (1, 1, 3);
