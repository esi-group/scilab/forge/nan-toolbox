// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//

A=[1,2,10;7,7.1,7.01];

computed = nan_std(A);
expected = stdev(A,'r');
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_std(A,[],2);
expected = stdev(A,'c');
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_std(A(:));
expected = stdev(A);
assert_checkalmostequal ( computed , expected , %eps );

A=[1 1 %nan];

computed = nan_std(A);
expected = 0;
assert_checkalmostequal ( computed , expected , %eps );

A=[1 1 %nan];

computed = nan_std(A');
expected = 0;
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_std(0);
expected = %nan;
assert_checkequal ( computed , expected );