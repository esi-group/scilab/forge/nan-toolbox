// <-- NO_CHECK_REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//

computed = nan_center ([1,2,3]);
expected = [-1,0,1];
assert_checkalmostequal ( computed , expected ,  %eps );

computed = nan_center (int8 ([1,2,3]));
expected = int8([-1,0,1]);
assert_checktrue ( computed==expected);

computed = nan_center (ones (3,2));
expected = zeros (3,2);
assert_checkalmostequal ( computed , expected ,  %eps );

computed = nan_center (testmatrix("magi",3));
expected = [3,-4,1;-2,0,2;-1,4,-3];
assert_checkalmostequal ( computed , expected ,  %eps );
