// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//


// make sure that it can go both directions automatically
assert_checkequal(nan_squareform(1:6), [0 1 2 3;1 0 4 5;2 4 0 6;3 5 6 0])
assert_checkequal(nan_squareform([0 1 2 3;1 0 4 5;2 4 0 6;3 5 6 0]), [1:6]')
// 
// // make sure that the command arguments force the correct behavior
assert_checkequal(nan_squareform(1), [0 1;1 0])
assert_checkequal(nan_squareform(1, "tomatrix"), [0 1;1 0])
assert_checkequal(nan_squareform(1, "tovector"), zeros(0,1))




