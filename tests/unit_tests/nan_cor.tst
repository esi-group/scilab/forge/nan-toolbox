// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//

x = rand (10, 2);
assert_checkalmostequal (nan_cor (x), nan_corrcoef (x), 5*%eps);
assert_checktrue (nan_cor (x(:,1), x(:,2)) == nan_corrcoef (x(:,1), x(:,2)));

// Test input validation
// error corrcoef ();
// error corrcoef (1, 2, 3);

