// Test train_sc and test_sc, weighted samples  

mode(-1)

//	$Id$
//	Copyright (C) 2010 by Alois Schloegl <a.schloegl@ieee.org>	
//       This function is part of the NaN-toolbox
//       http://hci.tu-graz.ac.at/~schloegl/matlab/NaN/

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the  License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


classifier= {'REG','REG2','MDA','MD2','QDA','QDA2','LD2','LD3','LD4','LD5','LD6','NBC','aNBC','WienerHopf','PLA', 'LMS','LDA/DELETION','MDA/DELETION','NBC/DELETION','RDA/DELETION','RDA','GDBC','SVM'};//,'RBF', 'LDA/GSVD','MDA/GSVD', 'LDA/GSVD','MDA/GSVD', 'LDA/sparse','MDA/sparse'}; 

N=1e2;
c=[1:N]'*2>N;

W3 = [ones(1,N/2)/5,ones(1,N/10)];
for l=1:size(classifier,'*'),
	printf('%s\n',classifier(l));
for k=1:10,

x=rand(N,2,'normal');
x=x+[c,c];

ix = 1:0.6*N;

//try,
CC = nan_train_sc(x(ix,:),c(ix)+1,classifier(l));
R1 = nan_test_sc(CC,x,c+1);

CC = nan_train_sc(x,c+1,classifier(l));
R2 = nan_test_sc(CC,x,c+1);

CC = nan_train_sc(x(ix,:),c(ix)+1,classifier(l),W3);
R3 = nan_test_sc(CC,x,c+1);

acc1(k,l)=[R1.ACC];
kap1(k,l)=[R1.kappa];
acc2(k,l)=[R2.ACC];
kap2(k,l)=[R2.kappa];
acc3(k,l)=[R3.ACC];
kap3(k,l)=[R3.kappa];
//end; 

end;
end; 

printf('classifier\tACC1 [%%]\tKappa1+-se\t\n'); 
for k = 1:size(acc1,2)

	printf('%8s\t%5.2f\t%5.2f+-\t\n',classifier(k),mean(acc1(:,k))*100,mean(kap1(:,k)));

end

printf('classifier\tACC2 [%%]\tKappa2+-se\t\n'); 
for k = 1:size(acc2,2)

	printf('%8s\t%5.2f\t%5.2f+-\t\n',classifier(k),mean(acc2(:,k))*100,mean(kap2(:,k)));

end

printf('classifier\tACC3 [%%]\tKappa2+-se\t\n'); 
for k = 1:size(acc3,2)

	printf('%8s\t%5.2f\t%5.2f+-\t\n',classifier(k),mean(acc3(:,k))*100,mean(kap3(:,k)));

end

[se,m]=nan_sem(acc1);disp(m)
[se,m]=nan_sem(acc2);disp(m)
[se,m]=nan_sem(acc3);disp(m)

//[diff(m),diff(m)/sqrt(sum(se.^2))]
//[se,m]=sem(kap);[diff(m),diff(m)/sqrt(sum(se.^2))]

//These are tests to compare varios classiers

return 


N=1e2;
c=[1:N]'*2>N;

for k=1:10,
disp(k);
x=rand(N,2,'normal');
x=x+[c,c];

ix = 1:0.6*N;
[R1,CC]=nan_xval(x(ix,:),c(ix)+1,'REG');
[R2,CC]=nan_xval(x,c+1,'REG');
[R3,CC]=nan_xval(x(ix,:),c(ix)+1,'LDA');
[R4,CC]=nan_xval(x,c+1,'LDA');

acc(k,1:4)=[R1.ACC,R2.ACC,R3.ACC,R4.ACC];
kap(k,1:4)=[R1.kappa,R2.kappa,R3.kappa,R4.kappa];

end;
 
[se,m]=nan_sem(acc),//[diff(m),diff(m)/sqrt(sum(se.^2))]
//[se,m]=sem(kap);[diff(m),diff(m)/sqrt(sum(se.^2))]

