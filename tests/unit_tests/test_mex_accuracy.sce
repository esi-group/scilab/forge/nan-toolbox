mode(-1);
// TEST_MEX_ACCURACY evaluates the accuracy and speed of 
//   different accuracy levels in SUMSKIPNAN_MEX and COVM_MEX
//
// see also: FLAG_ACCURACY_LEVEL, SUMSKIPNAN_MEX, COVM_MEX
//
// Reference:
// [1] David Goldberg, 
//       What Every Computer Scientist Should Know About Floating-Point Arithmetic
//       ACM Computing Surveys, Vol 23, No 1, March 1991. 

//	$Id$
// 	Copyright (C) 2009,2010 by Alois Schloegl <a.schloegl@ieee.org>
//       This function is part of the NaN-toolbox
//       http://hci.tu-graz.ac.at/~schloegl/matlab/NaN/

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

flag=0;
stacksize('max');
N = 1e6;
x=rand(N,10,'normal')+1e6;

level = flag_accuracy_level();         //// backup original level 
flag_accuracy_level(0);
tic,t=timer();[cc0,nn0]=covm_mex(x,[],flag);t0=[0,0];t0=[timer()-t,toc()];

flag_accuracy_level(1);
tic,t=timer();[cc1,nn1]=covm_mex(x,[],flag);t1=[timer()-t,toc()];

flag_accuracy_level(2);
tic,t=timer();[cc2,nn2]=covm_mex(x,[],flag);t2=[timer()-t,toc()];

flag_accuracy_level(3);
tic,t=timer();[cc3,nn3]=covm_mex(x,[],flag);t3=[timer()-t,toc()];

tic,t=timer();cc4=x'*x;nn4=size(x,1);t4=[timer()-t,toc()];

flag_accuracy_level(0);
tic,t=timer();[c0,n0]=sumskipnan_mex(x,1,flag);t0s=[timer()-t,toc()];

flag_accuracy_level(1);
tic,t=timer();[c1,n1]=sumskipnan_mex(x,1,flag);t1s=[timer()-t,toc()];

flag_accuracy_level(2);
tic,t=timer();[c2,n2]=sumskipnan_mex(x,1,flag);t2s=[timer()-t,toc()];

flag_accuracy_level(3);
tic,t=timer();[c3,n3]=sumskipnan_mex(x,1,flag);t3s=[timer()-t,toc()];

tic,t=timer();c4=sum(x,1);n4=size(x,1);t4s=[timer()-t,toc()];

flag_accuracy_level(level);        //// restore original level 

cc = list();
cc(1)=cc0;
cc(2)=cc1;
cc(3)=cc2;
cc(4)=cc3;
c = list();
c(1)=c0;
c(2)=c1;
c(3)=c2;
c(4)=c3;
tt = [t0;t1;t2;t3;t4];
t  = [t0s;t1s;t2s;t3s;t4s];
printf('Sum squared differences between accuracy levels:\n');
printf('Level:\t|(0) naive-dou\t|(1) naive-ext\t|(2) kahan-dou \t| (3) kahan-ext\n')
printf('error:\t|N*2^-52\t|N*2^-64\t| 2^-52  \t| 2^-64\n')
printf('COVM_MEX:\ntime:\t|%f\t|%f\t| %f  \t| %f',tt(1,1),tt(2,1),tt(3,1),tt(4,1))
for K1=1:4,
printf('\n(%i)\t',K1-1);
for K2=1:4,
	EE(K1,K2)=sum((cc(K1)-cc(K2)).^2);
	E(K1,K2) =sum((c(K1)-c(K2)).^2);
        printf('|%8g\t',EE(K1,K2)/nn1(1));
end;
end;
printf('\nSUMSKIPNAN_MEX:\n')
printf('time:\t|%f\t|%f\t| %f  \t| %f',t(1,1),t(2,1),t(3,1),t(4,1))
for K1=1:4,
printf('\n(%i)\t',K1-1);
for K2=1:4,
        printf('|%8g\t',E(K1,K2)/n1(1));
end;
end;
printf('\n');



