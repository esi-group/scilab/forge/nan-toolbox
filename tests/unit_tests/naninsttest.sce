mode(-1)
// NANINSTTEST checks whether the functions from NaN-toolbox have been
// correctly installed. 
//
// see also: NANTEST

//    $Id$
//    Copyright (C) 2000-2003 by Alois Schloegl <a.schloegl@ieee.org>
//    This script is part of the NaN-toolbox
//    http://biosig-consulting.com/matlab/NaN/

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; If not, see <http://www.gnu.org/licenses/>.


r = zeros(38,2);

x = [5,%nan,0,1,%nan];

// run test, k=1: with NaN, k=2: all NaN's are removed
// the result of both should be the same. 

//FLAG_WARNING = warning;
warning('off');

funlist = {'sumskipnan','mean','std','var','skewness','kurtosis','sem','median','mad','zscore','coefficient_of_variation','geomean','harmmean','meansq','moment','rms','','corrcoef','rankcorr','spearman','ranks','center','trimean','min','max','tpdf','tcdf','tinv','normpdf','normcdf','norminv','nansum','nanstd','histo_mex','sumskipnan_mex','covm_mex','svmtrain_mex','train','','','','','','','',''};
for k=1:2,
        if k==2, x(isnan(x))=[]; end; 
        r(1,k) =sumskipnan(x(1));
        r(2,k) =nan_mean(x);
        r(3,k) =nan_std(x);
        r(4,k) =nan_var(x);
	r(5,k) = nan_skewness(x);
	r(6,k) =nan_kurtosis(x);
        r(7,k) =nan_sem(x);
        r(8,k) =nan_median(x);
	r(9,k) =nan_mad(x);
    	tmp = nan_zscore(x); 
	r(10,k)=tmp(1);
        if exists('nan_coef_of_variation'),
                r(11,k)=nan_coef_of_variation(x);
        end;
                r(12,k)=nan_geomean(x);
                r(13,k)=nan_harmmean(x);
        if exists('nan_meansq'),
        	r(14,k)=nan_meansq(x);
        end;
        if exists('nan_moment'),
                r(15,k)=nan_moment(x,6);
        end;
        if exists('nan_rms'),
                r(16,k)=nan_rms(x);
        end;
        // r(17,k) is currently empty. 
        	tmp=nan_corrcoef(x',(1:length(x))');
        r(18,k)=or(isnan(tmp(:)));
        if exists('nan_rankcorr'),
                tmp=nan_rankcorr(x',(1:length(x))');
                r(19,k)=or(isnan(tmp(:)));
        end;
        if exists('nan_spearman'),
                tmp=nan_spearman(x',(1:length(x))');
	        r(20,k)=or(isnan(tmp(:)));
        end;
        if exists('nan_ranks'),
                r(21,k)=or(isnan(nan_ranks(x')))+k;
        end;
        if exists('nan_center'),
        	tmp=nan_center(x);
	        r(22,k)=tmp(1);
        end;
        if exists('nan_trimean'),
        	r(23,k)=nan_trimean(x);
        end;
        r(24,k)=min(x);
        r(25,k)=max(x);
        
 //       r(26,k) = k+isnan(tpdf(x(2),4));	        
        
 //       try
 //               r(27,k) = k*(~isnan(tcdf(%nan,4)));	        
 //       catch
 //               r(27,k) = k;	
 //       end;
        
 //       r(28,k) = k*(~isnan(tinv(%nan,4)));	        
        
        //if exists('normpdf'), 
        //        fun='normpdf'; 
        //elseif exists('normal_pdf'),
        //        fun='normal_pdf';
        //end;
        //r(29,k) = (eval(fun,k,k,0)~=Inf)*k;	
 //       r(29,k) = (normpdf(k,k,0)~=%inf)*k;
         
 //       if exists('normcdf'), 
 //                fun='normcdf'; 
 //        elseif exists('normal_cdf'),
 //              fun='normal_cdf';
 //        end;
 //        r(30,k) = (normcdf(4,4,0)~=0.5)*k;	  
         //if exists('norminv'), 
         //        fun='norminv'; 
         //elseif exists('normal_inv'),
         //        fun='normal_inv';
         //end;
         //r(31,k) = k*or(isnan(feval(fun,[0,1],4,0)));	   
 //       r(31,k) = k*or(isnan(norminv([0,1],4,0)));  
 //       if exists('nansum'),
 //       	r(32,k)=k*isnan(nansum(%nan));
 //       end;
 //       if exists('nanstd'),
 //       	r(33,k)=k*(~isnan(nanstd(0)));
 //       end;
        
        ////// check mex files 
        try 
                histo_mex([1:5]');
               	r(34,k)=0;
       	catch;
               	r(34,k)=k;
        end;
        try 
                sumskipnan_mex([1:5]');
               	r(35,k)=0;
       	catch;
               	r(35,k)=k;
       	end;
        try 
                covm_mex([1:5]');
               	r(36,k)=0;
       	catch;
               	r(36,k)=k;
       	end;
//         try 
//                 svmtrain([1,2,3;2,3,4],[1;2],"-q");
//                	r(37,k)=0;
//        	catch;
//                	r(37,k)=k;
//        	end;

//         try
//         if type(train)==130
//                 r(38,k)=0;
//         else
//               r(38,k)=k;
//         end;
//         catch;
//              r(38,k)=k;
//        	end;
end;

// check if result is correct
tmp = abs(r(:,1)-r(:,2))<%eps;

q = zeros(1,5);

// output
if and(tmp(1:32)) & and(~q),
        printf('NANINSTTEST successful - your nan-tools are correctly installed\n');
        if or(~tmp(34:38)),
                printf('Warning: the following mex-files are not (properly) compiled:\n');
                for k=find(~tmp(34:38)'),
                        printf('     %s.mex \n',funlist(k+33));
                end;
                printf('run \n  mex -setup\n  cd .../nan/src\n  make \n');
        end; 
else
       // printf('NANINSTTEST %d not successful \n', find(~tmp));
	printf('The following functions contain a nan-related bug:\n');
        for k=find(~tmp),
              printf(' test nr.%d  - %s\n',k,funlist(k));
        end
	//printf('  - %s\n',strcat(funlist(find(~tmp))));
	printf('Its recommended to install the toolbox.\n');
        error("error!");
end;

//warning(FLAG_WARNING);

