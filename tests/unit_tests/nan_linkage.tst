// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
x = matrix(modulo(testmatrix("magi", 6), 5), -1, 3);
t = 1e-6;

// eucl = @(v,m) sqrt(sumsq(repmat(v,size(m,1),1)-m,2));
deff("[x]=cond2(y)", ["sigma = svd (y);","x=sigma(1) / sigma($);"]);

assert_checkalmostequal (cond2 (nan_linkage (nan_pdist (x))),               34.119045,%eps, t);
assert_checkalmostequal (cond2 (nan_linkage (nan_pdist (x), "complete")),   21.793345,%eps, t);
assert_checkalmostequal (cond2 (nan_linkage (nan_pdist (x), "average")),    27.045012,%eps, t);
assert_checkalmostequal (cond2 (nan_linkage (nan_pdist (x), "weighted")),   27.412889,%eps, t);
// %!warning <monotonically> linkage (pdist (x), "centroid");
// %!test warning off clustering
assert_checkalmostequal (cond2 (nan_linkage (nan_pdist (x), "centroid")),  27.457477,%eps, t);
// %! warning on clustering
// %!warning <monotonically> linkage (pdist (x), "median");
// %!test warning off clustering
assert_checkalmostequal (cond2 (nan_linkage (nan_pdist (x), "median")),    27.683325,%eps, t);
// %! warning on clustering
assert_checkalmostequal (cond2 (nan_linkage (nan_pdist (x), "ward")),       17.195198,%eps, t);
assert_checkalmostequal (cond2 (nan_linkage(x,"ward","euclidean")),     17.195198,%eps, t);
assert_checkalmostequal (cond2 (nan_linkage(x,"ward",list("euclidean"))),   17.195198,%eps, t);
assert_checkalmostequal (cond2 (nan_linkage(x,"ward",list("minkowski", 2))),17.195198,%eps, t);
