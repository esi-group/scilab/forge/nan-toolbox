mode(-1);

//tmp = warning("query");
//warning("off");
//dir = getcwd();
chdir(get_absolute_file_path("run_tests.sce"));
seed = rand("seed");

//  Choice of the Instantaneous Amplitude 
printf("testing nantest ... ");
exec("nantest.sce");
printf("ok!\n");

printf("testing naninsttest ... ");
exec("naninsttest.sce");
printf("ok!\n");

printf("testing fss ... ");
exec("test_fss.sce");
printf("ok!\n");

printf("testing mex accuracy ... ");
exec("test_mex_accuracy.sce");
printf("ok!\n");

printf("testing svm ... ");
exec("test_svm.sce");
printf("ok!\n");

printf("testing train_sc ... ");
exec("test_train_sc.sce");
printf("ok!\n");

printf("testing xval ... ");
exec("test_xval.sce");
printf("ok!\n");


rand("seed",seed);
//chdir(dir);
//warning(tmp);
