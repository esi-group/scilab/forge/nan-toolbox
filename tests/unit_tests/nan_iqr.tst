// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
assert_checkequal (nan_iqr (1:101), 50);
// error iqr ();
// error iqr (1, 2, 3);
// error iqr (1);
// error iqr ([true, true]);
// error iqr (1:10, 3);


