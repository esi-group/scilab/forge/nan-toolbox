// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
computed = nan_covm([1;%nan;2],'D');
expected = 0.5;
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_covm([1;%nan;2],'M');
expected = 2.5;
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_covm([1;%nan;2],'E');
expected = [1,1.5;1.5,2.5];
assert_checkalmostequal ( computed , expected , %eps );


