// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//

A=[1,2,10;7,7.1,7.01];
computed = nan_mean(A);
expected = mean(A,'r');
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_mean(A,2);
expected = mean(A,'c');
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_mean(A(:));
expected = mean(A);
assert_checkalmostequal ( computed , expected , %eps );


A=[1 1 %nan];
computed = nan_mean(A);
expected = 1;
assert_checkalmostequal ( computed , expected , %eps );

A=[1 1 %nan];
computed = nan_mean(A');
expected = 1;
assert_checkalmostequal ( computed , expected , %eps );

A=matrix(1:12,[1,1,2,3,2]);
computed = nan_mean(A);
expected = mean(A,'m');
assert_checkalmostequal ( computed(:) , expected(:) , %eps );

computed = nan_mean([1,%nan],1);
expected = [1,%nan];
assert_checkalmostequal ( computed(:) , expected(:) , %eps );

computed = nan_mean([1,%nan],2);
expected = 1;
assert_checkalmostequal ( computed(:) , expected(:) , %eps );

computed = nan_mean([+%inf,-%inf]);
expected = %nan;
assert_checkequal ( computed(:) , expected(:) );

computed = nan_mean([+0,-0],'h');
expected = %nan;
assert_checkequal ( computed(:) , expected(:) );

computed = nan_mean([1,4,%nan],'g');
expected = 2;
assert_checkalmostequal ( computed(:) , expected(:) , %eps );

x = -10:10;
y = x';
z = [y, y+10];
assert_checktrue(nan_mean (x) == 0);
assert_checktrue(nan_mean (y) == 0);
assert_checktrue(nan_mean (z) == [0, 10]);
assert_checkequal(nan_mean ([2 8], 'g'), 4);
assert_checkequal(nan_mean ([4 4 2], 'h'), 3);
assert_checkequal(nan_mean ( ([1 0 1 1]==1)), 0.75);

// %% Test input validation
// error mean ();
// error mean (1, 2, 3, 4);
// error mean ({1:5});
// error mean (1, 2, 3);
// error mean (1, ones(2,2));
// error mean (1, 1.5);
// error mean (1, 0);
// error mean (1, 3);
// error mean (1, 'b');
