// TEST_XPTOPEN tests XPTOPEN  

//	$Id: test_xptopen.m 7602 2010-08-29 23:01:44Z schloegl $
//	Copyright (C) 2010 by Alois Schloegl <a.schloegl@ieee.org>	
//       This function is part of the NaN-toolbox
//       http://biosig-consulting.com/matlab/NaN/

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the  License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//x.c = [-1000,-2,-1,0,1,2,NaN,10,100,1000,10000,1e6,1e7,1e8]';
//y.Y = [1,2,NaN,1]'+10;

if 0, 
X=struct();
X.a = [-2,-0,%nan,10,444,-%pi]';//,100,1000,10000,1e6,1e7,1e8]';
X.d = [1,2,%nan,1,%inf,-%inf]'+10;
X.b = {'a','B',' ','*','Z','zzz'}';

fn  = 'test.xpt';
Y   = xptopen(fn,'w',X)
Z   = xptopen(fn,'r')


end;

fn = {'buy','humid','prdsale'};
for k1 = 1:max(size(fn));
	X = xptopen(fn(k1),'r');
//	xptopen([fn{k1},'.xpt'],'w',X);
	f = fieldnames(X);

	fid = fopen([fn(k1),'.csv'],'w');
	for k1=1:max(size(f))
		if k1>1, printf(fid,';'); end; 	
		printf(fid,'%s',f(k1));
	end; 	
	printf(fid,'\n');

	for k2=1:max(size(X.(f{1})));
	for k1=1:max(size(f))
		if k1>1, printf(fid,';'); end; 	
		v = X.(f(k1))(k2);
		if isnumeric(v)
		        if (f(k1)=='DATE'),
		        	printf(fid,'%s',datestr(v + datenum([1960,1,1]),1));
		        elseif (f(k1)=='MONTH'),
		        	printf(fid,'%s',datestr(v + datenum([1960,1,1]),3));
		        elseif v==ceil(v),
				printf(fid,'%i',v);
			else
				printf(fid,'%f',v);
			end	
		elseif iscell(v) & type(v{1})==10	
			printf(fid,'%s',v{1});
		else
			printf(fid,'--');
		end;	
	end; 	
	printf(fid,'\n');
	end; 	

	fclose(fid);	
end; 

