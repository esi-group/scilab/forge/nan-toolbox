// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
computed = nan_cat2bin([1;2;5;1;5]);
expected = [1,0,0;0,1,0;0,0,1;1,0,0;0,0,1];
assert_checkalmostequal ( computed , expected , %eps );
