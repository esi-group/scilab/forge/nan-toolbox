// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
A=[1,2;3,4];

computed = sumskipnan(A);
expected = sum(A,1);
assert_checkalmostequal ( computed , expected , %eps );

computed = sumskipnan(A(:));
expected = sum(A);
assert_checkalmostequal ( computed , expected , %eps );

I=uint8([2 95 103;254 9 0]);
computed = sumskipnan(I(:));
expected = sum(I,"double");
assert_checkalmostequal ( computed , expected ,  %eps );

B=[%t %t %f %f];
computed = sumskipnan(B);
expected = sum(B);
assert_checkalmostequal ( computed , expected , %eps );

x=[1 1 %nan];
computed = sumskipnan(x);
expected = 2;
assert_checkalmostequal ( computed , expected , %eps );

x=[1 1 %nan];
computed = sumskipnan(x');
expected = 2;
assert_checkalmostequal ( computed , expected , %eps );

x=[1 %i %nan];
computed = sumskipnan(x);
expected = 1+%i;
assert_checkalmostequal ( computed , expected , %eps );

x=[%i %i %nan];
computed = sumskipnan(x);
expected = 2*%i;
assert_checkalmostequal ( computed , expected , %eps );

x=[1 1 %nan];
[tmp, computed] = sumskipnan(x);
expected = 2;
assert_checkalmostequal ( computed , expected , %eps );

x=[-1 1 %nan];
[tmp, tmp, computed] = sumskipnan(x);
expected = 2;
assert_checkalmostequal ( computed , expected , %eps );

computed = sumskipnan([1,2],1);
expected = [1,2];
assert_checkalmostequal ( computed , expected , %eps );

computed = sumskipnan([1,%nan],2);
expected = 1;
assert_checkalmostequal ( computed , expected , %eps );

computed = sumskipnan([1,%nan],2);
expected = 1;
assert_checkalmostequal ( computed , expected , %eps );

computed = sumskipnan([%nan,1,4,5]);
expected = 10;
assert_checkalmostequal ( computed , expected , %eps );

computed = sumskipnan([%nan,1,4,5]',1,[3;2;1;0]);
expected = 6;
assert_checkalmostequal ( computed , expected , %eps );

