// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
computed = nan_ranks (1:2:10);
expected = 1:5;
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_ranks (10:-2:1);
expected = 5:-1:1;
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_ranks ([2, 1, 2, 4]);
expected = [2.5, 1, 2.5, 4];
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_ranks (ones(1, 5));
expected = 3*ones(1, 5);
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_ranks (1e6*ones(1, 5));
expected = 3*ones(1, 5);
assert_checkalmostequal ( computed , expected , %eps );

computed = nan_ranks (rand (1, 5), 1);
expected = ones(1, 5);
assert_checkalmostequal ( computed , expected , %eps );

// %% Test input validation
// %!error ranks ()
// %!error ranks (1, 2, 3)
// %!error ranks ({1, 2})
// %!error ranks (true(2,1))
// %!error ranks (1, 1.5)
// %!error ranks (1, 0)
// %!error ranks (1, 3)
